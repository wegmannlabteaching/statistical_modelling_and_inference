#------------------------------------
# Functions for Nelder-Mead algorithm
#------------------------------------

logDensityBetaBinomial <- function(values, n, k){
  # the values correspond to a = log(alpha) and b = log(beta). 
  # This is to ensure alpha and beta are always > 0
  curAlpha <- exp(values[1])
  curBeta <- exp(values[2])
  
  # calculate log density of a beta binomial distribution (omit normalizing constant choose(n, k))
  density <- sum(lbeta(k+curAlpha, n-k+curBeta) - lbeta(curAlpha, curBeta))
  return(density)
}

createInitialSimplex <- function(N, min, max){
  # based on a initial guess for range of alpha and beta (arguments min and max),
  # draw random values from this range and construct an initial simplex
  # this simplex is a matrix of dimensions N+1 times N -> number of points (3) times number of dimensions (2)
  initialSimplex <- matrix(runif(n = N*(N+1), min = log(min), max = log(max)), nrow = N+1, ncol = N)
  return(initialSimplex)
}

terminate <- function(indexBest,
                      indexWorst,
                      densityAtPoints,
                      tolerance,
                      counterFunctionCalls,
                      maxNumFunctionCalls){
  # compare density values at best and worst point
  # only stop if this fraction is very small, i.e. all points are almost equally good (-> found the peak)
  fracRange <- 2 * abs(densityAtPoints[indexWorst] - densityAtPoints[indexBest])/
    (abs(densityAtPoints[indexWorst]) + abs(densityAtPoints[indexBest]) + 10e-10)
  if (fracRange < tolerance){
    return(TRUE)
  } else {
    # check if we reached maximal number of function calls
    if (counterFunctionCalls >= maxNumFunctionCalls){
      print(paste0("Reached maximal number of function calls (", maxNumFunctionCalls, ") without converging to minimum!"))
      return(TRUE)
    } else {
      return(FALSE)
    }
  }
}

getCentroid <- function(simplex, indexWorst){
  # calculate center of all points except the worst
  xo <- colSums(simplex[-indexWorst,])/ncol(simplex)
  return(xo)
}

reflect <- function(simplex, indexWorst, alpha){
  xo <- getCentroid(simplex, indexWorst)
  reflectedPoint <- xo + alpha * (xo - simplex[indexWorst,])
  return(reflectedPoint)
}

expand <- function(simplex, indexWorst, reflectedPoint, gamma){
  xo <- getCentroid(simplex, indexWorst)
  expandedPoint <- xo + gamma * (reflectedPoint - xo)
  return(expandedPoint)
}

contract <- function(simplex, indexWorst, rho){
  xo <- getCentroid(simplex, indexWorst)
  contractedPoint <- xo + rho * (simplex[indexWorst,] - xo)
  return(contractedPoint)
}

shrink <- function(simplex, indexBest, sigma){
  for (point in 1:nrow(simplex)) {
    if (point != indexBest){
      simplex[point,] <- simplex[indexBest,] + sigma * (simplex[point,] - simplex[indexBest,])
    }
  }
  return(simplex)
}

runNelderMead <- function(initialSimplex, n, k, tolerance = 1e-20, maxNumFunctionCalls = 5000){
  # simplex is a N+1 times N matrix
  # for our example: we have N=2 variables (a and b), i.e. simplex is 3 times 2 matrix
  # rows = one point of the simplex
  # cols = values for a and b
  simplex <- initialSimplex

  # define Nelder-Mead coefficients
  alpha <- 1
  gamma <- 2
  rho <- 0.5
  sigma <- 0.5
  
  # initialize storage to keep track of all simplexes during algorithm (for plotting)
  bestPoints <- c()
  
  # calculate the function values at the initial simplex
  densityAtPoints <- apply(simplex, 1, logDensityBetaBinomial, n, k)
  
  counterFunctionCalls <- 0
  while(TRUE){
    # find point with worst (lowest), next-worst (next-lowest) and best (highest) density
    indexWorst <- which.min(densityAtPoints)
    indexNextWorst <- sort(densityAtPoints, index.return = T)$ix[2]
    indexBest <- which.max(densityAtPoints)
    
    # add current best point to bestPoints (for plotting it later)
    bestPoints <- rbind(bestPoints, simplex[indexBest,])
    
    # check if we should terminate
    if (terminate(indexBest, indexWorst, densityAtPoints, tolerance, counterFunctionCalls, maxNumFunctionCalls)){
      return(bestPoints)
    }
    
    # reflect
    reflectedPoint <- reflect(simplex, indexWorst, alpha)
    densityAtReflectedPoint <- logDensityBetaBinomial(reflectedPoint, n, k)
    counterFunctionCalls <- counterFunctionCalls + 1
    
    if (densityAtReflectedPoint > densityAtPoints[indexBest]) {
      # density at reflected point is the best so far!
      # try to push it a bit further: expand
      expandedPoint <- expand(simplex, indexWorst, reflectedPoint, gamma)
      densityAtExpandedPoint <- logDensityBetaBinomial(expandedPoint, n, k)
      counterFunctionCalls <- counterFunctionCalls + 1
      
      # now evaluate expansion
      if (densityAtExpandedPoint >= densityAtReflectedPoint) {
        # we further improved: density at expanded point is better than at reflected point
        # replace worst point with expanded point
        simplex[indexWorst,] <- expandedPoint
        densityAtPoints[indexWorst] <- densityAtExpandedPoint
      } else {
        # we didn't improve by expanding
        # replace worst point with reflected point
        simplex[indexWorst,] <- reflectedPoint
        densityAtPoints[indexWorst] <- densityAtReflectedPoint
      }
    } else if (densityAtReflectedPoint >= densityAtPoints[indexNextWorst]) {
      # we improved by reflection, but we didn't beat the best point
      # However, we are still better than the second-worst point
      # accept reflected point: replace worst point with reflected point
      simplex[indexWorst,] <- reflectedPoint
      densityAtPoints[indexWorst] <- densityAtReflectedPoint
    } else {
      # we didn't improve much (or not at all) by reflection
      # the reflected point is worse than the second worst
      # try to contract simplex
      contractedPoint <- contract(simplex, indexWorst, rho)
      densityAtContractedPoint <- logDensityBetaBinomial(contractedPoint, n, k)
      counterFunctionCalls <- counterFunctionCalls + 1
      
      if (densityAtContractedPoint >= densityAtPoints[indexWorst]){
        # we improved by contracting
        # replace worst point with contracted point
        simplex[indexWorst,] <- contractedPoint
        densityAtPoints[indexWorst] <- densityAtContractedPoint
      } else {
        # we didn't improve by contracting: we are worse than the worst point
        # try to contract around the best point (shrink)
        simplex <- shrink(simplex, indexBest, sigma)
        densityAtPoints <- apply(simplex, 1, logDensityBetaBinomial, n, k)
        counterFunctionCalls <- counterFunctionCalls + ncol(simplex) - 1
      }
    }
  }
}

#--------------------------------
# Functions for plotting
#--------------------------------

calculateDensitySurface <- function(a, b, n, k){
  density <- matrix(0, nrow=length(a), ncol=length(b))
  for(i in 1:length(a)){
    for(j in 1:length(b)){
      density[i,j] <- logDensityBetaBinomial(c(a[i], b[j]), n, k)
    }
  }
  return(density)
}

plotDensitySurface <- function(min, max, n, k, col='black', nPoints=100, nlevels=10){
  # set grid
  alpha <- seq(min, max, length.out=nPoints+2)[2:(nPoints+1)]
  beta <- seq(min, max, length.out=nPoints+2)[2:(nPoints+1)]
  
  # calculate density surface
  densitySurface <- calculateDensitySurface(log(alpha), log(beta), n, k)
  
  # adjust zlim to have nice contours
  minLL <- max(densitySurface) - 20;
  zlim <- c(minLL, max(densitySurface));
  
  # plot as contour lines
  plot(0, type='n', xlab=expression(alpha), ylab=expression(beta), xlim=c(0, max), ylim=c(0, max));
  box()
  contour(alpha, beta, densitySurface, zlim=zlim, nlevels=nlevels, add=TRUE, col=col, drawlabels=FALSE, lty=1, lwd=0.5);
}


plotSimplex <- function(bestPoints, min, max, n, k, nlevels=10){
  # add contour of function
  plotDensitySurface(min, max, n, k, nlevels=nlevels);
  
  # generate colors
  colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
  cols <- colfunc(nrow(bestPoints))
  
  # plot simplex
  curRow <- 1
  for (i in 1:nrow(bestPoints)){
    bestPoint <- bestPoints[i,]
    
    # add line connecting the current point to the previous one
    if (i > 1){
      previous <- bestPoints[i-1,]
      segments(x0=previous[1], y0=previous[2], x1=bestPoint[1], y1=bestPoint[2], col = cols[i])
    }
    
    # plot the best point
    points(bestPoint[1], bestPoint[2], col = cols[i], pch = 16, cex = 1)
  }
};

#--------------------------------
# Run!
#--------------------------------
# simulate observed data
n <- 500
numLoci <- 100
simAlpha <- 0.8
simBeta <- 0.7
p <- rbeta(numLoci, simAlpha, simBeta)
k <- rbinom(numLoci, n, p)

# 2 unknown parameters: alpha and beta
N <- 2

# a guess for the approximate values of alpha and beta
min <- 0.1
max <- 1.2

# create initial simplex at random position (inside this range)
initialSimplex <- createInitialSimplex(N, min, max)

# run Nelder-Mead
bestPoints <- runNelderMead(initialSimplex, n, k)
print(exp(bestPoints[nrow(bestPoints),])) # final best point

# plot
pdf("NelderMead_Poolseq.pdf", width = 5, height = 5)
par(
  mar      = c(3.5,3.75,0.2,1),
  xaxs     = "i",
  yaxs     = "i",
  cex.axis = 1,
  cex.lab  = 1,
  pty='s',
  mgp=c(2.5,0.66, 0), 
  las=1
)


plotSimplex(exp(bestPoints), min, max, n, k)
dev.off()

