#-----------------------------------------------------------
# Function to run MCMC
#-----------------------------------------------------------
runMCMC <- function(start, mu, sigma2, d=1, len=2000){
  #prepare storage
  chain <- numeric(len);
  chain[1] <- start;
  
  #run MCMC
  for(i in 2:len){
    #propose move x -> x' according to a uniform kernel with width d, centered on x
    xPrime <- chain[i-1] + runif(1)*d - d/2
    
    #calculate Hastings ratio in log
    log_h <- min(0, 1/(2*sigma2) * ( (chain[i-1] - mu)^2 - (xPrime - mu)^2) )

    #accept or reject
    if(log(runif(1)) < log_h){
      chain[i] <- xPrime
    } else {
      chain[i] <- chain[i-1]
    }
  }
  
  #return
  return(chain);
};

#-----------------------------------------------------------
# Functions to plot
#-----------------------------------------------------------
plotTrace <- function(chain, d=NA, ylim=range(chain), col='red', lwd=1, lty=1){
  #plot trace
  plot(chain, type='l', xlim=c(0,length(chain)), ylim=ylim, xlab="Iteration", ylab="x", col=col, lwd=lwd, lty=lty);
  
  #write start and d
  y <- par("usr")[4] - 0.05 * diff(par("usr")[3:4]);
  if(is.na(d)){
    text(0, y, labels=paste("x0=", chain[1], sep=""), pos=4); 
  } else {
    text(0, y, labels=(bquote('d'==.(d) ~ ~ ~ x[0]==.(chain[1]))), pos=4);
  }
  
  #write acceptance rate
  acc <- sum(chain[1:(length(chain)-1)] != chain[2:length(chain)])/(length(chain)-1);
  text(length(chain), y, labels=paste(round(100*acc, digits=2), "%", sep=""), pos=2);
};

plotDistribution <- function(chain, mu, sigma2, xlim=mu + sqrt(sigma2)*c(-3,3), col='red', lwd=1, lty=1, trueCol='black', trueLty=1, trueLwd=lwd, plotLegend=TRUE, add=FALSE){
  #plot true 
  if(!add){
    x <- seq(xlim[1], xlim[2], length.out=1000)
    y <- dnorm(x, mean=mu, sd=sqrt(sigma2))
    plot(x, y, type='l', col=trueCol, lwd=lwd, ylim=c(0, max(y)*1.5), ylab="Density", xlab="x")
    
    if(plotLegend){
      legend('topright', bty='n', col=c(trueCol, col), lwd=c(trueLwd, lwd), lty=c(trueLty, lty), legend=c("true", "MCMC"));
    }
  }
  
  #plot estimated density
  for(j in 1:length(d)){
    dens <- density(chain, n=8192);
    #make sure density is normalized also for extreme chains
    dens$y <- dens$y / sum(dens$y * (dens$x[2]-dens$x[1]));
    lines(dens, col=col, lwd=lwd, lty=lty);
  }
};

#Q-Q plot
plotQQ <- function(chain, mu, sigma2, col='red', lwd=1, lty=1, trueCol='black', trueLty=1, trueLwd=lwd, plotLegend=TRUE, add=FALSE){
  #plot true
  if(!add){
    plot(0, type='n', ylim=c(0,1.0), ylab="Empirical Quantile", xlab="True Quantile", xlim=c(0,1));
    lines(par("usr")[1:2], par("usr")[3:4], col=trueCol, lwd=lwd, lty=1);
    
    if(plotLegend){
      legend('bottomright', bty='n', col=c(trueCol, col), lwd=c(trueLwd, lwd), lty=c(trueLty, lty), legend=c("true", "MCMC"));
    }
  }
  
  #plot estimated quantiles
  x <- seq(0,1,length.out=200);
  y <- qnorm(x, mean=mu, sd=sqrt(sigma2));
  ec <- ecdf(chain);
  lines(x, ec(y), col=col, lwd=lwd, lty=lty);
};

#---------------------------------------------------
# Simulate and plot
#---------------------------------------------------
#parameters
mu <- 0; #mean of normal from which to sample
sigma2 <- 1; #variance of normal from which to sample
d <- 5; #width of uniform proposal kernel

#run chain
chain <- runMCMC(mu, mu, sigma2, d, len=10000);

chain <- chain[2000:length(chain)];

#plot
layout(matrix(c(1,1:3), nrow=2, byrow=TRUE));
par(mar=c(4,4,1,1), cex=1.25)
plotTrace(chain, d);
plotDistribution(chain, mu, sigma2)
plotQQ(chain, mu, sigma2);

