library(MASS)

#-----------------------------------------
# Function to simulate data
#-----------------------------------------
simReads <- function(f, e, n, lambda_n){
  #simulate genotypes
  g <- rbinom(n, 2, f);
  
  #simulate number of reads
  m <- rpois(n, lambda_n);
  
  #simulate number of ancstral reads
  a <- numeric(n);
  a[g==0] <- rbinom(sum(g==0), m[g==0], 1-e);
  a[g==1] <- rbinom(sum(g==1), m[g==1], 0.5);
  a[g==2] <- rbinom(sum(g==2), m[g==2], e);

  #return a and d = n - a
  return(data.frame(a=a, d=m-a));
};


#-----------------------------------------
# Functions for prior
# We assume a f ~ Beta(alpha, beta) and e ~ EXP(lambda)
#-----------------------------------------
logPrior <- function(f, e, alpha, beta, lambda){
  return(dbeta(f, alpha, beta, log=TRUE) + dexp(e, lambda, log=TRUE));
};

#-----------------------------------------
# Functions for likelihood
#-----------------------------------------
calcLL <- function(a, d, f, e){
  #calc P(d|f,e) = Sum_g [ P(d|g,e)P(g|f)] per individual 
  lik <-       (1-e)^a * e^d * (1-f)^2;   # g = 0
  lik <- lik + (0.5)^(a+d)   * 2*f*(1-f); # g = 1
  lik <- lik + e^a * (1-e)^d * f^2;       # g = 2
  
  #return sum(log(P(d|f,e)))
  return(sum(log(lik)));
};

#-----------------------------------------
# Function to run MCMC
#-----------------------------------------
runMCMC <- function(a, d, alpha, beta, lambda, f_1, e_1, propSd_f=0.1, propSd_e=0.1, len=1000){
  #prepare storage
  chain <- data.frame(f=numeric(len), e=numeric(len));
  chain[1,] <- c(f_1, e_1);
  N <- length(a);
  
  #run MCMC
  oldLL <- calcLL(a, d, chain$f[1], chain$e[1]);
  oldPrior <- logPrior(chain$f[1], chain$e[1], alpha, beta, lambda);
  for(t in 2:len){    
    #update f and e
    fPrime <- abs(chain$f[t-1] + rnorm(1, mean=0, sd=propSd_f));
    if(fPrime > 1){ fPrime <- 2 - fPrime; }
    ePrime  <- abs(chain$e[t-1] + rnorm(1, mean=0, sd=propSd_e));
    if(ePrime > 1){ ePrime <- 2 - ePrime; }
    
    #calculate hastings ratio
    newLL <- calcLL(a, d, fPrime, ePrime);
    newPrior <- logPrior(fPrime, ePrime, alpha, beta, lambda);
    log_h <- newLL + newPrior - oldLL - oldPrior;
    
    #accept or reject
    if(log(runif(1)) < log_h){
      chain[t,] <- c(fPrime, ePrime);
      oldLL <- newLL;
      oldPrior <- newPrior;
    } else {
      chain[t,] <- chain[t-1,];
    }
  }  
  
  #return
  return(chain);
};

#-----------------------------------------
# Function to plot results
#-----------------------------------------
plotTrue2D <- function(true_f=NA, true_e=NA){
  if(!is.na(true_f)){
    lines(rep(true_f,2), par("usr")[3:4], lty=2, col='black');
  }
  
  if(!is.na(true_e)){
    lines(par("usr")[1:2], rep(true_e,2), lty=2, col='black');
  }
}

plotTrace2D <- function(chain, true_f=NA, true_e=NA, col='red', lwd=1, lty=1, lim_f=c(0,1), lim_e=c(0, 0.5)){
  #add contour of likelihood
  plot(chain$f, chain$e, type='l', xlim=lim_f, ylim=lim_e, xlab="Frequency f", ylab=expression(paste("Error rate ", epsilon)), col=col, lwd=lwd, lty=lty);
  
  plotTrue2D(true_f, true_e);
};

plotTraces <- function(chain, true_f=NA, true_e=NA, col='red', lwd=1, lty=1, lim_f=c(0,1), lim_e=c(0, 0.5)){
  if(length(col) == 1){ col <- rep(col,2); }
  
  #plot trace for f
  plot(chain$f, type='l', xlim=c(0,length(chain$f)), ylim=lim_f, xlab="Iteration", ylab="Frequency f", col=col[1], lwd=lwd, lty=lty);
  
  if(!is.na(true_f)){
    lines(par("usr")[1:2], rep(true_f,2), lty=2, col='black');
  }
  
  #write acceptance rate
  acc <- sum(chain$f[1:(length(chain$f)-1)] != chain$f[2:length(chain$f)])/(length(chain$f)-1);
  y <- par("usr")[4] - 0.05 * diff(par("usr")[3:4]);
  text(length(chain$f), y, labels=paste(round(100*acc, digits=2), "%", sep=""), pos=2);
  
  #plot trace for e
  plot(chain$e, type='l', xlim=c(0,length(chain$e)), ylim=lim_e, xlab="Iteration", ylab=expression(paste("Error rate ", epsilon)), col=col[2], lwd=lwd, lty=lty);
  
  if(!is.na(true_e)){
    lines(par("usr")[1:2], rep(true_e,2), lty=2, col='black');
  }
  
  #write acceptance rate
  acc <- sum(chain$e[1:(length(chain$e)-1)] != chain$e[2:length(chain$e)])/(length(chain$e)-1);
  y <- par("usr")[4] - 0.05 * diff(par("usr")[3:4]);
  text(length(chain$e), y, labels=paste(round(100*acc, digits=2), "%", sep=""), pos=2);
};

plotPosterior2D <- function(chain, true_f=NA, true_e=NA, col='red', lwd=1, lty=1, lim_f=c(0,1), lim_e=c(0, 0.5)){
  dens <- kde2d(chain$f, chain$e);
  contour(dens, method="simple", labels="", xlim=lim_f, ylim=lim_e, xlab="Frequency f", ylab=expression(paste("Error rate ", epsilon)), col=col, lwd=lwd, lty=lty);
  
  plotTrue2D(true_f, true_e);
};

plotPosteriors <- function(chain, true_f=NA, true_e=NA, col='red', lwd=1, lty=1, lim_f=c(0,1), lim_e=c(0, 0.5)){
  if(length(col) == 1){ col <- rep(col,2); }
  
  #plot estimated density for f
  dens <- density(chain$f, n=8192);
  #make sure density is normalized also for extreme chains
  dens$y <- dens$y / sum(dens$y * (dens$x[2]-dens$x[1]));
  plot(dens, type='l', xlim=lim_f, xlab="Frequency f", ylab="Posterior density", col=col[1], lwd=lwd, lty=lty, main="");
  
  if(!is.na(true_f)){
    lines(rep(true_f,2), par("usr")[3:4], lty=2, col='black');
  }
  
  #plot estimated density for e
  dens <- density(chain$e, n=8192);
  #make sure density is normalized also for extreme chains
  dens$y <- dens$y / sum(dens$y * (dens$x[2]-dens$x[1]));
  plot(dens, type='l', xlim=lim_e, xlab=expression(paste("Error rate ", epsilon)), ylab="Posterior density", col=col[2], lwd=lwd, lty=lty, main="");
  
  if(!is.na(true_e)){
    lines(rep(true_e,2), par("usr")[3:4], lty=2, col='black');
  }
};

#-----------------------------------------
#Simulate data, run MCMC and plot
#-----------------------------------------
#simulation parameters
n <- 10; #number of individuals
f <- 0.7; #frequency of reference allele used in simulation
e <- 0.05; #error rate used in simulations
lambda_n <- 5; #average number of reads per individual such that n=r+a. We assume n_i~Pois(lambda_n)

#prior
alpha <- 0.5; beta <- 0.5; #alpha and beta of Beta prior on f ~ Beta(alpha, beta);
lambda <- 10; #lambda of exponential prior on e ~ EXP(lambda)

#MCMC parameter
MCMC_len <- 10000; # number of MCMC iterations.
propSd_f <- 0.05; # sd of normal proposal kernel on f
propSd_e <- 0.02; # sd of normal proposal kernel on e

#plotting parameters: ranges for parameters f and e
lim_f <- c(0,1);
lim_e <- c(0, 0.3);

#simulate data
data <- simReads(f, e, n, lambda_n);

#run MCMC, start at true location


#estimate starting locations using EM-algorithm

chain <- runMCMC(data$a, data$d, alpha, beta, lambda, 0.01, 0.5, propSd_f=propSd_f, propSd_e=propSd_e, len=MCMC_len);

#plot in 2D
#pdf("MCMC_Genotypes_2D.pdf", width=8, height=5.7);
#par(xaxs='i', yaxs='i', mfrow=c(2,1), mar=c(3.5,3.75,0.5,1), mgp=c(2.5,0.66, 0), las=1);
#plotTrace2D(chain, true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e);
#plotPosterior2D(chain, true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e);
#dev.off()

#plot in 1D
#pdf("MCMC_Genotypes_1D.pdf", width=8, height=5.7);
#par(xaxs='i', yaxs='i', mfrow=c(2,2), mar=c(3.5,3.75,0.5,1), mgp=c(2.25,0.66, 0), las=1);
#plotTraces(chain, true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e, col=c('orange2', 'dodgerblue'));
#plotPosteriors(chain, true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e, col=c('orange2', 'dodgerblue'));
#dev.off()

#plot figure for script
lim_f <- c(0,1);
lim_e <- c(0,0.5);
#pdf("MCMC_Genotypes_2D_forScript.pdf", width=8, height=2.33);
par(xaxs='i', yaxs='i', mfrow=c(1,2), mar=c(3.5,3.75,0.5,1), mgp=c(2.5,0.66, 0), las=1);
plotTrace2D(chain[1001:1500,], true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e, col='black');
plotPosterior2D(chain[1001:MCMC_len,], true_f=f, true_e=e, lim_f=lim_f, lim_e=lim_e, col='black');
#dev.off();






