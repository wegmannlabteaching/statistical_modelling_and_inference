#-----------------------------------------------------------
# Function to simulate data
#-----------------------------------------------------------
#get P(0|f,e) and P(1|f,e)
getP <- function(f, e){
  p1 <- f*(1-e) + (1-f)*e;
  return(cbind(1-p1, p1));
};

simulate <- function(f, lambda_e, m){
  #simulate error rates from exponential distribution
  e <- rexp(m, lambda_e);
  
  #simulate bases as 0 or 1
  p <- getP(f, e);
  d <- runif(m) < p[,2];
  return(data.frame(d=d, e=e));
};

#-----------------------------------------------------------
# likelihood functions
#-----------------------------------------------------------
calcLL <-  function(d, f){
  p <- getP(f, d$e);
  return(sum( (1-d$d)*log(p[,1]) + d$d*log(p[,2]) ));
};

LLSurface <-  function(d, xlim=c(0,1), numSurfacePoints=10000){
  f <- seq(xlim[1], xlim[2], length.out=numSurfacePoints);
  logLik <- unlist(lapply(f, FUN=calcLL, d=d));
  return(data.frame(f=f, LL=logLik));
};

#derivative of likelihood
calcGradient <- function(d, f){
    p <- getP(f, d$e);
    #Note: R indexes are 1,2 for d=0,1
    x <- (1-d$d)/p[,1] - d$d/p[,2];
    return(sum((2*d$e-1) * x));
};

calcFisherInfo <- function(d, f){
  p <- getP(f, d$e);
  #Note: R indexes are 1,2 for d=0,1
  x <- (1-d$d)/(p[,1]^2) + d$d/(p[,2]^2);
  return(sum((2*d$e-1)^2 * x));
};

#-----------------------------------------------------------
# Newton - Raphson
#-----------------------------------------------------------
NewtonRaphson <- function(d, f_init=0.5, iterations=10){
  #prepare storage
  f <- numeric(iterations+1);
  LL <- numeric(iterations+1);
  
  #set initial f
  f[1] <- f_init;
  LL[1] <- calcLL(d, f[1]);
  
  #iterate
  for(i in 1:iterations){
    #calculate first and second derivative
    gradient <- calcGradient(d, f[i]);
    FisherInfo <- calcFisherInfo(d, f[i]);
    
    #update f
    f[i+1] <- f[i] + gradient / FisherInfo;
    LL[i+1] <- calcLL(d, f[i+1]);
  }
 
  #return
  return(data.frame(f=f, LL=LL));
};

plotNewtonRaphson <- function(NR, d, xlim=c(0,1), true_f=NA, col="red", lwd=1.5, pch=19, plotLLSurface=TRUE, plotMLE=TRUE, numSurfacePoints=10000){
  #calc LSurface
  LLsurf <- LLSurface(d, xlim, numSurfacePoints);
  
  #get ylim
  ylim <- range(LLsurf$LL);
  
  #open plot
  plot(0, type='n', xlim=xlim, ylim=ylim, xlab="Allele frequency f", ylab="LL")
  
  #plot LL surface
  if(plotLLSurface){
    lines(LLsurf$f, LLsurf$LL);
  }
  
  #add MLE
  if(plotMLE){
    MLE <- LLsurf$f[which(LLsurf$LL == max(LLsurf$LL))[1]];
    lines(rep(MLE, 2), par("usr")[3:4], lty=2, col=col);
    axis(side=3, at=MLE, labels=quote(hat("f")), col = NA, tick=FALSE, col.axis=col);
  }
  
  #plot true_f
  if(!is.na(true_f)){
    lines(rep(true_f, 2), par("usr")[3:4], lty=1, col='black');
    axis(side=3, at=true_f, labels=expression(f^'*'), col = NA, tick=FALSE);
  }
  
  #plot Newton-Raphson
  lines(NR$f, NR$LL, type='b', col=col, pch=pch, lwd=lwd);
};

#---------------------------------------------------
# Simulate and plot
#---------------------------------------------------
#simulate data
true_f <- 0.2;
lambda_e <- 10;
m <- 20;
d <- simulate(true_f, lambda_e, m);

NR <- NewtonRaphson(d, f_init=0.95);
plotNewtonRaphson(NR, d, true_f=true_f);

