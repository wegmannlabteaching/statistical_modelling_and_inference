#Admixture HMM 
#States are AA, AB, BB

#-----------------------------------------------------------
#function to calculate transition matrix
#-----------------------------------------------------------
calTransMat <- function(rho){  
  #3x3 matrix for ancestries AA, AB and BB
  tm <- matrix(data=0, ncol=3, nrow=3);
  
  #from AA
  tm[1,1] <- (1-rho[2])^2;
  tm[1,2] <- 2 * rho[2] * (1-rho[1]);  
  tm[1,3] <- rho[2]^2;
  
  #from AB
  tm[2,1] <- rho[1] * (1-rho[2]);
  tm[2,2] <- (1-rho[1])*(1-rho[2]) + rho[1]*rho[2];
  tm[2,3] <- (1-rho[1]) * rho[2];
  
  #from BB
  tm[3,1] <- rho[1]^2;
  tm[3,2] <- 2 * rho[1] * (1-rho[1]);
  tm[3,3] <- (1-rho[1])^2;

  #return matrix
  return(tm);
}

calcInitialDist <- function(rho){
  prior <- c(rho[1]^2, 2*rho[1]*rho[2], rho[2]^2);
  prior <- prior / sum(prior);
  return(prior);
}

#-----------------------------------------------------------
#function to calculate emission probabilities
#-----------------------------------------------------------
calcEmission <- function(f_C){
  f_T <- 1 - f_C;
  
  #3x3 matrix with rows=ancestry AA, AB, BB and cols = genotypes CC, CT, TT
  em <- matrix(0, nrow=3, ncol=3);
  em[1,1] <- f_C[1]*f_C[1]; em[1,2] <- 2*f_C[1]*f_T[1];               em[1,3] <- f_T[1]*f_T[1];
  em[2,1] <- f_C[1]*f_C[2]; em[2,2] <- f_C[1]*f_T[2] + f_C[2]*f_T[1]; em[2,3] <- f_T[1]*f_T[2];  
  em[3,1] <- f_C[2]*f_C[2]; em[3,2] <- 2*f_C[2]*f_T[2];               em[3,3] <- f_T[2]*f_T[2];  
  return(em);
}

#-----------------------------------------------------------
#function to simulate data
#-----------------------------------------------------------
simulate <- function(rho, f_C, len){
  #calc transition and emission matrices
  tm <- calTransMat(rho);
  em <- calcEmission(f_C);
  
  #prepare storage
  z <- numeric(len);
  x <- numeric(len);
  
  #choose random starting position
  prior <- calcInitialDist(rho);
  z[1] <- sample(1:3, 1, prob=prior);
  x[1] <- sample(1:3, 1, prob=em[z[1],]);
  
  #run MC
  for(i in 2:len){
    z[i] <- sample(1:3, 1, prob=tm[z[i-1],]);
    x[i] <- sample(1:3, 1, prob=em[z[i],]);
  }
  
  #return
  return(data.frame(z=z, x=x)); #z is hidden (ancestry), x is observed (genotype)
}

#-----------------------------------------------------------
#Function to run forward recursion
#-----------------------------------------------------------
runForward <- function(x, tm, em, initialDist){
  #calc forward recursion
  len <- length(x);
  forward <- matrix(data=0, nrow=len, ncol=3);
  
  #Start with initial  
  forward[1,] <- em[,x[1]] * initialDist;  
  s <- sum(forward[1,]);
  forward[1,] <- forward[1,] / s;
  scale <- log10(s);
  
  #now make recursion    
  for(i in 2:len){
    #calc forward
    forward[i,] <- (forward[i-1,] %*% tm) * em[,x[i]];
    s <- sum(forward[i,]);
    forward[i,] <- forward[i,] / s;
    scale <- scale + log10(s);    
  }
  
  #return forward and scale
  return(list("forward"= forward, "scale"=scale, "em"=em, "tm"=tm));
}

#-----------------------------------------------------------
#Function to run backward recursion
#-----------------------------------------------------------
runBackward <- function(x, tm, em, forward){ #forward is list of forward function
  #prepare storage
  len <- length(x);
  backward <- matrix(data=0, nrow=len, ncol=3);
  gamma <- matrix(data=0, nrow=len, ncol=3);
  
  #set initial beta = 1
  backward[len,] <- 1;
  
  #now make recursion    
  for(i in (len-1):1){
    #first beta
    backward[i,] <- (backward[i+1,] * em[,x[i+1]]) %*% t(tm);
    s <- sum(backward[i,]);
    backward[i,] <- backward[i,] / s;
    
    #then gamma
    gamma[i,] <- forward$forward[i,] * backward[i,];
    gamma[i,] <- gamma[i,] / sum(gamma[i,]);
  }
  
  #return forward and scale
  return(list("forward"= forward$forward, "backward"=backward, "gamma"=gamma, "scale"=scale, "em"=em, "tm"=tm));
}

#-----------------------------------------------------------
#Function run Bayesian inference
#-----------------------------------------------------------
runBayesian <- function(data, rho, f_C, initialDist){
  #get transition, emission and initial probabilities
  tm <- calTransMat(rho);
  initialDist <- calcInitialDist(rho);
  em <- calcEmission(f_C);
  
  #run forward and backward
  f <- runForward(data$x, tm, em, initialDist)
  b <- runBackward(data$x, tm, em, f)
  
  #return backward object
  return(b);
}

#-----------------------------------------------------------
# Viterbi
#-----------------------------------------------------------
runViterbi <- function(data, rho, f_C, initialDist){ #forward is list of forward function
  #get transition, emission and initial probabilities
  tm <- calTransMat(rho);
  initialDist <- calcInitialDist(rho);
  em <- calcEmission(f_C);
  
  #prepare storage
  x <- data$x;
  len <- length(x);
  delta <- matrix(data=0, nrow=len, ncol=3);
  d <- matrix(data=0, nrow=len, ncol=3);
  
  #set initial beta = 1
  delta[1,] <- em[,x[1]] * initialDist; 
  
  #now make recursion    
  for(t in 2:len){
    #first delta
    for(k in 1:3){
      tmp <- delta[t-1,] * tm[,k] * em[,x[t]];
      delta[t,k] <- max(tmp);
      nn <- sum(tmp==max(tmp));
      xx <- sample.int(sum(tmp==max(tmp)),1);
      d[t,k] <- which(tmp==max(tmp))[xx];
    }
    delta[t,] <- delta[t,] / sum(delta[t,]);
  }
  
  #back tracing
  z <- numeric(len);
  z[len] <- which(delta[len,] == max(delta[len,]));
  for(t in (len-1):1){
    z[t] <- d[t+1,z[t+1]];
  }
  
  #return forward and scale
  return(z);
}


#-----------------------------------------------------------
#Function to plot ancestry probabilities
#-----------------------------------------------------------
openLayout <- function(numPlots=1){
  layout(matrix(1:(numPlots*3), ncol=numPlots), heights=c(0.1,0.1,1));
  par(mar=c(0,4,0.5,1.0), oma=c(4,0,0,0), yaxs='i', xaxs='i', las=1);
}

plotEstimates <- function(data, b, vit, col=c('red', 'gray', 'dodgerblue'), lwd=1.5, makeLayout=TRUE){
  #plot true z
  if(makeLayout){
    openLayout(1);
  }
  
  #plot true chromosome (shift x b< 1 to start at 0)
  plot(0, type='n', xaxt='n', yaxt='n', xlab="", ylab="", xlim=c(1, length(data$x)), bty='n');
  change <- c(1, which(data$z[2:length(data$z)] != data$z[1:(length(data$z)-1)])+1, length(data$z)+1);
  for(i in 2:length(change)){
    rect(change[i-1]-1.5, par("usr")[3], change[i]-1.5, par("usr")[4], col=rgb(t(col2rgb(col[data$z[change[i-1]]])/255), alpha=1.0), border=NA);
  }
  
  #plot viterbi estimates (shift x b< 1 to start at 0)
  plot(0, type='n', xaxt='n', yaxt='n', xlab="", ylab="", xlim=c(1, length(vit)), bty='n');
  change <- c(1, which(vit[2:length(vit)] != vit[1:(length(vit)-1)])+1, length(vit)+1);
  for(i in 2:length(change)){
    if(vit[change[i-1]] == 2){
      rect(change[i-1]-1.5, par("usr")[3], change[i]-1.5, par("usr")[4], col=rgb(t(col2rgb(col[vit[change[i-1]]])/255), alpha=1.0), density=50, border=NA);
    }
  }
  for(i in 2:length(change)){
    if(vit[change[i-1]] != 2){
      rect(change[i-1]-1.5, par("usr")[3], change[i]-1.5, par("usr")[4], col=rgb(t(col2rgb(col[vit[change[i-1]]])/255), alpha=1.0), density=50, border=TRUE);
    }
  }

  #plot estimates (shift x b< 1 to start at 0)
  plot(0:(length(data$x)-1), b$gamma[,1], type='l', col=col[1], xlab="Locus", ylab="Posterior Probability", lwd=lwd, ylim=c(-0.02,1.02));
  lines(0:(length(data$x)-1), b$gamma[,2], type='l', col=col[2], lwd=lwd);
  lines(0:(length(data$x)-1), b$gamma[,3], type='l', col=col[3], lwd=lwd);
}


#-----------------------------------------------------------
#RUN!
#-----------------------------------------------------------
#params (for simulation)
rho <- c(0.002, 0.002); #the two transition probabilities rho_1 and rho_2
len <- 10001; # length of genome in SNPs
f_C <- c(0.45, 0.55); #the frequencies of the C allele populations A and B, respectively

#simulate data
data <- simulate(rho, f_C, len);

#estimate ancestries
Bayes <- runBayesian(data, rho, f_C);
Viterbi <- runViterbi(data, rho, f_C);


#plot
#top to bottom: true, Viterby and Bayes
plotEstimates(data, Bayes, Viterbi, lwd=2);


