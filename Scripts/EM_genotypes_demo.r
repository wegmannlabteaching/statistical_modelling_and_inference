#-----------------------------------------
# Set variables
#-----------------------------------------
n <- 100; #number of individuals
f <- 0.3; #frequency of reference allele used in simulation
e <- 0.1; #error rate used in simulations
lambda_n <- 5; #average number of reads per individual such that n=r+a. We assume n_i~Pois(lambda_n)
maxT <- 100 # maximum number of iterations.
deltaLL <- 10^-3; #convergence criterion: abort EM if the improvement of LL < deltaLL

#-----------------------------------------
# Function to simulate data
#-----------------------------------------
simReads <- function(f, e, n, lambda_n){
  #simulate genotypes
  g <- rbinom(n, 2, f);
  
  #simulate number of reads
  m <- rpois(n, lambda_n);
  
  #simulate number of ancestral reads
  a <- numeric(n);
  a[g==0] <- rbinom(sum(g==0), m[g==0], 1-e);
  a[g==1] <- rbinom(sum(g==1), m[g==1], 0.5);
  a[g==2] <- rbinom(sum(g==2), m[g==2], e);

  #return a and d = n - a
  return(data.frame(a=a, d=m-a));
};

#-----------------------------------------
# Functions for likelihood
#-----------------------------------------
calcLL <- function(a, d, f, e){
  #calc P(d|f,e) = Sum_g [ P(d|g,e)P(g|f)] per individual 
  lik <-       (1-e)^a * e^d * (1-f)^2;   # g = 0
  lik <- lik + (0.5)^(a+d)   * 2*f*(1-f); # g = 1
  lik <- lik + e^a * (1-e)^d * f^2;       # g = 2
  
  #return sum(log(P(d|f,e)))
  return(sum(log(lik)));
};

calcLLSurface <- function(a, d, f_vec, eps_vec){
  res <- matrix(0, nrow=length(f_vec), ncol=length(eps_vec));
  for(i in 1:length(f_vec)){
    for(j in 1:length(eps_vec)){
      res[i,j] <- calcLL(a, d, f_vec[i], eps_vec[j]);
    }
  }
  return(res);
};

plotLLSurface <- function(a, d, col='black', nPoints=200, nlevels=11, minLL=NA){
  #set parameter grid
  f_vec <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
  eps_vec <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
  
  #calculate LL surface
  LLsurf <- calcLLSurface(a, d, f_vec, eps_vec);
  
  #plot contour
  if(is.na(minLL)){
    minLL <- max(LLsurf) - 15;
  }
  zlim <- c(minLL, max(LLsurf));
  contour(f_vec, eps_vec, LLsurf, zlim=zlim, nlevels=nlevels, add=TRUE, col=col, drawlabels=FALSE, lty=1, lwd=0.5);
};

#-----------------------------------------
# Function to run EM
#-----------------------------------------
EMweights_p_g<-function(a, d, f, e){
  #calculates P(g|d,e,f)
  #returns an N x 3 matrix:
  # one row per individual, one col for g=0,1,2
  weights <- matrix(0, nrow=length(a), ncol=3);
  
  #calculate weights for each genotype
  weights[,1] <- (1-e)^a * e^d * (1-f)^2;   #g = 0
  weights[,2] <- (0.5)^(a+d)   * 2*f*(1-f); #g = 1
  weights[,3] <- e^a * (1-e)^d * f^2;       #g = 2
  
  #normalize by row
  return(weights / rowSums(weights));
};

runEM <- function(a, d, f_1, e_1, maxT=20, deltaLL=10^-3){
  #prepare storage
  f <- numeric(maxT); f[1] <- f_1;  
  e <- numeric(maxT); e[1] <- e_1;  
  LL <- numeric(maxT); LL[1] <- calcLL(a, d, f[1], e[1]);
  N <- length(a);
  
  #run EM
  for(t in 2:maxT){    
    # E-step: calculate EM weights
    p_g <- EMweights_p_g(a, d, f[t-1], e[t-1]);
    
    # M-step: update parameters new parameters
    #Note: R indexes are 1:3 (not 0:2 as for g)
    f[t] <- sum(p_g[,2] + 2*p_g[,3]) / (2*N);
    e[t] <- sum(d*p_g[,1] + a*p_g[,3]) /
            sum((d+a) * (p_g[,1]+p_g[,3]));
    
    #calc LL
    LL[t] <- calcLL(a, d, f[t], e[t]);
    
    #check convergence
    if(LL[t] - LL[t-1] < deltaLL || t == maxT){
      return(list(f=f[1:t], e=e[1:t], LL=LL[1:t]));
    }
  }  
};

#-----------------------------------------
# Function to plot results
#-----------------------------------------
getMinLLAmongEM <- function(EMList){
  minLL <- min(EMList[[1]]$LL);
  if(length(EMList) > 1){
    for(i in 2:length(EMList)){
      if(min(EMList[[i]]$LL) < minLL){
        minLL <- min(EMList[[i]]$LL);
      }
    }
  }
  return(minLL);
};

plotEM <- function(EM, col='black'){
  lines(EM$f, EM$e, col=col, type='l', lwd=1.5);
  
  #special symbol for first
  numIt <-  length(EM$LL);
  points(EM$f[2:numIt], EM$e[2:numIt], col=col, pch=19, cex=0.65);
  points(EM$f[1], EM$e[1], col=col, pch=15, cex=0.9);
};

plotEMList <- function(EMList, a, d, maxLL=0, col=c("black", "dodgerblue", "red", "purple", "orange2"), nlevels=11, true_f=NA, true_e=NA){
  #find minLL among runs
  minLL <- getMinLLAmongEM(EMList);
  
  #init plot
  plot(0, type='n', xlab="Frequency f", ylab=expression(paste("Error rate ", epsilon)), xlim=c(0,1), ylim=c(0,1));
  
  #plot true values
  if(!is.na(true_f)){
    lines(rep(true_f, 2), par("usr")[3:4], col='black', lty=2);  
  }
  if(!is.na(true_e)){
    lines(par("usr")[1:2], rep(true_e, 2), col='black', lty=2);  
  }
  
  #add contour of likelihood
  if(nlevels > 0){
    plotLLSurface(a, d, minLL=minLL, nlevels=nlevels);
  }
  
  #plot EM runs
  for(i in 1:length(EMList)){
    plotEM(EMList[[i]], col=col[i]);
  }
};

plotLLTrace <- function(EM, col='black'){
  lines(EM$LL, col=col, lwd=1.5);
  
  #special symbol for first
  numIt <-  length(EM$LL);
  points(2:numIt, EM$LL[2:numIt], col=col, pch=19, cex=c(0.65));
  points(EM$LL[1], col=col, pch=15, cex=0.9);
};

plotLLTraces <- function(EMList, maxLL, col=c("black", "dodgerblue", "red", "purple", "orange2")){
  #find minLL abnd maxIt among runs
  minLL <- getMinLLAmongEM(EMList);

  #sort traces by numIt
  numIt <- numeric(length(EMList));
  for(i in 1:length(EMList)){
    numIt[i] <- length(EMList[[i]]$LL);
  }
    
  #set ylim
  ylim <- c(floor((minLL-1) / 5), ceiling((maxLL+1) / 5)) * 5;
  
  #open plot
  par(xaxs='r', yaxs='i')
  plot(0, type='n', xlim=c(1,max(numIt)), ylim=ylim, xlab="Iteration t", ylab="LL(f,e)");
  
  #plot maxLL
  lines(par("usr")[1:2], rep(maxLL, 2), lty=2)

  #plot traces of each EM with decreasing numIt
  for(i in sort(numIt, decreasing=TRUE, index.return=TRUE)$ix){
    plotLLTrace(EMList[[i]], col=col[i]);
  }
};

#-----------------------------------------
#Simulate data, run EM and plot
#-----------------------------------------
#simulate data
data <- simReads(f, e, n, lambda_n);

#set starting points
#start <- matrix(runif(2*rep), ncol=2);
start <- matrix(c(0.55, 0.4, 0.155,0.075,0.93,0.52,0.18,0.355, 0.61, 0.86), ncol=2, byrow=TRUE);

#run EM for multiple starting points
maxIt <- 0;
minLL <- 0;
EMList <- list();

for(i in 1:dim(start)[1]){
  EMList[[i]] <- runEM(data$a, data$d, start[i,1], start[i,2], maxT=maxT, deltaLL=deltaLL);
}

#get maxLL by running one EM for very long
longEM <- runEM(data$a, data$d, start[i,1], start[i,2], maxT=1000, deltaLL=10^-6);
maxLL <- max(longEM$LL);

#now plot EM and LL traces
par(xaxs='i', yaxs='i', mfrow=c(1,2), pty='s', mar=c(3.5,3.75,0.2,1), mgp=c(2.5,0.66, 0), las=1);
plotEMList(EMList, data$a, data$d, nlevels=15, true_f=f, true_e=e);
plotLLTraces(EMList, maxLL);

