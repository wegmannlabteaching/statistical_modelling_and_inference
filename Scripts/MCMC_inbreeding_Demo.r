#-----------------------------------------
# Function to simulate data
#-----------------------------------------
calcGenoFreq <- function(F_val, f_l){
  # calculate P(g_il | F, f_l)
  genoFreq <- numeric(3);
  genoFreq[1] = (1 - F_val) * f_l^2 + F_val * f_l;
  genoFreq[2] = 2 * (1 - F_val) * f_l * (1 - f_l);
  genoFreq[3] = (1 - F_val) * (1 - f_l)^2 + F_val * (1 - f_l);
  genoFreq[which(genoFreq == 0)] <- 0.00000000001;
  return(genoFreq);
}

simulate <- function(sim_F, sim_a, n, L){
  #simulate allele frequencies
  f <- rbeta(L, sim_a, sim_a);
  
  #genotype matrix n x L
  g <- matrix(NA, nrow = n, ncol=L);
  for(l in 1:L)
    #simulate genotypes locus by locus, with probability given by P(g_il | F, f_l)
    g[,l] <- sample(size=n, 0:2, prob=calcGenoFreq(sim_F, f[l]), replace=T);
  return(g);
}

#-----------------------------------------
# Functions for likelihood
#-----------------------------------------
calculateLogLLAllLoci <- function(F_val, data, f){
  # calculate P(g_il | F, f_l) and apply it to each locus
  genoFreqs <- log(sapply(f, calcGenoFreq, F_val=F_val));
  LL <- genoFreqs[1,] * data[,1] + genoFreqs[2,] * data[,2] + genoFreqs[3,] * data[,3];
  LL[f == 0] <- -99999999999;
  LL[f == 1] <- -99999999999;
  
  return(LL);
}

#-----------------------------------------
# Function to run MCM
#-----------------------------------------
runMCMC <- function(data, F_1, a_1, f_1, lambdaF, lambdaA, propSd_F=0.1, propSd_a=0.1, propSd_f=0.1, len=1000){
  L <- nrow(data) 
  # initialize storage
  chain_F <- numeric(len);
  chain_a <- numeric(len);
  chain_f <- matrix(0, nrow=len, ncol=L);
  chain_LL <- numeric(len);
  
  #initialize chain
  chain_F[1] <- F_1;
  chain_a[1] <- a_1;
  chain_f[1,] <- f_1;
  oldLL_f <- calculateLogLLAllLoci(chain_F[1], data, chain_f[1,]);
  chain_LL[1] <- sum(oldLL_f);

  for(t in 2:len){
    #------ update F ------
    #propose and mirror (0 <= F <= 1)
    old_F <- chain_F[t-1];
    new_F <- abs(old_F + rnorm(1, 0, propSd_F));
    if (new_F > 1)
      new_F <- 2 - new_F;
    newLL_f <- calculateLogLLAllLoci(new_F, data, chain_f[t-1,]);
    
    #prior (exponential)
    logPriorRatio <- dexp(new_F, lambdaF, log=TRUE) - dexp(old_F, lambdaF, log=TRUE);
    
    #calculate Hastings ratio
    LL <- sum(newLL_f);
    logH <- logPriorRatio + LL - chain_LL[t-1];
    
    #accept or reject
    if(log(runif(1)) < logH){
      chain_F[t] <- new_F;
      oldLL_f <- newLL_f;
    } else{
      chain_F[t] <- old_F;
    }
    
    #------ update f (all in one) ------
    # propose and mirror (0 <= f <= 1)
    old_f <- chain_f[t-1,];
    new_f <- abs(old_f + rnorm(L, 0, propSd_f));
    new_f[new_f > 1] <- 2 - new_f[new_f > 1];

    #calculate prior (beta)
    logPriorRatio <- dbeta(new_f, chain_a[t-1], chain_a[t-1], log=TRUE) - dbeta(old_f, chain_a[t-1], chain_a[t-1], log=TRUE);
    
    #calculate Hastings ratio
    newLL_f <- calculateLogLLAllLoci(chain_F[t], data, new_f);
    logH <- logPriorRatio + newLL_f - oldLL_f;
    
    #accept or reject
    logRandom <- log(runif(L));
    accept <- logRandom <= logH;
    reject <- !accept;
    
    #update
    chain_f[t,] <- old_f;
    chain_f[t, accept] <- new_f[accept];
    oldLL_f[accept] <- newLL_f[accept];
    chain_LL[t] <- sum(oldLL_f[reject]) + sum(newLL_f[accept]);
    
    #------ update a ------
    #propose and mirror (a > 0)
    old_a <- chain_a[t-1];
    new_a <- abs(old_a + rnorm(1, 0, sd=propSd_a));
    
    #calculate prior (beta)
    logPriorRatio <- dexp(1-new_a, lambdaA, log=TRUE) - dexp(1-old_a, lambdaA, log=TRUE);
    
    #calculate Hastings ratio
    LL_old <- sum(dbeta(chain_f[t,], old_a, old_a, log=TRUE));
    LL_new <- sum(dbeta(chain_f[t,], new_a, new_a, log=TRUE));
    logH <- logPriorRatio + LL_new - LL_old;
    
    #accept or reject
    if(log(runif(1)) < logH){
      chain_a[t] <- new_a;
    } else {
      chain_a[t] <- old_a;
    }
  }
  return(data.frame(F_val=chain_F, a=chain_a, f=chain_f));
}

#-----------------------------------------
# Functions to plot
#-----------------------------------------
plotPosterior <- function(chain_F, chain_a, sim_F, sim_a, lambdaF, lambdaA, len_burnin, myLwd=1.5, F_lim=0.3){
  len <- length(chain_F)
  #plot posterior of F
  dens <- density(chain_F[len_burnin:len]);
  plot(dens, xlim=c(0,F_lim), col='orange2', main="", ylab="Posterior density", xlab="F", ylim=c(0, max(dens$y)*1.05), lwd=myLwd);
  #add line for true F
  abline(v= sim_F, lty="32", lwd=myLwd);
  #add prior
  x <- seq(0, F_lim, length.out=100);
  prior_y <- dexp(x, lambdaF) / pexp(1.0, lambdaF);
  lines(x, prior_y, col='black', lwd=myLwd);
  
  #plot posterior of a
  dens <- density(chain_a[len_burnin:len]);
  plot(dens, xlim=c(0.2,1.0), col='dodgerblue', main="", ylab="Posterior density", xlab="a", ylim=c(0, max(dens$y)*1.05), lwd=myLwd);
  #add line for true a
  abline(v = sim_a, lty="32", lwd=myLwd);
  #add prior
  x <- seq(0, 1, length.out=100);
  prior_y <- dexp(1-x, lambdaA) / pexp(1.0, lambdaA);
  lines(x, prior_y, col='black', lwd=myLwd);
  
  #print point estimates
  print(paste("Posterior mean F", mean(chain_F[len_burnin:len])));
  print(paste("Posterior mean a", mean(chain_a[len_burnin:len])));
}


plotTrace <- function(x, ylim, ylab, true_val, col, myLwd=1.5){
  plot(x, type='l', xlim=c(0,length(x)), ylim=ylim, xlab="Iteration", ylab=ylab, col=col, lwd=myLwd);
  #add line for true value
  abline(h = true_val, lty="32", lwd=myLwd);
}

#-----------------------------------------
#Simulate data, run MCMC and plot
#-----------------------------------------
#simulation parameters
L <- 500; # number of loci
n <- 10; # number of individuals
sim_F <- 0.2 # F used in simulations
sim_a <- 0.5; # beta prior on F used in simulations

#MCMC parameters
MCMC_len <- 10000; # number of MCMC iterations.
burnin_len <- 500; # number of burnin iterations.
propSd_F <- 0.01; # sd of normal proposal kernel on F
propSd_a <- 0.01;# sd of normal proposal kernel on a
propSd_f <- 0.01; # sd of normal proposal kernel on f

#prior
lambdaF <- 10; #lambda of exponential prior on F ~ EXP(lambda)
lambdaA <- 10; #lambda of exponential prior on a ~ EXP(lambda)

#simulate data
genotypes <- simulate(sim_F, sim_a, n, L);

#prepare data as matrix of genotype counts
data <- matrix(0, ncol=3, nrow=L);
data[,1] = colSums(genotypes == 0);
data[,2] = colSums(genotypes == 1);
data[,3] = colSums(genotypes == 2);

# determine initial values
F_1 <- 0;
a_1 <- 1;
f_1 <- (data[,1]*2 + data[,2]) / (2*rowSums(data));

# run MCMC
chain <- runMCMC(data, F_1, a_1, f_1, lambdaF, lambdaA, propSd_F=propSd_F, propSd_a=propSd_a, propSd_f=propSd_f, len=MCMC_len);
chain_F <- chain$F_val;
chain_a <- chain$a;
chain_f <- chain[,3:ncol(chain)];

#set layout options
par(mar=c(4,4,1,1), las=1, xaxs='i', yaxs='i', mgp=c(2.5,0.85, 0));
layout(matrix(c(1, 2, 3, 4), ncol=2, byrow=FALSE), widths=c(2,1));
F_lim <- 0.3

#plot trace of F
plotTrace(chain_F, ylim=c(0,F_lim), "F", sim_F, "orange2");

#plot trace of a
plotTrace(chain_a, ylim=c(0.2,1.0), "a", sim_a, "dodgerblue");

#plot posterior of F and a
plotPosterior(chain_F, chain_a, sim_F, sim_a, lambdaF, lambdaA, burnin_len);
