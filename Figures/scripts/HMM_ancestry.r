#Admixture HMM 
#States are AA, AB, BB

#params (for simulation)
rho <- c(0.001, 0.005); #recombination rate
pA <- c(0.5,0.5); #frequency of A ancestry in genome
len <- 10001; # length of genome in SNPs
CA <- c(0.4, 0.45); CB <- c(0.6,0.55); #Frequency of C allele in populations A and B, respectively. Note: TA = 1 - CA!

#-----------------------------------------------------------
#function to calculate transition matrix
#-----------------------------------------------------------
calTransMat <- function(rho, pA){  
  pB <- 1 - pA;
  #3x3 matrix for ancestries AA, AB and BB
  tm <- matrix(data=0, ncol=3, nrow=3);
  #from AA
  tm[1,2] <- 2 * rho*pB * ((1-rho) + rho*pA);  
  tm[1,3] <- (rho*pB)^2
  tm[1,1] <- 1.0 - tm[1,2] - tm[1,2];
  
  #from AB
  tm[2,1] <- rho*pA * ((1-rho) + rho*pA);  
  tm[2,3] <- rho*pB * ((1-rho) + rho*pB);
  tm[2,2] <- 1.0 - tm[2,1] - tm[2,3];
  
  #from BB
  tm[3,1] <- (rho*pA)^2;
  tm[3,2] <- 2 * rho*pA * ((1-rho) + rho*pB);
  tm[3,3] <- 1.0 - tm[3,1] - tm[3,2];  

  #return matrix
  return(tm);
}

#-----------------------------------------------------------
#function to calculate emission probabilities
#-----------------------------------------------------------
calcEmission <- function(CA, CB){
  TA <- 1-CA; TB <- 1-CB;
  #3x3 matrix with rows=ancestry AA, AB, BB and cols = genotypes CC, CT, TT
  em <- matrix(0, nrow=3, ncol=3);
  em[1,1] <- CA*CA; em[1,2] <- 2*CA*TA;       em[1,3] <- TA*TA;
  em[2,1] <- CA*CB; em[2,2] <- CA*TB + CB*TA; em[2,3] <- TA*TB;  
  em[3,1] <- CB*CB; em[3,2] <- 2*CB*TB;       em[3,3] <- TB*TB;  
  return(em);
}

#-----------------------------------------------------------
#function to simulate data
#-----------------------------------------------------------
simulate <- function(rho, pA, CA, CB, len){
  #calc transition and emission matrices
  tm <- calTransMat(rho, pA);
  em <- calcEmission(CA, CB);
  
  #prepare storage
  z <- numeric(len);
  x <- numeric(len);
  
  #choose random starting position
  prior <- c(pA^2, 2*pA*(1-pA), (1-pA)^2);  
  z[1] <- sample(1:3, 1, prob=prior);
  x[1] <- sample(1:3, 1, prob=em[z[1],]);
  
  #run MC
  for(i in 2:len){
    z[i] <- sample(1:3, 1, prob=tm[z[i-1],]);
    x[i] <- sample(1:3, 1, prob=em[z[i],]);
  }
  
  #return
  return(data.frame(z=z, x=x)); #z is hidden (ancestry), x is observed (genotype)
}

#-----------------------------------------------------------
#Function to run forward recursion
#-----------------------------------------------------------
runForward <- function(x, rho, pA, em){
  #calc transition matrix
  tm <- calTransMat(rho, pA);  
  
  #calc forward recursion
  len <- length(x);
  forward <- matrix(data=0, nrow=len, ncol=3);
  
  #Start with initial  
  prior <- c(pA^2, 2*pA*(1-pA), (1-pA)^2);
  forward[1,] <- em[,x[1]] * prior;  
  s <- sum(forward[1,]);
  forward[1,] <- forward[1,] / s;
  scale <- log10(s);
  
  #now make recursion    
  for(i in 2:len){
    #calc forward
    forward[i,] <- (forward[i-1,] %*% tm) * em[,x[i]];
    s <- sum(forward[i,]);
    forward[i,] <- forward[i,] / s;
    scale <- scale + log10(s);    
  }
  
  #return forward and scale
  return(list("forward"= forward, "scale"=scale, "em"=em, "tm"=tm));
}

#-----------------------------------------------------------
#Function to run backward recursion
#-----------------------------------------------------------
runBackward <- function(x, rho, pA, em, forward){ #forward is list of forward function
  #calc transition matrix
  tm <- calTransMat(rho, pA);  
  
  #prepare storage
  len <- length(x);
  backward <- matrix(data=0, nrow=len, ncol=3);
  gamma <- matrix(data=0, nrow=len, ncol=3);
  
  #set initial beta = 1
  backward[len,] <- 1;
  
  #now make recursion    
  for(i in (len-1):1){
    #first beta
    backward[i,] <- (backward[i+1,] * em[,x[i+1]]) %*% tm;
    s <- sum(backward[i,]);
    backward[i,] <- backward[i,] / s;
    
    #then gamma
    gamma[i,] <- forward$forward[i,] * backward[i,];
    gamma[i,] <- gamma[i,] / sum(gamma[i,]);
  }
  
  #return forward and scale
  return(list("forward"= forward$forward, "backward"=backward, "gamma"=gamma, "scale"=scale, "em"=em, "tm"=tm));
}


#-----------------------------------------------------------
#Function to calc and plot ancestry probabilities
#-----------------------------------------------------------
estimateAncestries <- function(data, rho, pA, em){
  #run forward and backward
  f <- runForward(data$x, rho, pA, em)
  b <- runBackward(data$x, rho, pA, em, f)
  
  #return backward object
  return(b);
}

openLayout <- function(numPlots=1){
  layout(matrix(1:(numPlots*2), ncol=numPlots), heights=c(0.1,1));
  par(mar=c(0,4,0.5,1.0), oma=c(4,0,0,0), yaxs='i', xaxs='i', las=1);
}

plotEstimates <- function(data, b, col=c('red', 'gray', 'dodgerblue'), lwd=1.5, makeLayout=TRUE){
  #plot true z
  if(makeLayout){
    openLayout(1);
  }
  
  #plot true chromosome (shift x b< 1 to start at 0)
  plot(0, type='n', xaxt='n', yaxt='n', xlab="", ylab="", xlim=c(1, length(data$x)), bty='n');
  change <- c(1, which(data$z[2:length(data$z)] != data$z[1:(length(data$z)-1)])+1, length(data$z)+1);
  for(i in 2:length(change)){
    rect(change[i-1]-1.5, par("usr")[3], change[i]-1.5, par("usr")[4], col=rgb(t(col2rgb(col[data$z[change[i-1]]])/255), alpha=1.0), border=NA);
  }
  
  #plot estimates (shift x b< 1 to start at 0)
  plot(0:(length(data$x)-1), b$gamma[,1], type='l', col=col[1], xlab="Locus", ylab="Posterior Probability", lwd=lwd, ylim=c(-0.02,1.02));
  lines(0:(length(data$x)-1), b$gamma[,2], type='l', col=col[2], lwd=lwd);
  lines(0:(length(data$x)-1), b$gamma[,3], type='l', col=col[3], lwd=lwd);
}
#-----------------------------------------------------------
#RUN!
#-----------------------------------------------------------
#opn PDF
pdf("HMM_HiddenStatePosterior.pdf", width=length(rho)*4.5, height=3.25)
data <- list();
em <- list();

openLayout(length(rho));
for(i in 1:length(rho)){
  #simulate data
  data[[i]] <- simulate(rho[i], pA[i], CA[i], CB[i], len);

  #Estimate Ancestries
  #-------------------------------
  #calc emission matrix
  em[[i]] <- calcEmission(CA[i], CB[i]);
  es <- estimateAncestries(data[[i]], rho[i], pA[i], em[[i]]);

  #plot
  plotEstimates(data[[i]], es, makeLayout=FALSE, lwd=2);
}

dev.off();

