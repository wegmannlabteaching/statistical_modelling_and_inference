#params
mu <- 0;
sd <- 1;
d <- c(0.1, 10, 100, 10);
start <- c(0, 0, 0, -1000);
rr <- c(-3.25,3.25);
col <- c("purple", "", "red", "dodgerblue");
col <- c("dodgerblue", "red", "purple", "orange2");

len <- 2000;
runMCMC <- FALSE;

if(runMCMC){
  res <- matrix(0, ncol=length(d), nrow=len);
  for(j in 1:length(d)){
    res[1,j] <- start[j];
    for(i in 2:len){
      x <- res[i-1,j] + runif(1)*d[j] - d[j]/2;
      h <- dnorm(x, mean=mu, sd=sd, log=TRUE) - dnorm(res[i-1,j], mean=mu, sd=sd, log=TRUE);
      if(log(runif(1)) < h){
        res[i,j] <- x;
      } else {
        res[i,j] <- res[i-1,j];
      }
    }
  }
}

#plot
#------------------------
#pdf("MCMC_Normal.pdf", width=8, height=4)
par(mar=c(3.5,3,0,1), oma=c(0,0,0.5,0.5), xaxs='i', yaxs='i', las=1, mgp=c(2,0.66, 0));
layout(matrix(c(1,2,5,3,4,5), ncol=3, byrow=TRUE), width=c(1,1,1));

#traces
for(j in 1:length(d)){
  acc <- sum(res[1:(len-1),j] != res[2:len,j])/len;
  plot(res[,j], type='l', ylim=rr, xlab="Iteration", ylab="x", col=col[j], lwd=1.25, xlim=c(0,len));
  text(len, rr[1]*0.9, labels=paste(round(100*acc, digits=2), "%", sep=""), pos=2);
  text(0, rr[2]*0.9, labels=paste("d=", d[j], ", x0=", start[j], sep=""), pos=4);
}

#dist
#x <- seq(rr[1], rr[2], length.out=1000);
#y <- dnorm(x, mean=mu, sd=sd);
#par(mar=c(3.5,4,0,0));
#plot(x, y, type='l', col='black', lwd=2, ylim=c(0, max(y)*1.5), ylab="Density", xlab="x")
#for(j in 1:length(d)){
#  lines(density(res[,j]), col=col[j], lwd=1.5)
#}

#cumuldist
#x <- seq(rr[1], rr[2], length.out=1000);
#y <- pnorm(x, mean=mu, sd=sd);
#plot(x, y, type='l', col='black', lwd=2, ylim=c(0,1.0), ylab="Density", xlab="x", xlim=c(-4,4))
#for(j in 1:length(d)){
#  ec <- ecdf(res[,j]);
#  lines(x, ec(x), col=col[j], lwd=1.25)
#}

#Q-Q plot
xx <- seq(0,1,length.out=200)
yy <- qnorm(xx, mean=mu, sd=sd)
plot(0, type='n', ylim=c(0,1.0), ylab="Empirical Quantile", xlab="True Quantile", xlim=c(0,1))
lines(par("usr")[1:2], par("usr")[3:4], lty=1, col='black', lwd=1.25)
for(j in 1:length(d)){
  ec <- ecdf(res[,j]);
  lines(xx, ec(yy), col=col[j], lwd=1.5)
}
#legend("topleft", bty='n', col=col, lwd=1.5, legend=paste("d=", d, ", x0=", start, sep=""));

#dev.off();

