#--------------
#definitions
#--------------
n_runs <- 10000
burnin <- 500;
SD_p <- 0.01
SD_F <- 0.01;
SD_a <- 0.01;

#priors
#unif on a?
#exponential on F with lambda
lambda_FIS <- 10;
lambda_a <- 10;

simulate_data = FALSE;
read_simulated_data = TRUE;
real_data = FALSE;
runMCMC = FALSE;
plotLogLL= FALSE;
print_debug_F = FALSE;
print_debug_p = FALSE;
limit_chr = NA;
site_observed <- 100

if(real_data == TRUE && simulate_data == TRUE){stop("can't use real and simulated data at the same time!")}
if(simulate_data && read_simulated_data){stop("don't simulate and read simulated data at the same time!")}


calcGenoFreq <- function(FIS,p,gt){
  genoFreq <- numeric(length(gt));
  genoFreq[gt == 0] = (1 - FIS) * p^2 + FIS * p
  genoFreq[gt == 1] = 2 * (1 - FIS) * p * (1 - p)
  genoFreq[gt == 2] = (1 - FIS) * (1 - p)^2 + FIS * (1 - p)
  genoFreq[which(genoFreq == 0)] <- 0.00000000001
  #if(sum(genoFreq < 0, na.rm=TRUE) != 0){stop("negative values in genoFreq")}
  return(genoFreq)
}

#--------------
#simulate
#--------------
n_sites <- 500;
n_ind <- 10;
sim_FIS <- 0.2
beta_a <- 0.5;


simulate <- function(FIS, p, n_ind){
  #matrix nInd x nSites
  d <- matrix(rep(NA, times=length(p)*max(n_ind)), ncol=length(p))
  for(i in 1:length(p)){
    #fill matrix site by site
    d[1:n_ind,i] <- sample(size=n_ind, c(0,1,2), prob=c(calcGenoFreq(FIS,p[i],gt=0), calcGenoFreq(FIS,p[i],gt=1), calcGenoFreq(FIS,p[i],gt=2)), replace=T)
  }
  return(d)
}

if(simulate_data == TRUE){
  print("simulating data")
  sim_p <- rbeta(n_sites, beta_a, beta_a);
  #sim_p <- sample(x=seq(0,1, by=0.01), size=n_sites, replace = T)
  d <- simulate(sim_FIS, sim_p, n_ind)
  write.csv(d,file="simulated_d.txt", quote=F, row.names=F)
  write.csv(sim_p,file="simulated_p.txt", quote=F, row.names=F)
}

if(read_simulated_data){
  d <- read.csv("simulated_d.txt", header=TRUE)
  sim_p <- read.csv("simulated_p.txt", header=TRUE)
}

#prepare data as matrix of genotype counts
D <- matrix(0, ncol=3, nrow=n_sites);
D[,1] = colSums(d==0);
D[,2] = colSums(d==1);
D[,3] = colSums(d==2);

#--------------
#MCMC functions
#--------------
calculateLogLLAllSitesFast <- function(FIS, D, p){
  #calculate likelihood of all data
  
  genoFreq_Homo = log((1 - FIS) * p^2 + FIS * p);
  genoFreq_Het = log(2 * (1 - FIS) * p * (1 - p));
  genoFreq_Homo2 = log((1 - FIS) * (1 - p)^2 + FIS * (1 - p));
  
  LL <- genoFreq_Homo * D[,1] + genoFreq_Het * D[,2] + genoFreq_Homo2 * D[,3];
  LL[p == 0] <- -99999999999
  LL[p == 1] <- -99999999999
  
  
  return(LL);
}

#--------------
#total log likelihood
#--------------
calculateTotalLogLikelihood <- function(FIS, d, p){
  if(ncol(d) != length(p)) stop("not same amount of allele frequencies as sites in data!")
  ll <- 0
  for(s in 1:ncol(d)){
    ll = ll + calculateLogLLOneSite(FIS=FIS, d[,s], p[s])
  }
  return(ll)
}

calculateTotalLogLikelihoodAlternative <- function(FIS, D, p){
  ll <- 0
  for(s in 1:nrow(D)){
    ll = ll + calculateLogLLOneSiteAlternative(FIS, D[s,], p[s])
  }
  return(ll)
}

#--------------
#initialize 
#--------------
if(runMCMC){
print("Initialize ...")
FIS_values_vec <- numeric(n_runs);
a_values_vec <- numeric(n_runs);
p_values_matrix <- matrix(data=NA, ncol=nrow(D), nrow=n_runs)

total_log_likelihood <- numeric(n_runs);

#initialize chain
FIS_values_vec[1] <- 0.00;
a_values_vec[1] <- 1.0;
current_p <- (D[,1]*2 + D[,2])/(2*rowSums(D));

old_LL_p <- calculateLogLLAllSitesFast(FIS_values_vec[1], D, current_p);
new_LL_p <- numeric(nrow(D));
total_log_likelihood[1] <- sum(old_LL_p);
#FIS_acceptance[1] <- 0

p_values_matrix[1,] <- current_p
#for(s in 1:ncol(d)){
#  p_likelihood[1,s] = calculateLogLLOneSite(FIS_values_vec[1], d[,s], current_p[s]) 
#}
#p_acceptance[1,] <- rep(0, ncol(d))

#--------------
#run MCMC
#--------------
  print("in MCMC")
  for(i in 2:n_runs){
    #propose new FIS
     new_FIS <- rnorm(1, mean=FIS_values_vec[i-1], sd=SD_F)
     if(new_FIS < 0){
       new_FIS <- -new_FIS;
     }
     if(new_FIS > 1){
       new_FIS <- 2-new_FIS;
     }
     new_LL_p <- calculateLogLLAllSitesFast(new_FIS, D, current_p);
     
    #prior
     pr <- dexp(new_FIS, lambda_FIS, log=TRUE) - dexp(FIS_values_vec[i-1], lambda_FIS, log=TRUE);
     
    # #calculate h
     h <- pr + sum(new_LL_p) - total_log_likelihood[i-1];

    # #accept or reject
     if(log(runif(1)) < h){
       FIS_values_vec[i] <- new_FIS;
       old_LL_p <- new_LL_p;
     } else{
       FIS_values_vec[i] <- FIS_values_vec[i-1];
     }
    
    #------------------
    #update allele freq
    #------------------
    new_p <- abs(rnorm(length(current_p), mean=current_p, sd=SD_p));
    new_p[new_p>1] <- 2 - new_p[new_p>1];
    new_p[new_p<0] <- -new_p[new_p<0];

    new_LL_p <- calculateLogLLAllSitesFast(FIS_values_vec[i], D, new_p);
     
    #calculate prior
    pr <- dbeta(new_p, a_values_vec[i-1], a_values_vec[i-1], log=TRUE) - dbeta(p_values_matrix[i-1,], a_values_vec[i-1], a_values_vec[i-1], log=TRUE);
     
    #calculate h
    h <- pr + new_LL_p - old_LL_p;

    #accept or reject
    r <- log(runif(nrow(D)))
    accept <- r <= h
    reject <- !accept
    
    #update
    current_p[accept] <- new_p[accept]
    old_LL_p[accept] <- new_LL_p[accept];

    p_values_matrix[i,] <- current_p;
    total_log_likelihood[i] <- sum(old_LL_p[reject]) + sum(new_LL_p[accept]);
    
    #------------------
    #update a
    #------------------
    new_a <- rnorm(1, mean=a_values_vec[i-1], sd=SD_a);
    if(new_a < 0){
      new_a <- -new_a;
    }
    if(new_a > 1){
      new_a <- 2-new_a;
    }
    
    #prior
    pr <- dexp(1-new_a, lambda_a, log=TRUE) - dexp(1-a_values_vec[i-1], lambda_a, log=TRUE);
    
    #h
    h <- pr + sum(dbeta(p_values_matrix[i,], new_a, new_a, log=TRUE)) - sum(dbeta(p_values_matrix[i,], a_values_vec[i-1], a_values_vec[i-1], log=TRUE));
    if(log(runif(1)) < h){
      a_values_vec[i] <- new_a;
    } else {
      a_values_vec[i] <- a_values_vec[i-1];
    }
    
    #report progress
    if(i %% 100 == 0){print(paste("i",i))}
  }
}


#--------------------------------
#PLOT
#--------------------------------
plotSpecialBurnin <- function(x, ylim, ylab, burnin, n_runs, bplotfactor, lwd){
  #precalc stuff
  b_len <- 2*burnin*bplotfactor + (n_runs-2*burnin);
  b_change <- 2*burnin*bplotfactor;
  
  #now plot
  plot(0, type='n', ylim=ylim, xlim=c(0,b_len), ylab=ylab, xlab="Iteration", xaxt='n');
  lines(rep(b_change, 2), par("usr")[3:4], lty="11", lwd=lwd);
  lines(1 + (0:(burnin-1) * bplotfactor), x[1:burnin], col='black', lwd=lwd);
  lines(1 + (burnin:(2*burnin) * bplotfactor), x[burnin:(2*burnin)], col='red', lwd=lwd);
  lines((2*burnin):n_runs + 2*burnin*(bplotfactor-1), x[(2*burnin):n_runs], col='red', lwd=lwd);

  #add axis
  lab <- seq(0, 2*burnin, length.out=3);
  at <- lab*bplotfactor;
  axis(side=1, at=at, labels=lab);
  lab <- c(n_runs/2, n_runs);
  at <- 2*burnin*(bplotfactor-1) + lab;
  print(at);
  axis(side=1, at=at, labels=lab);
}

bplotfactor <- 10;
F_lim <- 0.3;
myLwd <- 1.5;

pdf("Posterior_F.pdf", width=8, height=4.5);
par(mar=c(4,4,1,1), las=1, xaxs='i', yaxs='i', mgp=c(2.5,0.85, 0));
#layout(matrix(c(1, 2, 3, 4, 5, 5), ncol=3, byrow=FALSE), widths=c(2,1,2));
layout(matrix(c(1, 2, 3, 4), ncol=2, byrow=FALSE), widths=c(2,1));

#plot trace of F
#plot(1:burnin, FIS_values_vec[1:burnin], type='l', ylim=c(0,0.3), xlim=c(1,n_runs), ylab="F", xlab="Iteration", xaxt='n');
#lines(burnin:n_runs, FIS_values_vec[burnin:n_runs], col='red');
plotSpecialBurnin(FIS_values_vec, ylim=c(0,F_lim), "F", burnin=burnin, n_runs=n_runs, bplotfactor=bplotfactor, myLwd);
lines(par("usr")[1:2], rep(sim_FIS, 2), lty="32", lwd=myLwd);

#plot trace of a
#plot(1:burnin, a_values_vec[1:burnin], type='l', ylim=c(0,1), xlim=c(1,n_runs), ylab="a", xlab="Iteration");
#lines(burnin:n_runs, a_values_vec[burnin:n_runs], col='red');
plotSpecialBurnin(a_values_vec, ylim=c(0.2,1.0), "a", burnin=burnin, n_runs=n_runs, bplotfactor=bplotfactor, myLwd);
lines(par("usr")[1:2], rep(beta_a, 2), lty="32", lwd=myLwd);

#plot posterior of F
dens <- density(FIS_values_vec[burnin:n_runs]);
plot(dens, xlim=c(0,F_lim), col='red', main="", ylab="Posterior density", xlab="F", ylim=c(0, max(dens$y)*1.05), lwd=myLwd);
lines(rep(sim_FIS, 2), par("usr")[3:4], lty="32", lwd=myLwd);
#add prior
x <- seq(0, F_lim, length.out=100);
prior_y <- dexp(x, lambda_FIS) / pexp(1.0, lambda_FIS);
lines(x, prior_y, col='black', lwd=myLwd)

#plot posterior of a
dens <- density(a_values_vec[burnin:n_runs]);
plot(dens, xlim=c(0.2,1.0), col='red', main="", ylab="Posterior density", xlab="a", ylim=c(0, max(dens$y)*1.05), lwd=myLwd);
lines(rep(beta_a, 2), par("usr")[3:4], lty="32", lwd=myLwd);
#add prior
x <- seq(0, 1, length.out=100);
prior_y <- dexp(1-x, lambda_a) / pexp(1.0, lambda_a);
lines(x, prior_y, col='black', lwd=myLwd)

#plot posteriors of p
#plot(sim_p, colMeans(p_values_matrix[burnin:n_runs,]), pch=19, col='red', xlab="True p", ylab="Posterior Mean of p", xlim=c(0,1), ylim=c(0,1), cex=0.7);
#lines(par("usr")[1:2], par("usr")[3:4], lty=1, lwd=1.5, col='black');
dev.off();

#print point estimates
print(paste("Posterior mean F", mean(FIS_values_vec[burnin:n_runs])));
print(paste("Posterior mean a", mean(a_values_vec[burnin:n_runs])));