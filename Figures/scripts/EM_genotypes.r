#set variables
N <- 20; #number of individuals
f <- 0.7; #frequency of reference allele used in simulation
e <- 0.05; #error rate use din simulations
n <- 5; #number of reads per individual such that n=r+a. We assume all individuals have the same number of reads
len <- 10 #length of EM = number of iterations.
rep <- 5; #number of replicates from random starting points
randomStart <- T; #choose starting points at random

#-----------------------------------------
#Function to simulate data
#-----------------------------------------
simReads<-function(N, f, e, n){
  #first simulate genotypes using binomial sampling
  g<-rbinom(N, 2, f);
  #simulate number of reference reads without error as binomial sampling with probability g/2
  r<-rbinom(N, n, g/2)
  #add errors: estimate number of r->a and a->r changes and add total to number of reference reads
  r <- r - (rbinom(N, r, e)) + (rbinom(N, n-r, e))
  return(r);
}

#-----------------------------------------
#Functions for likelihood
#-----------------------------------------
Lik<-function(g, r, a, f, e){
  return((g*(1-2*e)+2*e)^r * (g*(2*e-1)-2*e+2)^a * choose(2,g) * f^g*(1-f)^(2-g) * 2^(-a-r));
}

LL_fast<-function(r,a,f,e){      
  res <- sum(log( rowSums(matrix(data=Lik(rep(0:2,length(r)), rep(r, each=3),rep(a, each=3),f,e), ncol=3, byrow=T))));  
  return(res);  
}

calcLLSurface <- function(r, n, f_vec, eps_vec){
  a <- n - r;
  
  res <- matrix(0, nrow=length(f_vec), ncol=length(eps_vec));
  for(i in 1:length(f_vec)){
    for(j in 1:length(eps_vec)){
      res[i,j] <- LL_fast(r,a,f_vec[i], eps_vec[j]);
    }
  }
  
  return(res);
}


#-----------------------------------------
#Function to run EM
#-----------------------------------------
P_g_above<-function(g, r, a, f, e){
  return((g*(1-2*e)+2*e)^r * (g*(2*e-1)-2*e+2)^a * choose(2,g) * f^g*(1-f)^(2-g));
}

P_g_below<-function(r, a, f, e){
  return(2^(r+a) * (e^r * (1-e)^a * (1-f)^2 + (1-e)^r * e^a * f^2) + 2*f*(1-f));         
}

P_g<-function(g,r,a,f,e){
  return(P_g_above(g,r,a,f,e)/P_g_below(r,a,f,e));
}

P_g_above_sum_mult_g<-function(r,a,f,e){
  res<-numeric(length(r));  
  for(i in 1:length(r)){   
    res[i]<-sum(1:2 * P_g_above(1:2, r[i],a[i],f,e));
  }  
  return(res);  
}

P_g_above_sum_mult_g_fast<-function(r,a,f,e){      
  res<-rowSums(matrix(data=(1:2)*P_g_above(rep(1:2,length(r)), rep(r, each=2),rep(a, each=2),f,e), ncol=2, byrow=T));  
  return(res);  
}


runEM<-function(r, n, f_start, e_start, len=20, plot=F, col='black'){
  #r and a are vectors!
  a<-n-r;
  N<-length(r);
  len<-len+1;
  
  #prepare storage
  f<-numeric(len); f[1]<-f_start;  
  e<-numeric(len); e[1]<-e_start;  
  LL <- numeric(len); LL[1] <- LL_fast(r, a, f[1], e[1]);
  
  #run EM
  numIt=len;
  for(l in 2:len){    
    old <- l-1;
    tmp<-P_g_below(r, a, f[old], e[old]);
    
    #calculate new f        
    f[l] <- sum( P_g_above_sum_mult_g_fast(r,a,f[old], e[old])/tmp ) / 2/N;            
    
    #calculate epsilon    
    tmp0 <- P_g(0,r,a,f[old],e[old]);
    tmp2 <- P_g(2,r,a,f[old],e[old]);    
    e[l]<-sum(r*tmp0 + a*tmp2)/sum((a+r)*(tmp0+tmp2));
    
    #calc LL
    LL[l] <- LL_fast(r, a, f[l], e[l]);
    
    #if(round(e[l], 8)==round(e[old],8) & round(f[l],8)==round(f[old],8)){ 
    #  print(paste("Required", l, "iterations"));
    #  numIt=l;
    #  break;
    #}
    numIt <- l;
  }  

  #return
  return(list(numIt=numIt, f=f, e=e, LL=LL));
}

plotEM <- function(EM, col='black'){
  #points(f[numIt], e[numIt], pch=19, col=col)
  lines(EM$f[1:EM[['numIt']]], EM[['e']][1:EM[['numIt']]], col=col, type='l', lwd=1.5);
  points(EM$f[2:EM[['numIt']]], EM[['e']][2:EM[['numIt']]], col=col, pch=19, cex=0.65);
  points(EM$f[1], EM[['e']][1], pch=15, cex=0.9, col=col);
}

#-----------------------------------------
#Simulate data, run EM and plot
#-----------------------------------------
#simulate data
r<-simReads(N, f, e, n);

#init plot
pdf("EM_genotypes.pdf", width=8, height=4)
par(xaxs='i', yaxs='i', mfrow=c(1,2), pty='s', mar=c(3.5,3.75,0.2,1), mgp=c(2.5,0.66, 0), las=1);
plot(0, type='n', xlab="Frequency f", ylab="Error rate epsilon", xlim=c(0,1), ylim=c(0,1));

#add contour of likelihood
nPoints <- 200;
f_vec <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
eps_vec <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
LLsurf <- calcLLSurface(r, n, f_vec, eps_vec);
contour(f_vec, eps_vec, LLsurf, zlim=c(max(LLsurf)-15, max(LLsurf)), nlevels=11, add=TRUE, col='black', drawlabels=FALSE, lty=1, lwd=0.5);

#add MLE position


#run EM and plot
col <- c("black", "dodgerblue", "red", "purple", "orange2");
maxIt <- 0;
minLL <- 0;

#if(randomStart){
  #start <- matrix(runif(2*rep), ncol=2);
  start <- matrix(c(0.55, 0.4, 0.155,0.075,0.93,0.52,0.18,0.355, 0.61, 0.86), ncol=2, byrow=TRUE);
  EM <- list();
  for(i in 1:rep){
    EM[[i]] <- runEM(r, n, start[i,1], start[i,2], len);
    plotEM(EM[[i]], col=col[i]);
    if(EM[[i]]$numIt > maxIt){
      maxIt <- EM[[i]]$numIt;
    }
    if(max(EM[[i]]$LL) > maxLL){
      maxLL <- max(EM[[i]]$LL);
    }
    if(min(EM[[i]]$LL) < minLL){
      minLL <- min(EM[[i]]$LL);
    }
  }
  
  #get mxLL by running one EM for very long
  maxLL <- max(runEM(r, n, start[i,1], start[i,2], 1000)$LL);
#} else {
#  nstep<-round(sqrt(rep))-1;
#  start_f<-rep(0:nstep * 1/nstep, nstep+1)
#  start_e<-rep(0:nstep * 1/nstep, each=nstep+1)
#  for(i in 1:rep){  
#    res<-runEM(r, n, start_f[i], start_e[i], len, plot=T);
#  }
#}

#plot LL traces
par(xaxs='r', yaxs='i')
#plot(0, type='n', xlim=c(0,maxIt-1), ylim=c(minLL, maxLL), xlab="Iteration", ylab="LL(f,e)");
plot(0, type='n', xlim=c(0,maxIt-1), ylim=c(-80, -55), xlab="Iteration", ylab="LL(f,e)");
lines(par("usr")[1:2], rep(maxLL, 2), lty=2)
for(i in 1:length(EM)){
  lines(0:(EM[[i]]$numIt-1), EM[[i]]$LL[1:EM[[i]]$numIt], col=col[i], lwd=1.5);
  points(1:(EM[[i]]$numIt-1), EM[[i]]$LL[2:EM[[i]]$numIt], col=col[i], cex=0.65, pch=19);
  points(0, EM[[i]]$LL[1], col=col[i], cex=0.8, pch=15);
}
dev.off();

