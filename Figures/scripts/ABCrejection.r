N <- 25;
f <- 0.33;
alpha <- 0.5;
beta <- alpha;

abcLen <- 5000000;
epsilon <- c(0, 5, 10, 15);
col <- c("dodgerblue", "purple", "orange2", "red");
numRetained <- 5000;
numPMCIterations <- 4;
doSims <- FALSE;

#-----------------------------
#function to simulate data
#-----------------------------
simulate <- function(N, f){
  g <- 0:2;
  p <- choose(2,g) * f^(2-g) * (1-f)^g;
  if(sum(p<0) > 0){
    print(f);
    print(p);
  }
  G <- c(sample(0:2, N, replace=TRUE, prob=p), g); #add a 0, 1 and 2 to make sure each category exists
  return(tabulate(G+1)-1);
};

simulateMany <- function(N, fPrime){
  nPrime <- sapply(fPrime, simulate, N=N);
  return(data.frame(f=fPrime, n_0=nPrime[1,], n_1=nPrime[2,], n_2=nPrime[3,]));
};

simFromPrior <- function(len, N, alpha, beta){
  fPrime <- rbeta(len, alpha, beta);
  return(simulateMany(N, fPrime));
};



#-----------------------------
#function to get analytical posterior
#-----------------------------
posterior <- function(n, x, alpha, beta){
  return(dbeta(x, alpha + 2*n[1] + n[2] + 1, beta + n[2] + 2*n[3] + 1));
}

L1 <- function(x, trueP, abcP){
  return(sum(abs(trueP - abcP)) * (x[2]-x[1]) / 2);
}

#-----------------------------
#function to do ABC
#-----------------------------
calcDistances <- function(n, abcRes){
  #d <- colSums(abs(t(abcRes[,2:4]) - n));
  d <- sqrt(colSums((t(abcRes[,2:4]) - n)^2));
  
  return(d);
}

doRegression <- function(n, retained, sd=0.0001, nPoints=200){
  x <- seq(0, 1, length.out=nPoints);
  
  #prepare matrices
  numSim <- length(retained[,1]);
  P <- matrix(retained$f, nrow=1);
  X <- matrix(c(rep(1, numSim), retained$f), byrow=FALSE, ncol=2);
  S <- as.matrix(t(retained[,2:4]));
  
  #fit glm
  C_c0 <- S %*% X %*% solve(t(X) %*% X);
  c0 <- C_c0[,1];
  C <- C_c0[,2];
  R <- t(S) - X %*% t(C_c0);
  Sigma_s <- 1/(numSim - 1) * (t(R) %*% R)
  
  #calculate posterior
  Sigma_s_inv <- solve(Sigma_s + diag(0.01,3));
  Sigma_theta_inv <- matrix(1/sd);
  T <- solve(t(C) %*% Sigma_s_inv %*% C + Sigma_theta_inv);
  T_inv <- solve(T);
  
  c_j <- numeric(numSim);
  t_j <- numeric(numSim);
  for(j in 1:numSim){
    v_j <- t(C) %*% Sigma_s_inv %*% (n - c0) + Sigma_theta_inv * retained$f[j];
    t_j[j] <- T %*% v_j;
    c_j[j] <- -0.5 * (retained$f[j] * Sigma_theta_inv * retained$f[j] - t(v_j) %*% T %*% v_j);
  }
  c_j <- exp(c_j - max(c_j));
  
  post <- numeric(nPoints);
  for(i in 1:nPoints){
    aa <- x[i]-t_j;
    post[i] <- sum(c_j * exp(-0.5 * aa * as.numeric(T_inv) * aa));
  }
  
  #normalize
  d <- 1/(nPoints-1);
  A <- sum(post[2:(nPoints-1)]) * d + (post[1] + post[nPoints]) * d/2;
  post <- post / A;
  
  #return
  return(data.frame(x=x, post=post));
  
}

runPMC <- function(n, numRetained, epsilon, N, alpha, beta, numIter=4){
  numSim <- numRetained / epsilon;
  res <- list();

  #sample initial sims
  res[[1]] <- data.frame(f=simFromPrior(numRetained, N, alpha, beta)$f, w=rep(1/numRetained, numRetained));

  #now iterate
  for(i in 2:(numIter+1)){
    #cal weighted var
    old <- i-1;
    wMean <- sum(res[[old]]$f * res[[old]]$w);
    wVar <- sum((res[[old]]$f - wMean)^2 * res[[old]]$w);
    T2 <- 2 * wVar;
    T <- sqrt(T2);
    
    #sample new f
    new_f <- rnorm(numSim, mean=sample(res[[old]]$f, numSim, replace=TRUE, prob=res[[old]]$w), sd=T);
    new_f <- new_f[new_f > 0 & new_f < 1];
    
    #reject
    sim <- simulateMany(N, new_f);
    d <- sort(calcDistances(n, sim), index.return=TRUE);
    keep <- d$ix[1:numRetained];
    
    res[[i]] <- data.frame(f=new_f[keep], w=numeric(numRetained));
    for(j in 1:numRetained){
      res[[i]]$w[j] = dbeta(res[[i]]$f[j], alpha, beta) / sum(dnorm(res[[i]]$f[j], mean=res[[old]]$f, sd=T) * res[[old]]$w);
    }
    res[[i]]$w <- res[[i]]$w / sum(res[[i]]$w);
  }
  
  #return
  return(res);
}

#-----------------------------
#run analysis
#-----------------------------
#simulate data
#n <- simulate(N, f);
n <- c(2, 11, 12);

#ABC simulations
if(doSims){
  abcRes <- simFromPrior(abcLen, N, alpha, beta);
}
d <- calcDistances(n, abcRes);

#retain
ret <- list(); reg <- list();
accRate <- numeric(length(epsilon));
for(e in 1:length(epsilon)){
  if(sum(d <= epsilon[e]) < numRetained){
    print(paste("Not sufficient sims for epsilon", epsilon[e], "!"));
  }
  accRate[e] <- sum(d <= epsilon[e]) / abcLen;
  ret[[e]] <- abcRes[d <= epsilon[e],];
  ret[[e]] <- ret[[e]][1:numRetained,];
  if(epsilon[e] > 0){
    reg[[e]] <- doRegression(n, ret[[e]], sd=0.0002);
  }
}

#run PMC
#pmc <- runPMC(n, numRetained, 0.25, N, alpha, beta, numIter=numPMCIterations);

#---------------------------
#plot
#---------------------------
#get true posterior and prior
x <- seq(0, 1, length.out=1000);
trueP <- posterior(n, x, alpha, beta);
prior <- dbeta(x, alpha, beta);

makePlot <- function(x, trueP, prior){
  plot(0, type='n', xlim=c(0,1), ylim=c(0, max(trueP)*1.05), xlab="Alelle frequency f", ylab="Density");
  lines(x, prior, lwd=1.25, col='black', lty=2);
  polygon(c(x, x[1]), c(trueP, trueP[1]), border=NA, col='gray92');
  #lines(rep(f, 2), par("usr")[3:4], col='black', lty=2);
  
  lines(x, trueP, lwd=1.25, col='black');
}

#open plot
pdf("ABC_figure.pdf", width=10, height=3)
par(mfrow=c(1,3), xaxs='i', yaxs='i', las=1, mgp=c(2.5,0.66, 0), mar=c(3.5,4,0.1,1));

#------------------------------------
#First plot with rejection algo
#------------------------------------
makePlot(x, trueP, prior);

#add ABC estimates
for(e in 1:length(epsilon)){
  dens <- density(ret[[e]]$f, adjust=1.25);
  lines(dens$x, dens$y, col=col[e], lwd=1.5, lty=1);
}

#legend
legend('topright', bty='n', col=c("black", "black", col), lwd=c(1.25, 1.25, rep(1.5, length(epsilon))), lty=c(2, 1, rep(1, length(epsilon))), legend=c("Prior", "Posterior", paste("ABC with eps=", epsilon, " (", round(accRate*100,1), "%)", sep="")));

#------------------------------------
#Second plot with PMC
#------------------------------------
makePlot(x, trueP, prior);

#add PMC iterations
col_pmc <- rainbow(numPMCIterations, start=4/6, end=0);
col_pmc <- c("dodgerblue", "dodgerblue", "dodgerblue", "dodgerblue");
lty_pmc <- c("12", "33", "42", "solid");
for(i in 1:numPMCIterations){
  lines(density(pmc[[i+1]]$f, adjust=1.25), col=col_pmc[i], lwd=1.5, lty=lty_pmc[i]);
}

legend('topright', bty='n', col=col_pmc, lwd=1.5, lty=lty_pmc, legend=lty_pmc);

#------------------------------------
#Third plot with regression results
#------------------------------------
makePlot(x, trueP, prior);

#add ABC estimates
for(e in length(epsilon):2){
  lines(reg[[e]]$x, reg[[e]]$post, col=col[e], lwd=1.5, lty=1);
}

dev.off();

