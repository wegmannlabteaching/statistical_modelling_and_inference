

n <- 2:25;
rep <- 100000;

mle <- matrix(0, ncol=length(n), nrow=rep);
corrected <- matrix(0, ncol=length(n), nrow=rep);

quant_mle <- matrix(0, nrow=length(n), ncol=5);
quant_corrected <- matrix(0, nrow=length(n), ncol=5);

mean_mle <- numeric(length(n));
mean_corrected <- numeric(length(n));

for(i in 1:length(n)){
  d <- matrix(rnorm(n[i]*rep), ncol=n[i]);
  m <- rowMeans(d);
  mle[,i] <- rowSums((d - m)^2) / n[i];
  corrected[,i] <- rowSums((d - m)^2) / (n[i]-1);
  
  quant_mle[i,] <- quantile(mle[,i], probs=c(0.995, 0.95, 0.5, 0.05, 0.005));
  quant_corrected[i,] <- quantile(corrected[,i], probs=c(0.995, 0.95, 0.5, 0.05, 0.005));
  
  mean_mle[i] <- mean(mle[,i]);
  mean_corrected[i] <- mean(corrected[,i]);
}


pdf("VarianceEstimation.pdf", width=4, height=5);
par(mar=c(4,4,0.1,0.1));
plot(0, type='n', xlim=range(n), ylim=c(0,3), xlab="Sample size", ylab="Estimated variance");

#plot MLE
x <- c(n, rev(n), n[1]);
y <- c(quant_mle[,2], rev(quant_mle[,4]), quant_mle[1,2]);
#polygon(x, y, col='lightgray', border='lightgray', density=100);
#polygon(x, y, col='lightgray', density=10);

y <- c(quant_corrected[,2], rev(quant_corrected[,4]), quant_corrected[1,2]);
#polygon(x, y, col='red', density=10, angle=-45);

lines(n, quant_mle[,3], lwd=2, col='black', lty=2);
lines(n, mean_mle, lwd=2, col='black', lty=1);
lines(n, quant_mle[,2], lwd=1, col='black', lty=3);
lines(n, quant_mle[,4], lwd=1, col='black', lty=3);


lines(n, quant_corrected[,3], lwd=2, col='red', lty=2);
lines(n, mean_corrected, lwd=2, col='red', lty=1);
lines(n, quant_corrected[,2], lwd=1, col='red', lty=3);
lines(n, quant_corrected[,4], lwd=1, col='red', lty=3);

legend('topright', legend=c("MLE", "Bessel's correction"), col=c('black', 'red'), lwd=2, bty='n');

dev.off();

