library(shape);

F <- function(d, f, eps){
  prob <- (1-2*eps)*(f*(2*d-1) - d) + 1 - eps;
  return(sum(log(prob)));
}

Fp <- function(d, f, eps){
    x <- (1-2*eps)*(2*d-1) / ((1-2*eps)*(f*(2*d-1) - d) + 1 - eps);
    return(sum(x));
}

Fpp <- function(d, f, eps){
  x <- (1-2*eps)*(2*d-1) / ((1-2*eps)*(f*(2*d-1) - d) + 1 - eps);
  return(sum(-x*x));
}

#simulate
doSims <- FALSE;
if(doSims){
  n <- 200;
  #eps <- runif(n) * 0.2;
  eps <- rep(0.1, n);
  f <- 0.3;
  g <- runif(n) < f;
  err <- runif(n) < eps;
  d <- (g + err) %% 2;
}

#calc functions
len <- 1000;
fx <- seq(0, 1, length.out=len);
fy <- numeric(len);
fyp <- numeric(len);
fypp <- numeric(len);
for(i in 1:len){
  fy[i] <- F(d, fx[i], eps);
  fyp[i] <- Fp(d, fx[i], eps);
  fypp[i] <- Fpp(d, fx[i], eps);
}

#make plot
#par(mfrow=c(3,1));
#plot(fx, fy, type='l');
#plot(fx, fyp, type='l');
#plot(fx, fypp, type='l');

topY <- 0.1;

pdf("NewtonRaphson.pdf", width=6, height=4)
par(mfrow=c(1,1), xaxs='i', yaxs='i', las=1, mar=c(4,4,0.1,0.1));
range <- which(fx > f-0.24 & fx<f+0.04);
plot(fx[range], fyp[range], type='l', lwd=2);
lines(par("usr")[1:2], rep(0,2), lty=2);

fstart <- 0.15;
fstart_yp <- Fp(d, fstart, eps);
#lines(rep(fstart,2), c(0, fstart_yp), lty=2, col='red');



fstart_ypp <- Fpp(d, fstart, eps);

points(fstart, fstart_yp, pch=19, col='red')
intercept <- fstart_yp - fstart_ypp*fstart;
abline(intercept, fstart_ypp, col='red');

intersection <- -intercept / fstart_ypp;
fp_intersection <- Fp(d, intersection, eps);
#lines(rep(intersection,2), c(0, fp_intersection), lty=2, col='red');

#points(intersection, fp_intersection, pch=19, col='red')



#second step
#fpp_intersection <- Fpp(d, intersection, eps);
#intercept2 <- fp_intersection - fpp_intersection*intersection;
#abline(intercept2, fpp_intersection, col='red');



#vertical lines
verticalLineCoords <- c(par("usr")[3], par("usr")[4] - (par("usr")[4]-par("usr")[3])*topY);
lines(rep(fstart,2), verticalLineCoords, lty=2, col='red');
lines(rep(intersection,2), verticalLineCoords, lty=2, col='red');
mle <- fx[which(abs(fyp) == min(abs(fyp)))];
lines(rep(mle,2), verticalLineCoords, lty=2, col='black');

labelY <- par("usr")[4] - (par("usr")[4]-par("usr")[3])*topY/2;
text(fstart, labelY, labels=expression(theta[1]), col='red');
text(intersection, labelY, labels=expression(theta[2]), col='red');
text(mle, labelY, labels=expression(theta^"*"));


y <- 150;
Arrows(fstart, y, intersection, y, code=2, arr.type="triangle", arr.adj=1, lwd=1.2, col='red')

dev.off();
