

f <- read.table("results_mig.txt", header=FALSE, skip=1)

pdf("NumericalIntegration.pdf", width=8, height=3.5)
par(mfrow=c(1,2), las=1, mgp=c(2.5,0.66, 0), mar=c(3.5,4,0.1,1))

#density of LL of one genealogy
one <- read.table("one/LL_migration_one.txt.gz", header=FALSE, skip=1);

x <- seq(min(one[,3]), max(one[,3]), length.out=100);
y <- numeric(length(x));
for(i in 1:length(x)){
  y[i] <- sum(one[,3] >= x[i]) / length(one[,3]);
}
plot(x, log10(y), type='l', lwd=1.5, ylim=c(-7,0), xlim=c(-180, -60))

#get average
mm <- max(one[,3]);
average <- log(mean(exp(one[,3]-mm)))+mm;

#add boxplot of max LL
boxplot(f[,2]~f[,1], ylab="hat L(theta)", xlab="Number of samples genealogies k")
lines(par("usr")[1:2], rep(average, 2), lty=2, col='red')
dev.off()