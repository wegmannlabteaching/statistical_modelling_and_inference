#!/bin/bash

nReps=100
numGenealogies="1 10 100 1000 10000 100000"
numGenealogies="1000000"

#create results file
resFile="results_mig.txt"
#echo -e "numGenealogies\tLL" > $resFile

#Loop
for ng in $numGenealogies; do
	for i in `seq 1 1 $nReps`; do
		rm -r migration
		./fsc25221 -t migration.tpl -n 1 -N 1 -d -e migration.est -M 0.001 -c 1 -l $ng -L $ng -q
		Rscript getMeanLL.r migration/migration.brent_lhoods > tmp.txt
		cat tmp.txt | awk -v ng=$ng '{print ng, $2}' | head -n $nReps >> $resFile
	done
done

