file <- commandArgs(trailingOnly = TRUE)[1];
#print(paste("Will read '", file, "'", sep=""));
a <- read.table(file, header=FALSE, skip=1);
mm <- max(a[,3]);
print(log(mean(exp(a[,3]-mm)))+mm);
