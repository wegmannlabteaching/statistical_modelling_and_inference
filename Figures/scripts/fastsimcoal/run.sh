#!/bin/bash

nReps=100
numGenealogies="1 10 100 1000 10000 100000"

#create results file
resFile="results_5000.txt"
echo -e "numGenealogies\tLL" > $resFile

#Loop
for ng in $numGenealogies; do
	rm -r constSize
	./fsc25221 -t constSize.tpl -n $ng -N $ng -d -e constSize.est -M 0.001 -c 1 -l $nReps -L $nReps
	tail -n+2 constSize/constSize.brent_lhoods | awk -v ng=$ng '{print ng, $3}' | head -n $nReps >> $resFile
done

