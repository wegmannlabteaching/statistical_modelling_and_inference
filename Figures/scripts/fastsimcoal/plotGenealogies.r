#load library to work with tree is R
library(ape); 

#read trees
#args <- commandArgs(trailingOnly=TRUE);
#t<-read.nexus(args[1]);

t<-read.nexus("/home/phaentu/ownCloud/Teaching/Population Genetics/Lecture 6 - Exercises 4 and Practical Coalescent/Practical_coalescent/constSize/constSize_1_true_trees.trees");

numTrees <- length(t);
print(paste("Plotting ", numTrees, " genealogies from file '", args[1], "'", sep=""));

#get tallest tree to adjust plotting area
maxHeight <- 0;
for(i in 1:numTrees){
  maxThisTree <- max(branching.times(t[[i]]));
  if(maxThisTree > maxHeight){ maxHeight <- maxThisTree; }
}

#plot trees
#pdf("genealogies.pdf", width=8, height=5)
par(mfrow=c(1,numTrees), mar=c(0.5,0.5,0.5,0.5), oma=c(0,3,0,0))
for(i in 1:numTrees){
  #add length of root edge so that all trees align at 0
  #t[[i]]$root.edge <- maxHeight -  max(branching.times(t[[i]]));
  mb <- max(branching.times(t[[i]]));
  t[[i]]$root.edge <- maxHeight - mb;
  
  edge.col <- c("red", rep("blue", length(t[[i]]$edge)), "red", "");
  
  print(sum(max(branching.times(t[[i]]))) + t[[i]]$root.edge);
  
  pop <- as.numeric(unlist(strsplit(t[[i]]$tip.label, "[.]"))[c(FALSE, TRUE)]);
  
  #plot.phylo(t[[i]], direction="downwards", show.tip.label=FALSE, y.lim=c(0, maxHeight), root.edge=TRUE)
  
  plot.phylo(t[[i]], direction="downwards", show.tip.label=FALSE, y.lim=c(0, mb), root.edge=TRUE, edge.color = edge.col);
  tiplabels(pch=19, adj=c(0.6, 0.5), cex=0.3, col=pop);
  if(i==1){ axis(side=2, outer=T); }  
}  
#dev.off();






