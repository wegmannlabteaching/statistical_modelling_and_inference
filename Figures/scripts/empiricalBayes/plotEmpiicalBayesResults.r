#which n?
n <- c(2,10);
col <- c("black", "red", "dodgerblue", "black", "orange2", "red", "purple", "dodgerblue");

#loop over n
res <- list();
ylim <- c(0.01, 0.01);
xlim <- c(100,100);
for(i in 1:length(n)){
  #read data
  res[[i]] <- list();
  res[[i]][["data"]] <- read.table(paste("all_L1_n_", n[i], ".txt", sep=""), header=FALSE);
  
  #get quantiles
  res[[i]][["N"]] <- sort(unique(res[[i]]$data[,1]));
  res[[i]][["N"]] <- res[[i]][["N"]][res[[i]][["N"]]>0];
  num <- length(res[[i]]$N);
  
  res[[i]][["quantiles"]] <- matrix(0, ncol=3, nrow=num);
  for(j in 1:length(res[[i]]$N)){
    res[[i]]$quantiles[j,] <- quantile(res[[i]]$data[res[[i]]$data[,1] == res[[i]]$N[j], 2], probs=c(0.05, 0.5, 0.95)); 
  }
  
  #update ylim
  ylim <- range(c(ylim, res[[i]]$quantiles[,2]));
  xlim <- range(c(xlim, res[[i]]$N));
}

#open plot
pdf("EmpiricalBayes.pdf", width=4, height=4.3);
par(yaxs='r', las=1, mgp=c(2.5,0.66, 0), mar=c(3.5,4,0.1,1), xaxs='r');
plot(0, type='n', ylim=c(-4.0, -0.55), xlim=c(0.04, 2.96), xlab="n", ylab="L1", yaxp=c(-4, -1, 3), xaxp=c(0, 3, 3));
angle <- c(45, -45, 90)

#plot 90 quantile
for(i in 1:length(n)){
    lines(log10(res[[i]]$N), log10(res[[i]]$quantiles[,1]), lty=2, col=col[i]);
    lines(log10(res[[i]]$N), log10(res[[i]]$quantiles[,3]), lty=2, col=col[i]);
    
    x <- log10(c(res[[i]]$N, rev(res[[i]]$N), res[[i]]$N[1]));
    y <- log10(c(res[[i]]$quantiles[,1], rev(res[[i]]$quantiles[,3]), res[[i]]$quantiles[1,1]));
    
    
    cc <- as.numeric(col2rgb(col[i]));
    cc <- rgb(cc[1], cc[2], cc[3], 60, maxColorValue=255)
    
    #polygon(x, y, col=col[i], border=NA, density=20, angle=angle[i]);
    polygon(x, y, col=cc, border=NA);


}

#plot median
for(i in 1:length(n)){
  lines(log10(res[[i]]$N), log10(res[[i]]$quantiles[,2]), type='b', pch=19, col=col[i])
}

#legend
legend('topright', bty='n', col=col[1:length(n)], pch=19, lwd=1, legend=paste(n, "x", sep=""))

dev.off()