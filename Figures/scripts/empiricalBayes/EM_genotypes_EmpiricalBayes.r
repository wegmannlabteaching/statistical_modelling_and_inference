#set variables
args <- commandArgs(trailingOnly = TRUE);
if(length(args) != 4){
  stop("Need three arguments: 1) allele frequency, 2) number of samples N, 3) depth, 4) number replicates!");
}

f <- as.numeric(args[1]);
N <- as.numeric(args[2]);
n <- as.numeric(args[3]);
rep <- as.numeric(args[4]);
print(paste("Running", rep, "replicates with f", f, ", N", N, "and n", n));

#other params
e <- 0.05; #error rate used in simulations
lenEM <- 10 #length of EM = number of iterations.
lenMCMC <- 100000;
burninMCMC <- 500;

#-----------------------------------------
#Function to simulate data
#-----------------------------------------
simReads<-function(N, f, e, n){
  #first simulate genotypes using binomial sampling
  g<-rbinom(N, 2, f);
  #simulate number of reference reads without error as binomial sampling with probability g/2
  r<-rbinom(N, n, g/2)
  #add errors: estimate number of r->a and a->r changes and add total to number of reference reads
  r <- r - (rbinom(N, r, e)) + (rbinom(N, n-r, e))
  return(list(N=N, n=n, f=f, e=e, g=g, r=r, a=n-r));
}

#-----------------------------------------
#Functions for likelihood
#-----------------------------------------
Lik<-function(g, r, a, f, e){
  return((g*(1-2*e)+2*e)^r * (g*(2*e-1)-2*e+2)^a * choose(2,g) * f^g*(1-f)^(2-g) * 2^(-a-r));
}

LL_fast<-function(r,a,f,e){      
  res <- sum(log( rowSums(matrix(data=Lik(rep(0:2,length(r)), rep(r, each=3),rep(a, each=3),f,e), ncol=3, byrow=T))));  
  return(res);  
}

calcLLSurface <- function(r, n, f_vec, eps_vec){
  a <- n - r;
  
  res <- matrix(0, nrow=length(f_vec), ncol=length(eps_vec));
  for(i in 1:length(f_vec)){
    for(j in 1:length(eps_vec)){
      res[i,j] <- LL_fast(r,a,f_vec[i], eps_vec[j]);
    }
  }
  
  return(res);
}

#-----------------------------------------
#Function to run EM
#-----------------------------------------
P_g_above<-function(g, r, a, f, e){
  return((g*(1-2*e)+2*e)^r * (g*(2*e-1)-2*e+2)^a * choose(2,g) * f^g*(1-f)^(2-g));
}

P_g_below<-function(r, a, f, e){
  return(2^(r+a) * (e^r * (1-e)^a * (1-f)^2 + (1-e)^r * e^a * f^2) + 2*f*(1-f));         
}

P_g<-function(g,r,a,f,e){
  return(P_g_above(g,r,a,f,e)/P_g_below(r,a,f,e));
}

P_g_above_sum_mult_g<-function(r,a,f,e){
  res<-numeric(length(r));  
  for(i in 1:length(r)){   
    res[i]<-sum(1:2 * P_g_above(1:2, r[i],a[i],f,e));
  }  
  return(res);  
}

P_g_above_sum_mult_g_fast<-function(r,a,f,e){      
  res<-rowSums(matrix(data=(1:2)*P_g_above(rep(1:2,length(r)), rep(r, each=2),rep(a, each=2),f,e), ncol=2, byrow=T));  
  return(res);  
}


runEM<-function(sim, e, len=20){
  len<-len+1;
  
  #prepare storage
  f<-numeric(len); 
  
  #estimate initial f
  f[1]<- sum(sim$r) / (sim$n*sim$N);  
  
  #run EM
  numIt=len;
  for(l in 2:len){    
    old <- l-1;
    tmp <- P_g_below(sim$r, sim$a, f[old], e);
    
    #calculate new f        
    f[l] <- sum( P_g_above_sum_mult_g_fast(sim$r, sim$a, f[old], e)/tmp ) / 2/N;            
  
    #if(round(e[l], 8)==round(e[old],8) & round(f[l],8)==round(f[old],8)){ 
    #  print(paste("Required", l, "iterations"));
    #  numIt=l;
    #  break;
    #}
    numIt <- l;
  }  

  #return
  return(list(numIt=numIt, f=f));
}

#-----------------------------------------
#MCMC to get true posterior
#-----------------------------------------
post_tmp<-function(g, r, a, f, e){
  return((g*(1-2*e)+2*e)^r * (g*(2*e-1)-2*e+2)^a * choose(2,g) * f^g*(1-f)^(2-g));
}

posterior_fast<-function(r,a,f,e){      
  res <- matrix(data=post_tmp(rep(0:2,length(r)), rep(r, each=3),rep(a, each=3),f,e), ncol=3, byrow=T);
  return(res / rowSums(res));
}

estimateWithFullPosterior <- function(sim, e, len=20000, burnin=500){
  sd <- 1.0 / (sqrt(sim$N));
  
  #first run MCMC to sample f
  mf <- numeric(len+burnin);
  mf[1]<- sum(sim$r) / (sim$n*sim$N);  
  
  #now run MCMC
  oldLik <- LL_fast(sim$r, sim$a, mf[1], e);    
  for(i in 2:(len+burnin)){
    new_f <- abs(rnorm(1, mean=mf[i-1], sd=sd));
    while(new_f > 1){
      new_f <- abs(2 - new_f);
    }
    newLik <- LL_fast(sim$r, sim$a, new_f, e);      
    if(log(runif(1)) < newLik - oldLik){
      mf[i] <- new_f;
      oldLik <- newLik;
    } else {
      mf[i] <- mf[i-1];
    }
  }
  
  #subsample every tenth after bunrin
  mf <- mf[1:(len/10) * 10 + burnin];

  #now calculate true posteriors as average across sampled f
  res <- matrix(0, ncol=3, nrow=sim$N);
  for(i in 1:length(mf)){
    res <- res + posterior_fast(sim$r, sim$a, mf[1], e);
  }
  res <- res / length(mf);
  return(list(posterior=res, f=mf));
}

#-----------------------------------------
#Empirical Bayes
#-----------------------------------------
empiricalBayes <- function(sim, e, len=20){
  #first run EM
  EM <- runEM(sim, e, len);
  
  #then get posterior at MLE
  post <- posterior_fast(sim$r, sim$a, EM$f[EM$numIt], e);
  
  #return
  return(post);
}


#-----------------------------------------
#Simulate data, and run stuff
#-----------------------------------------
runComparison <- function(N, f, e, n){
  #simulate data
  sim <- simReads(N, f, e, n);
  
  #run empirical Bayes
  postEB <- empiricalBayes(sim, e, lenEM);
  
  #run full posterior
  postFull <- estimateWithFullPosterior(sim, e, lenMCMC, burninMCMC);

  #calc L1 distance
  L1 <- sum(abs(postEB - postFull$posterior)) / 3 / sim$N;
  return(L1);
}

#run replicates
L1 <- data.frame(N=rep(N, rep), L1=numeric(rep));
for(i in 1:length(L1[,1])){
  print(paste("Running comparison", i, "with N", N));
  L1$L1[i] <- runComparison(N, f, e, n);
}

#save
write.table(L1, file=paste("L1_table_f_", f, "_N_", N, "_n_", n, ".txt", sep=""), quote=FALSE, append=TRUE, col.names=FALSE, row.names=FALSE);

#init plot
#col <- c("black", "dodgerblue", "red", "purple", "orange2");
#pdf("EM_genotypes.pdf", width=8, height=4)
#par(xaxs='i', yaxs='i', mfrow=c(1,2), pty='s', mar=c(3.5,3.75,0.2,1), mgp=c(2.5,0.66, 0), las=1);
#plot(0, type='n', xlab="Frequency f", ylab="Error rate epsilon", xlim=c(0,1), ylim=c(0,1));
#dev.off();

