\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{26.5cm}
\setlength\topmargin{-2.5cm}
\linespread{1.3}

\usepackage[withSolutions]{optional}
%\usepackage[withoutSolutions]{optional}

\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.2cm}\\ {\color{red} \texttt{#1}}}\bigskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}

\def\d{\boldsymbol{d}}
\def\t{\boldsymbol{\tau}}

%opening
\title{Machine Learning - Exercises 6}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{Hidden Markov Model - Forward Algorithm}


\begin{enumerate}[1.]
\item A researcher is studying the activity behavior of rodents in the wild. For this purpose, she attached small transmitters that measure and report the heart rate to several individuals. For each individual, the collected data thus consist of a series of heart rate measurements $\d=\{d_1, \ldots, d_T\}$ for time points $t=1, \ldots, T$.

The goal is now to use this data to infer the pattern with which an individual rests or is active using a hidden Markov model. For this we first define the transition probability matrix 
\begin{equation*}
 \t = \begin{pmatrix}
       1-\tau_a & \tau_a\\
       \tau_r & 1-\tau_r
      \end{pmatrix},
\end{equation*}

where $\tau_a$ is the probability to switch from the resting (r) to the active (a) state, and $\tau_r$ to switch in the other direction. We will further assume $d_t \sim {\cal N}(\mu_{s_t}, \sigma)$, that is, that the measured heart rate is normally distributed with standard deviation $\sigma$ and mean $\mu_r$ or $\mu_a$, depending on the hidden state $s_t=r,a$ at time $t$. 

\begin{enumerate}[a)]
\item Given fixed $\tau_a$ and $\tau_r$, what are the stationary probabilities $\P_{\infty}(s_t)$ of this Markov chain? Write a function to calculate these, as we will use them probabilities as the initial probabilities of our Markov chain.\sol{We have \begin{eqnarray*}                                                                                                                                                                                                                       \P_{\infty}(s_t=a) &=& \P_{\infty}(s_t=a)(1-\tau_r) + \P_{\infty}(s_t=r)\tau_a\\
\P_{\infty}(s_t=r) &=& \P_{\infty}(s_t=a)(\tau_r) + \P_{\infty}(s_t=r)(1-\tau_a)\\
\P_{\infty}(s_t=a) + \P_{\infty}(s_t=r) &=& 1,
\end{eqnarray*}
from which we get
\begin{equation*}
 \P_{\infty}(s_t=a) = \frac{\tau_a}{\tau_a + \tau_r}, \quad \P_{\infty}(s_t=r) = \frac{\tau_r}{\tau_a + \tau_r}
\end{equation*}
A corresponding function could look like this:\\
\script{statProb <- function(tau)\{\\
\hspace*{1cm} return(c(tau[1]/sum(tau), tau[2]/sum(tau)));\\
\}}
}

\item Implement a function in R to simulated data under this model. Your function should accept the transition matrix $\t$, a vector $\mu=\{\mu_r, \mu_a\}$, the standard deviation $\sigma$, the stationary probabilities $\P_{\infty}(s_t)$ as well as then number of time points T, and then  \begin{itemize}                                                                                                                                                                                  \item simulate a Markov chain where the first state is distributed according to $\P_{\infty}(s_t)$ and all following states are simulated according to $\t$,
\item simulate the observed data $\d$ according to $d_t \sim {\cal N}(\mu_{s_t}, \sigma)$,
\item Return a data frame with both the hidden states as well as the observed data for each time point.
\end{itemize}
\sol{\script{simulate <- function(Tau, mu, sigma, P\_inf, T)\{\\
\hspace*{1cm}  simulate hidden states\\
\hspace*{1cm}  s = numeric(T);\\
\hspace*{1cm}  s[1] = sample(1:2, 1, prob=P\_inf);\\
\hspace*{1cm}  for(i in 2:T)\{\\
\hspace*{2cm}    s[i] <- sample(1:2, 1, prob=Tau[s[i-1],]);\\
\hspace*{1cm}  \}\\
\hspace*{1cm}  \#simulate data\\
\hspace*{1cm}  d <- rnorm(T, mean=mu[s], sd=sigma);\\
\hspace*{1cm}  \#return\\
\hspace*{1cm}  return(data.frame(s=s, d=d));\\
\}}
}

\item \label{llfunction} Implement a function to calculate the likelihood $\pi(\d|\t, \mu, \sigma)$ using the forward variable $\alpha_t(s_t)$ where
\begin{eqnarray*}
 \alpha_t(s_t) &=& \pi(d_t|s_t) \sum_{x \in{r,a}} \alpha_{t-1}(s_{t-1}=x)\P(s_t|s_{t-1}=x),\\
\alpha_1(s_t) &=&  \pi(d_t|s_t) \P_{\infty}(s_t)
\end{eqnarray*}
and $\pi(d_t|s_t)$ is given by the normal density with mean $\mu_{s_t}$ and standard deviation $\sigma$. Remember that 
\begin{equation*}
 \pi(\d|\t, \mu, \sigma^2) = \sum_{x\in\{r,a\}}\alpha_T(s_t=x).
\end{equation*}\sol{\script{getL <- function(d, Tau, mu, sigma, P\_inf)\{\\
\hspace*{1cm}  alpha <- P\_inf * dnorm(d[1], mu, sigma);\\
\hspace*{1cm}  for(i in 2:length(d))\{\\
\hspace*{2cm}    alpha <- dnorm(d[i], mu, sigma) * alpha \%*\% t(Tau);\\
\hspace*{1cm}  \}\\
\hspace*{1cm}  return(sum(alpha));\\
\}}}

\item Finally, implement a function to infer $\tau_r$ and $\tau_a$ using an MCMC and assuming uniform priors $\tau_r, \tau_a \sim {\cal U}[0,1]$. Your function should accept the data $\d$, a vector $\mu=\{\mu_r, \mu_a\}$, the standard deviation $\sigma$ and the desired length of the MCMC, and return an MCMC chain that samples $\tau_r$ and $\tau_b$ values. Note that in each MCMC iteration, your function will need to calculate the stationary distribution $\P_{\infty}(s_t)$ and then the likelihood using your function from exercise \ref{llfunction}.\sol{\script{runMCMC <- function(d, mu, sigma, len)\{\\
\hspace*{1cm}  mcmc <- matrix(0, nrow=len, ncol=2);
\hspace*{1cm}  \#choose starting values\\
\hspace*{1cm}  mcmc[1,] <- c(0.5, 0.5);\\
\hspace*{1cm}  Tau <- transProb(mcmc[1,]);\\
\hspace*{1cm}  P\_inf <- statProb(mcmc[1,]);\\
\hspace*{1cm}  L <- getL(d, Tau, mu, sigma, P\_inf);\\
\hspace*{1cm}  \\
\hspace*{1cm}  for(i in 2:len)\{\\
\hspace*{2cm}    \#propose move\\
\hspace*{2cm}    prop <- abs(mcmc[i-1,] + rnorm(2, mean=0, sd=0.01));\\
\hspace*{2cm}    prop[prop>1] <- 2 - prop[prop>1];\\
\hspace*{2cm}    \#calc Likelihood\\
\hspace*{2cm}    Tau <- transProb(prop);\\
\hspace*{2cm}    P\_inf <- statProb(prop);\\
\hspace*{2cm}    newL <- (getL(d, Tau, mu, sigma, P\_inf));\\
\hspace*{2cm}    \#accept or reject\\
\hspace*{2cm}    h <- newL - L;\\
\hspace*{2cm}    if(runif(1) < h)\{\\
\hspace*{3cm}      mcmc[i,] <- prop;\\
\hspace*{3cm}      LL <- newLL;\\
\hspace*{2cm}    \} else \{\\
\hspace*{3cm}      mcmc[i,] <- mcmc[i-1,];\\
\hspace*{2cm}    \}\\
\hspace*{1cm}  \}\\
\hspace*{1cm}  return(mcmc);\\
\}}}

\item Now make use of your functions to simulate data for $T=100$, $\mu=\{80, 120\}$, $\sigma^2=10$, $\tau_r=0.1$ and $\tau_a=0.2$, and to infer and plot the two dimensional posterior distributions for $\tau_r$ and $\tau_a$.


\end{enumerate}

\end{enumerate}

\end{document}
