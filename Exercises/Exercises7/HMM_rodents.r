#HMM Rodents

#parameters
T <- 10000;
tau <- c(0.1, 0.2);
mu <- c(80, 120);
sigma <- 10;
mcmcLen <- 5000;

#function to calculate stationary probabilities
statProb <- function(tau){
  return(c(tau[1]/sum(tau), tau[2]/sum(tau)));
}

#function to get tau matrix
transProb <- function(tau){
  return(matrix(c(1-tau[1], tau[1], tau[2], 1-tau[2]), byrow=TRUE, nrow=2));
}

#function to simulate
simulate <- function(Tau, mu, sigma, P_inf, T){
  #simulate hidden states
  s = numeric(T);
  s[1] = sample(1:2, 1, prob=P_inf);
  for(i in 2:T){
    s[i] <- sample(1:2, 1, prob=Tau[s[i-1],]);
  }
  
  #simulate data
  d <- rnorm(T, mean=mu[s], sd=sigma);
  
  #return
  return(data.frame(s=s, d=d));
}

#function to calculate LL
getL <- function(d, Tau, mu, sigma, P_inf){
  alpha <- P_inf * dnorm(d[1], mu, sigma);
  for(i in 2:length(d)){
    alpha <- dnorm(d[i], mu, sigma) * alpha %*% t(Tau);
  }
  return(sum(alpha));
}

getLL <- function(d, Tau, mu, sigma, P_inf){
  alpha <- P_inf * dnorm(d[1], mu, sigma);
  LL <- 0;
  for(i in 2:length(d)){
    alpha <- dnorm(d[i], mu, sigma) * alpha %*% Tau;
    LL <- LL + log(sum(alpha));
    alpha <- alpha / sum(alpha);
  }
  return(LL);
}

#function to run MCMC
runMCMC <- function(d, mu, sigma, len){
  mcmc <- matrix(0, nrow=len, ncol=2);
  
  #choose starting values
  mcmc[1,] <- c(0.5, 0.5);
  Tau <- transProb(mcmc[1,]);
  P_inf <- statProb(mcmc[1,]);
  LL <- getLL(d, Tau, mu, sigma, P_inf);
  
  #loop
  for(i in 2:len){
    #propose move
    prop <- abs(mcmc[i-1,] + rnorm(2, mean=0, sd=0.01));
    prop[prop>1] <- 2 - prop[prop>1];
    
    #calcLL
    Tau <- transProb(prop);
    P_inf <- statProb(prop);
    newLL <- getLL(d, Tau, mu, sigma, P_inf);
    #newLL <- log(getL(d, Tau, mu, sigma, P_inf));
    
    #accept or reject
    h <- newLL - LL;
    if(log(runif(1)) < h){
      mcmc[i,] <- prop;
      LL <- newLL;
    } else {
      mcmc[i,] <- mcmc[i-1,];
    }
  }
  
  #return
  return(mcmc);
}

#now run!
P_inf <- statProb(tau);
Tau <- transProb(tau);
sim <- simulate(Tau, mu, sigma, P_inf, T);
mcmc <- runMCMC(sim$d, mu, sigma, mcmcLen);

#plot
burnin <- 500;
layout(matrix(c(1,1,2,3), nrow=2, byrow=TRUE));
plot(mcmc[,1], type='l', ylim=c(0,1), ylab="tau", xlab="iteration");
lines(mcmc[,2], type='l', col='red');
lines(rep(burnin,2), par("usr")[3:4], lty=2)

plot(density(mcmc[burnin:mcmcLen,1]), col='black', xlab="Tau_r", ylab="posterior density", main="");
lines(rep(tau[1],2), par("usr")[3:4], lty=2)
plot(density(mcmc[burnin:mcmcLen,2]), col='red', xlab="Tau_r", ylab="posterior density", main="");
lines(rep(tau[2],2), par("usr")[3:4], lty=2)

