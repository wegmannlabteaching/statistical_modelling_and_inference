
##################################
# Analytical posterior
##################################

posterior <- function(lambda, Dobs, alpha, beta){
  alpha_prime <- alpha + length(Dobs);
  beta_prime <- beta + sum(Dobs);
  
  #get 
  return(dgamma(lambda, alpha_prime, beta_prime));
}

##################################
# Functions for ABC
##################################

#function to perform simulations
simulate <- function(m, lambda){
  N <- length(lambda);
  return(matrix(rexp(N*m, rate=lambda), nrow=N, byrow=FALSE));
}

#function to calc summary statistics
calcStats <- function(D){
  S <- data.frame("mean" = apply(D, 1, mean),
                  "var"  = apply(D, 1, var),
                  "median" = apply(D, 1, median),
                  "min"    = apply(D, 1, min),
                  "max"    = apply(D, 1, max),
                  "range"  = apply(D, 1, max) - apply(D, 1, min));
  
  #S <- data.frame("mean" = apply(D, 1, mean));
  
  return(S);
}

#function to generate simulations
makeSims <- function(N, m, alpha, beta){
  #draw parameters from prior
  lambda <- rgamma(N, alpha, beta);
  
  #simulate data from normal distribution
  D <- simulate(m, lambda);
  
  #calculate summary statistics
  S <- calcStats(D);
  
  #return
  return(list(P=as.matrix(lambda), S=S));
}

#function to get posterior samples (generic)
getABCPosteriorSamples <- function(Sims, sobs, tolerance){
  #extract stats that are present in sobs
  keep <- match(names(sobs), names(Sims$S));
  S <- Sims$S[,keep, drop=FALSE];
  
  #standardize stats
  for(i in 1:ncol(S)){
    mean <- mean(S[,i]);
    sd <- sd(S[,i]);
    S[,i] <- (S[,i] - mean) / sd;
    sobs[i] <- (sobs[i] - mean) / sd;
  }
  
  #calc euclidian dist
  x <- t(S) - as.numeric(sobs);
  distances <- sqrt(colSums(x*x));
  
  #do rejection
  distancesSorted <- sort(distances, index.return=T);
  bestSims <- distancesSorted$ix[1:round(tolerance*nrow(Sims$P))];
  return(list(P=Sims$P[bestSims,], S=S[bestSims,]));
}

##################################
# Function to plot
##################################

#function to plot posterior distributions
plotPosteriors <- function(d, sobs, alpha, beta, Sims, delta, trueLambda, col=c("dodgerblue", "purple", "orange2", "red"), main=""){
  par(mar=c(4.2,4,1.2,0.2))
  
  #get smoothed ABC densities, xlim and ylim
  abcDensity <- list(length(delta));
  xlim <- rep(trueLambda, 2);
  ymax <- 0;
  for(i in 1:length(delta)){
    abcSamples <- getABCPosteriorSamples(Sims, sobs, delta[i]); 
    abcDensity[[i]] <- density(abcSamples$P);  
    
    #get range (xlim and ylim)
    xlim <- range(c(abcDensity[[i]]$x, xlim));
    ymax <- max(ymax, abcDensity[[i]]$y);
  }
  
  #prepare grid points
  lambda <- seq(xlim[1], xlim[2], length.out=1000);
  analyticalDensity <- posterior(lambda, d, alpha, beta);

  #plot analytical posteriors
  ylim <- c(0, max(analyticalDensity, ymax));
  plot(0, type='n', xlim=xlim, ylim=ylim, xlab=expression(lambda), ylab="Posterior density", main=main);
  polygon(c(lambda, lambda[1]), c(analyticalDensity, analyticalDensity[1]), border=NA, col='gray86');
  lines(lambda, dgamma(lambda, alpha, beta), lty=2);
  abline(v=trueLambda, lty=2)
  
  #add ABC posteriors
  for(i in 1:length(delta)){
    lines(abcDensity[[i]], col=col[i]);
  }
  
  #add legend
  legend <- c("Prior", "Analytical",  paste("ABC", delta));
  legend('topright', lwd=1, col=c('black', 'black', col), lty=c(2, 1, rep(1, length(delta))), legend=legend, bty='n');
}

##################################
# Run!
##################################

#true parameter values
trueLambda <- 2;
m <- 10;

#prior
alpha <- 5;
beta <- 1;

#ABC settings
N <- 100000;
toleranceDelta <- c(0.5, 0.1, 0.05, 0.01);

#generate simulations
Sims <- makeSims(N, m, alpha, beta);

#generate observed data
d <- simulate(m, trueLambda);
sobs <- calcStats(d);

# Effect of num simulations (smoothing)
par(mfrow=c(1,2))
plotPosteriors(d, sobs, alpha, beta, Sims, toleranceDelta, trueLambda, main=paste("N =", N));
Sims_short <- list(P=Sims$P[1:1000,, drop=FALSE], S=Sims$S[1:1000,])
plotPosteriors(d, sobs, alpha, beta, Sims_short, toleranceDelta, trueLambda, main="N = 1000");

# Effect of summary statistics choice
par(mfrow=c(1,3))
plotPosteriors(d, sobs, alpha, beta, Sims_short, toleranceDelta, trueLambda, main="All statistics");
plotPosteriors(d, sobs[,1,drop=FALSE], alpha, beta, Sims, toleranceDelta, trueLambda, main="Only mean");
plotPosteriors(d, sobs[,3,drop=FALSE], alpha, beta, Sims, toleranceDelta, trueLambda, main="Only min");

###########################################
# Coverage
###########################################
# coverageProperty <- function(pseudo_obs, simulations, tolerance=0){
#   #prepare storage
#   coverage <- numeric(dim(pseudo_obs$S)[1]);
#   
#   #conduct ABC estimations
#   for(i in 1:dim(pseudo_obs$S)[1]){
#     retained <- getABCPosteriorSamples(Sims, calcStats(pseuso_obs$S[i,]), tolerance);
#     coverage[i] <- sum(retained$P < pseudo_obs$f[i]) / length(retained$P);
#   }
#   
#   #return
#   return(coverage);
# }
# 
# pseudo_obs <- makeSims(100, m, alpha, beta);
# coverage <- coverageProperty(pseudo_obs, Sims, tolerance=0.1)
