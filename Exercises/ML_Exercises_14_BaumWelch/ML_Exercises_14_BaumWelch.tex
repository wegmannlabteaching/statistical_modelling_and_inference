\documentclass{../exercises}

\exerciseTitle{14}{HMM -- Baum Welch Algorithm}

\begin{document}

\maketitle

\begin{enumerate}[1.]

\item The goal of this exercise is to obtain maximum likelihood estimates (MLE) of the parameters of the hidden Markov model (HMM) outlined in Exercises 13. In that model, a researcher is studying the activity behavior of rodents in the wild. For this purpose, she attached small transmitters that measure and report the heart rate to several individuals. For each individual, the collected data thus consist of a series of heart rate measurements $\d=\{d_1, \ldots, d_T\}$ for time points $t=1, \ldots, T$.

To infer the pattern with which an individual rests or is active, an HMM is used with two states $r$ (resting) and $a$ (active) and transition probability matrix \begin{equation*}
 \btau = \begin{pmatrix}
       1-\tau_a & \tau_a\\
       \tau_r & 1-\tau_r
      \end{pmatrix},
\end{equation*}

where $\tau=(\tau_a, \tau_r)$ are, respectively, the probabilities to switch from the resting ($r$) to the active ($a$) state, and to switch in the other direction. We will further assume $d_t \sim {\cal N}(\mu_{s_t}, \sigma^2)$, that is, that the measured heart rate is normally distributed with variance $\sigma^2$ and means $\mu=(\mu_r, \mu_a)$, depending on the hidden state $s_t=r,a$ at time $t$. 

We will resort to the Baum-Welch algorithm, a variant of the EM algorithm, to obtain the MLE of $\tau=(\tau_a, \tau_r)$, $\mu=(\mu_r, \mu_a)$, $\sigma^2$ as well as of the initial distribution $\P(s_1|\phi)$ with by $\P(s_1=a) = \phi$ and $\P(s_1=r) = 1-\phi$. To unburden the notation, we will use $\btheta=(\tau, \mu, \sigma^2, \phi)$ to denote the vector of parameters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{enumerate}[a)]

\item Write out the complete data likelihood $\L_c(\btheta) =  \P(\d, \s |\btheta)$ and the corresponding complete data log $\l_c(\btheta) = \log \P(\d, \s |\btheta)$ likelihood, where $\s = (s_1, \ldots, s_T)$ denotes the hidden state at time $t=1, \ldots, T$.

\sol{\begin{eqnarray*}
      \L_c(\btheta) =  \P(\d, \s |\btheta) &=& \P(s_1|\phi)\prod_{t=2}^T \P(s_t|s_{t-1}, \tau) \prod_{t=1}^T \P(d_t|s_t, \mu_{s_t}, \sigma^2)\\
      \l_c(\btheta) =  \log \P(\d, \s |\btheta) &=& \log \P(s_1|\phi) + \sum_{t=2}^T \log \P(s_t|s_{t-1}, \tau) + \sum_{t=1}^T \log \P(d_t|s_t, \mu_{s_t}, \sigma^2)
     \end{eqnarray*}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Write out the $Q$-function $Q(\btheta|\btheta')$.

\sol{\begin{eqnarray*}
      Q(\btheta|\btheta') &=& \E[\l_c(\btheta) | \d, \btheta']\\
      &=& \E[\log \P(s_1|\phi)|\d, \btheta'] + \sum_{t=2}^T \E[ \log \P(s_t|s_{t-1}, \tau) | \d, \btheta'] + \sum_{t=1}^T \E[ \log \P(d_t|s_t, \mu_{s_t}, \sigma^2) | \d, \btheta']\\
      &=& \sum_{s_1} \gamma_1(s_1) \log \P(s_1|\phi) + \sum_{t=2}^T \sum_{s_t} \sum_{s_{t-1}} \xi_t(s_t, s_{t-1}) \log \P(s_t|s_{t-1}, \tau) + \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t)  \log \P(d_t|\mu_{s_t}, \sigma^2),
     \end{eqnarray*}
     where $\gamma_t(s_t)$ and $\xi_t(s_t, s_{t-1})$ denote the expectation weights $\P(s_t|\d, \btheta')$ and $\P(s_t,s_{t-1}|\d, \btheta')$.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Derive the updates in the M-step of the Baum-Welch for parameters $\tau_a$ and $\tau_r$.

\sol{For $\tau_a$,
\begin{eqnarray*}
      \frac{\partial}{\partial \tau_a} Q(\btheta|\btheta') &=& \sum_{t=2}^T \sum_{s_t} \xi_t(s_t, s_{t-1}=r) \frac{\partial}
{\partial \tau_a} \log \P(s_t|s_{t-1}=r, \tau_a) \\
      &=& \sum_{t=2}^T \left[\frac{\xi_t(s_t = a, s_{t-1}=r)}{\tau_a} - \frac{\xi_t(s_t = r, s_{t-1}=r)}{1-\tau_a} \right] = 0\\
      \hat{\tau_a} &=& \frac{\displaystyle\sum_{t=2}^T \xi_t(s_t = a, s_{t-1}=r)}{\displaystyle \sum_{t=2}^T \sum_{s_t} \xi_t(s_t, s_{t-1}=r)},
     \end{eqnarray*}
and analogously for $\tau_r$
\begin{eqnarray*}
      \hat{\tau_r} &=& \frac{\displaystyle\sum_{t=2}^T \xi_t(s_t = r, s_{t-1}=a)}{\displaystyle \sum_{t=2}^T \sum_{s_t} \xi_t(s_t, s_{t-1}=a)}.
     \end{eqnarray*}}
     
\item Derive the updates in the M-step of the Baum-Welch for parameters $\mu_a$ and $\mu_r$.

\sol{For $\mu_a$,
     \begin{eqnarray*}
      \frac{\partial}{\partial \mu_a} Q(\btheta|\btheta') &=& \sum_{t=1}^T \gamma_t(s_t=a)  \frac{\partial}{\partial \mu_a} \log \P(d_t|\mu_a, \sigma^2) = \frac{1}{\sigma^2} \sum_{t=1}^T \gamma_t(s_t=a) (d_t - \mu_a) = 0\\
      \hat{\mu_a} &=& \frac{\sum_{t=1}^T \gamma_t(s_t=a) d_t}{\sum_{t=1}^T \gamma_t(s_t=a)},
     \end{eqnarray*}
     and analogously for $\mu_r$
     \begin{eqnarray*}
      \hat{\mu_r} &=& \frac{\sum_{t=1}^T \gamma_t(s_t=r) d_t}{\sum_{t=1}^T \gamma_t(s_t=r)},
     \end{eqnarray*}}

\item Derive the updates in the M-step of the Baum-Welch for parameter $\sigma^2$.

\sol{For $\sigma^2$,
     \begin{eqnarray*}
      \frac{\partial}{\partial \sigma^2} Q(\btheta|\btheta') &=& \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t)  \frac{\partial}{\partial \sigma^2} \log \P(d_t|\mu_{s_t}, \sigma^2)\\
      &=& \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) \left(-\frac{1}{2\sigma^2} + \frac{(\mu_{s_t}-d_t)^2}{2\left(\sigma^2\right)^2} \right) = \frac{1}{2\sigma^2} \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) \left(\frac{1}{\sigma^2} (\mu_{s_t}-d_t)^2 - 1\right) = 0      
      \end{eqnarray*}
      from which we get
      \begin{eqnarray*}
      \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) \left(\frac{1}{\sigma^2} (\mu_{s_t}-d_t)^2 - 1\right) &=& 0\\
      \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) \frac{1}{\sigma^2} (\mu_{s_t}-d_t)^2 - \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) &=& 0\\
     \frac{1}{\sigma^2} \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) (\mu_{s_t}-d_t)^2 - T &=& 0\\
       \sigma^2 T &=& \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) (\mu_{s_t}-d_t)^2\\
       \hat{\sigma}^2 &=& \frac{1}{T} \sum_{t=1}^T \sum_{s_t} \gamma_t(s_t) (\mu_{s_t}-d_t)^2\\
        &=& \frac{1}{T} \sum_{t=1}^T \left( \gamma_t(s_t=a) (\mu_a-d_t)^2 + \gamma_t(s_t=r) (\mu_r - d_t)^2 \right)\\
    \end{eqnarray*}}
    
\item Derive the updates in the M-step of the Baum-Welch for parameter $\phi$.
          
\sol{And for $\phi$,
\begin{eqnarray*}
      \frac{\partial}{\partial \phi} Q(\btheta|\btheta') &=&  \sum_{s_1} \gamma_1(s_1)  \frac{\partial}{\partial \phi} \log \P(s_1|\phi) = \frac{\gamma_1(s_1=a)}{\phi} - \frac{\gamma_1(s_1=r)}{1-\phi} = 0\\
      \hat{\phi} &=& \frac{\gamma_1(s_1=a)}{\gamma_1(s_1=a)+\gamma_1(s_1=r)} = \gamma_1(s_1=a)
    \end{eqnarray*}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Implement the Baum-Welch algorithm in R. Remember to use the forward-backward algorithm to calculate the EM weights $\gamma_t(s_t)$ and $\xi_t(s_t, s_{t-1})$.

\sol{See link \solutions{ML_Exercises_14_BaumWelch/ML_Exercises_14_BaumWelch.R} for a possible implementation.}


\item Write a function to simulate data under the parameters. This function will be similar to that under Exercises 13, but use $\phi$ instead of the stationary distribution to sample the first state.

\sol{See link \solutions{ML_Exercises_14_BaumWelch/ML_Exercises_14_BaumWelch.R} for a possible implementation.}

\item Simulate some data for $T=1000$ steps with $\tau_a=0.05$, $\tau_r=0.02$, $\sigma^2=1000$ and $\mu_r=120$, $\mu_a=180$ and $\phi=0.2$. Can you reliably infer these parameters? Are there multiple solutions?

\sol{You should be able to estimate these parameters rather well. The exception is $\phi$, for which there is only one data point and it will always be estimated close to zero or one to reflect the simulated state $s_1$.}

\item Implement an empirical Bayes solution for the state posteriors $\P(s_t|\d, \btau, \mu, \sigma2)$.

\sol{Remember that for an empirical Bayes solution, we run the Bayesian inference on the latent variables ($s_t$) given the MLE estimates of the hierarchical parameters, in this case $\tau$, $\mu$, $\sigma^2$ and $\phi$. You can therefore implement an empirical Bayes solutions rather easily: run your Baum-Welch estimation, followed by another forward-backward pass using the MLE estimates. See link \solutions{ML_Exercises_14_BaumWelch/ML_Exercises_14_BaumWelch.R} for a possible implementation.}


\end{enumerate}

\end{enumerate}

\end{document}
