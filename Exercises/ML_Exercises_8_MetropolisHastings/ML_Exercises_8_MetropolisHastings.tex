\documentclass{../exercises}

\exerciseTitle{8}{Metropolis Hastings Algorithm (MCMC)}


\def\mmu{\boldsymbol{\mu}}
\def\f{\boldsymbol{f}}
\def\g{\boldsymbol{g}}


\begin{document}

\maketitle


\begin{enumerate}[1.]

%-----------------------------
% Compare to analytical posterior: mutation rate from exercise 6
%-----------------------------

\item The goal of this exercise is to develop an MCMC scheme for a simple model, under which the MCMC posterior can be compared to the analytical posterior distribution. For this, we will revisit the model of Exercises 6.

Consider aligned DNA sequences of length $L$ of two species that diverged $T$ generations ago (T is known). Further, these sequences shall differ at $n$ and be identical at $L-n$ bases. The goal is to infer the mutation rate per generation $\mu$ under an infitie sites model. Under this model, the number of differences $n$ is Poisson distributed as $n \sim \mathrm{Pois}(2T\mu L)$ and hence
\begin{equation*}
\P(n|\mu) = \frac{1}{n!}(2T\mu L)^n e^{-2T\mu L}.
\end{equation*}

We will assume that the prior distribution on $\mu$ is Gamma with known parameters $\alpha, \beta$ such that
\begin{equation*}
 \P(\mu) \sim \mathrm{Gamma}(\alpha, \beta) = \frac{\beta^\alpha}{\Gamma(\alpha)}\mu^{\alpha-1}e^{-\mu \beta}.
\end{equation*}
Here, $\Gamma(\cdot)$ denotes the Gamma function.

The gamma distribution is conjugate to the Poisson distribution and in Exercises 6 we derived the analytical form of the posterior distribution, which is a Gamma distribution with parameters

\begin{equation*}
 \mu|n \sim \mathrm{Gamma}(n+\alpha, 2TL+\beta).
\end{equation*}

\begin{enumerate}[a)]
\item Determine the simplified Hastings-ratio for an update $\mu \rightarrow \mu'$ according to a symmetric proposal kernel $q(\mu'|\mu)$.

\sol{\begin{align*}
    h_\mu &= \min \left(1, \frac{\frac{1}{n!}(2T\mu' L)^n e^{-2T\mu' L}\frac{\beta^\alpha}{\Gamma(\alpha)}\mu'^{\alpha-1}e^{-\mu' \beta}}{\frac{1}{n!}(2T\mu L)^n e^{-2T\mu L}\frac{\beta^\alpha}{\Gamma(\alpha)}\mu^{\alpha-1}e^{-\mu \beta}} \right)\\
    &= \min \left(1, \left(\frac{\mu'}{\mu}\right)^{n+\alpha-1} e^{(2TL+\beta)(\mu-\mu')} \right)
     \end{align*}
}


\item Implement a function \texttt{calcLogHastings} that calculates this Hastings-ratio in the log (to prevent under- or overflow). What are the required arguments for this function?

\sol{\script{calcLogHastings <- function(mu, mu\_prime, n, t, L, alpha, beta)\{\\
  \hspace*{0.5cm}return(\\
  \hspace*{1cm}  min(0,\\
  \hspace*{1.7cm}    (n+alpha-1) * log(mu\_prime / mu) + (2*t*L + beta)*(mu - mu\_prime)\\
  \hspace*{1cm} )\\
  \hspace*{0.5cm} )\\
\}}}

\item The mutation rate $\mu$ must be positive (i.e. $\mu \leq 0$). Propose a symmetric proposal function $q(\mu'|\mu)$ that respects this range and implement it in a function \texttt{propose}. Your function should take $\mu$ and a proposal tuning parameter $\sigma$ as arguments and return $\mu'$.

\sol{\script{propose <- function(mu, sigma)\{\\
 \hspace*{0.5cm} return(\\
 \hspace*{1cm}   abs(\\
  \hspace*{1.5cm}    rnorm(1, mean = mu, sd = sigma)\\
  \hspace*{1cm}  )\\
 \hspace*{0.5cm} )\\
\}}}


\item With the above functions at hand, implement an MCMC scheme in a function \texttt{MCMC}. Your function shoudl take $n$, $L$, $\alpha$, $\beta$, the tuning parameter $\sigma$ as well as the desired length of the MCMC chain as input and return the resulting MCMC chain as a vector.

\sol{\solutions{ML_Exercises_8_MetropolisHastings/ML_Exercises_8_MetropolisHastings_Mutation.R}}

\item Simulate some data for a true $\mu=10^-8$, $L=10^10$ and $T=10^4$ using the simulation function developed in Exercises 6. Then use your MCMC to infer the parameter $\mu$ from the data. Make sure to tune $\sigma$ to obtain an acceptable acceptance rate.

\sol{\solutions{ML_Exercises_8_MetropolisHastings/ML_Exercises_8_MetropolisHastings_Mutation.R}}

\end{enumerate}

%-----------------------------
% Biased MLE variance
%-----------------------------

\item In this exercise, we will illustrate that MLE estimates are potentially biased and that these biases may be overcome in a Bayesian setting. Consider $M$ measurements $\x_i=(x_{i1}, \ldots, x_{im})$ for each of $n$ experiments $i=1, \ldots, n$. These measurements shall be normally distributed with means $\mu_i$ specific to each experiment, but a common variance $\sigma^2$, such that

\begin{equation*}
 x_{ij} \sim \N(\mu_i, \sigma^2), j=1,\ldots,m.
\end{equation*}

The goal is now to infer the variance $\sigma^2$ from the full data.

\begin{enumerate}[a)]
\item Write out the full likelihood $\L(\mmu, \sigma^2)=\P(\x|\mmu, \sigma^2)$ where $\x=(\x_1, \ldots, \x_n)$ and $\mmu=(\mu_1, \ldots, \mu_n)$.

\sol{\begin{eqnarray*}
    \L(\mmu, \sigma^2)=\P(\x|\mmu, \sigma^2) &=& \prod_{i=1}^n \prod_{j=1}^m \P(x_{ij}|\mu_i, \sigma^2) \\
    &=& \left(\frac{1}{\sqrt{2 \pi \sigma^2 }}\right)^{nm} \prod_{i=1}^n \prod_{j=1}^m  \exp{\left(- \frac{(x_ij-\mu_i)^2}{2 \sigma^2} \right)} \\
    &=& \left(2 \pi \sigma^2\right)^{-\frac{nm}{2}} \exp{\left( \sum_{i=1}^n \sum_{j=1}^m - \frac{(x_ij-\mu_i)^2}{2 \sigma^2}\right)}\\
    &=&\left(2 \pi \sigma^2\right)^{-\frac{nm}{2}} \exp{\left(-\frac{1}{2\sigma^2} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2\right)}.
\end{eqnarray*}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Derive the maximum likelihood estimators $\hat{\sigma}^2$ and $\hat{\mu}_i$.

\sol{Let's start with the log-likelihood:
\begin{equation*}
    \l(\mmu, \sigma^2) = -\frac{nm}{2}(\log \left(2 \pi \right) + \log \sigma^2) - \frac{1}{2\sigma^2}\sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}
The derivative with respect to $\mu_i$ is:
\begin{equation*}
    \frac{\partial}{\partial \mu_i} \l(\mmu, \sigma^2) = \frac{1}{2\sigma^2} \sum_{j=1}^m (x_{ij} - \mu_i).
\end{equation*}
Where $\sum_{i=1}^n$ falls away due to the $\mu_i$ being independant from each other.\\
Setting it to zero and solving for $\mu_i$ results in:
\begin{equation*}
    \hat{\mu_i} = \frac{1}{m} \sum_{j=1}^m x_{ij}.
\end{equation*}
The derivative with respect to $\sigma^2$ is:
\begin{equation*}
    \frac{\partial}{\partial \sigma^2} \l(\mmu, \sigma^2) = -\frac{nm}{2\sigma^2} + \frac{1}{2(\sigma^2)^2} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}
Setting it to zero and solving for $\sigma^2$ results in:
\begin{equation*}
    \hat{\sigma}^2 = \frac{1}{nm} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item\label{item:normal:implement} Write a function to simulate data under this model. Simulate data for $n=100$ experiments and $m=2$ values each. Simulate the $\mu_i \sim \N(0, 1)$ and set $\sigma^2=1$.

\sol{\script{simData <- function(n, m, sigma2, sigma2Mu)\{\\
  \hspace*{1cm} \#simulate mu\\
  \hspace*{1cm} mu <- rnorm(n, mean=0, sd=sqrt(sigma2Mu));\\
  \hspace*{1cm} \#simulate x\_ij $\sim$ N(mu\_i, sigma2Mu)\\
  \hspace*{1cm} x <- matrix(rnorm(n*m, mean=mu, sd=sqrt(sigma2)), nrow = n, ncol = m);\\
  \hspace*{1cm}return(data.frame(x=x, mu=mu));\\
\};}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Implement a function to calculate the maximum likelihood estimates and use it tp estimate the values from the data simulated under \ref{item:normal:implement}. Repeat the process of simulating and estimating to convince yourself that the estimator $\hat{\sigma}^2$ is biased (underestimation).

\sol{\script{MLE\_mu <- function(x)\{\\
  \hspace*{1cm}m <- ncol(x);\\
  \hspace*{1cm}return(1/m * rowSums(x));\\
\}\\
MLE\_sigma2 <- function(x, mu)\{\\
  \hspace*{1cm}m <- ncol(x);\\
  \hspace*{1cm}n <- nrow(x);\\
  \hspace*{1cm}return((1/(n*m)) * sum(rowSums((x - mu)\textasciicircum 2)));\\
\};}
\solutions{ML_Exercises_8_MetropolisHastings/ML_Exercises_8_MetropolisHastings_Gaussian.R}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Let us next develop a Bayesian inference scheme assuming uniform priors $\P(\mu_i) \propto 1$ and $\P(\sigma^2) \propto 1$. To generate estimates, we will resort to an MCMC using the Metropolis-Hastings algorithm. For this, propose symmetric proposal kernels and derive the necessary Hastings ratios to update each $\mu_l$ and $\sigma^2$ individually.

\sol{Potential symmetric proposal kernels include normal kernels:
\begin{equation*}
    \mu'_i \sim {\cal N}(\mu_i, \sigma_{\mu_i}^2), \quad \sigma'^2 \sim {\cal N}(\sigma^2, \sigma_{\sigma^2}^2),
\end{equation*}
where $\sigma'^2$ must be mirrored at 0 to respect the interval $(0,\infty)$.

The Hasting ratio $h_{\mu_i}$ for an update $\mu_i \rightarrow \mu_i'$ is then
\begin{eqnarray*}
   h_{\mu_i} &=& \min \left(1, \frac{\prod_{j=1}^m\P(x_{ij}|\mu_i', \sigma^2)}{\prod_{j=1}^m\P(x_{ij}|\mu_i, \sigma^2)} \right) = \min \left(1, \prod_{j=1}^m \frac{\P(x_{ij}|\mu_i', \sigma^2)}{\P(x_{ij}|\mu_i, \sigma^2)} \right) = \\
   &=& \min  \left(1, \exp \left[-\frac{1}{2\sigma^2}\sum_{j=1}^m(x_{ij}-\mu_i')^2 + \frac{1}{2\sigma^2}\sum_{j=1}^m(x_{ij}-\mu_i)^2 \right] \right)\\ 
   &=& \min  \left(1, \exp \left[\frac{1}{2\sigma^2}\left(\sum_{j=1}^m(x_{ij}-\mu_i)^2 - \sum_{j=1}^m(x_{ij}-\mu_i')^2 \right) \right] \right)\\
   &=& \min  \left(1, \exp \left[\frac{1}{2\sigma^2}\left(\sum_{j=1}^m x_{ij}^2 -2\sum_{j=1}^mx_{ij}\mu_i + \sum_{j=1}^m \mu_i^2 - \sum_{j=1}^m x_{ij}^2 +2\sum_{j=1}^m x_{ij}\mu_i' - \sum_{j=1}^m \mu_i^{'2}) \right) \right] \right)\\
    &=& \min \left(1, \exp \left[ \frac{1}{\sigma^2}\left( \frac{m}{2}(\mu_i^2 - \mu_i^{'2}) + (\mu_i' - \mu_i)\sum_{j=1}^m x_{ij} \right) \right] \right)
\end{eqnarray*}
The Hasting ratio $h_{\sigma^2}$ for an update $\sigma^2 \rightarrow \sigma^{2'}$ is then
\begin{eqnarray*}
   h_{\sigma^2} &=& \min \left(1, \frac{\prod_{i=1}^n \prod_{j=1}^m  \P(x_{ij}|\mu_i, {\sigma^2}')}{\prod_{i=1}^n \prod_{j=1}^m  \P(x_{ij}|\mu_i, \sigma^2)} \right) = \min \left(1, \prod_{i=1}^n \prod_{j=1}^m \frac{\P(x_{ij}|\mu_i, {\sigma^2}')}{\P(x_{ij}|\mu_i, \sigma^2)} \right)\\
   &=& \min \left(1, \frac{(2\pi\sigma^{2'})^{-\frac{nm}{2}}}{(2\pi\sigma^{2})^{-\frac{nm}{2}}} \exp \left[ \frac{1}{2\sigma^2} \sum_{i=1}^n\sum_{j=1}^m (x_{ij} - \mu_i)^2 -\frac{1}{2\sigma^{2'}} \sum_{i=1}^n\sum_{j=1}^m (x_{ij} - \mu_i)^2 \right]   \right)\\ 
   &=& \min \left(1, \left(\frac{\sigma^2}{\sigma^{2'}}\right)^{\frac{nm}{2}} \exp \left[ \frac{1}{2}\left(\frac{1}{\sigma^2}-\frac{1}{\sigma^{2'}}\right)\left( \sum_{i=1}^n\sum_{j=1}^m (x_{ij} - \mu_i)^2 \right) \right]   \right)\\ 
   &=& \min \left(1, \left(\frac{\sigma^2}{\sigma^{2'}}\right)^{\frac{nm}{2}} \exp \left[ \frac{1}{2} \left(\frac{1}{\sigma^2} - \frac{1}{\sigma^{2'}}\right) \left(\sum_{i=1}^n\sum_{j=1}^m x_{ij}^2 - \sum_{i=1}^n\sum_{j=1}^m x_{ij}\mu_i + m \sum_{i=1}^n \mu_i^2 \right)\right] \right).   
\end{eqnarray*}
Note that the simplifactions require a single $\exp$ evaluation per interation and allow for the sums across $x_{ij}$ to be pre-calculated.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Implement the MCMC scheme and use it to infer the posterior $\P(\sigma^2|\x)$ on the data simulate under \ref{item:normal:implement}. You may use the function \texttt{dnorm} rather than the simplified hastings ratios. Make sure the chain is converging by monitoring the trace. Is the posterior also biased?

\sol{See link \solutions{ML_Exercises_8_MetropolisHastings/ML_Exercises_8_MetropolisHastings_Gaussian.R} for an example on how to implement such an MCMC. \\
It is clearly visible that the posterior is not biased and estimates $\sigma_2$ very well, even if $M$ is small. This is because the Bayesian inference integrates over the uncertainty in $\mmu$. The MLE of $\mu_i$ are generally estimated too close to the data, especially if $M$ is small. \\ Therefore, the variance $\sigma^2$ is estimated too small. This issue cannot be overcome by increasing the number of experiments $n$. Only increasing the number of measurements $M$ results in more accurate estimates of $\mu_i$ and hence also $\sigma^2$.}
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{enumerate}

\end{document}
