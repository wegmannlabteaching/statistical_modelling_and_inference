\documentclass{../exercises}

\exerciseTitle{2}{Maximum Likelihood Estimation}

\begin{document}

\maketitle

\begin{enumerate}[1.]
%-------------------------------------------
% Exercise Poisson
%-------------------------------------------

\item Let $\d=(d_1, \ldots, d_n)$ denote the discrete distances along a chromosome (in base pairs) between $n+1$ sites that diverged between two species. Assuming that mutations occur randomly in the genome, these distances are geometrically distributed $d_i \sim \mathrm{Geo}(\mu)$ with per-site mutation probability $\mu$ and probability
\begin{equation*}
\P(d_i|\mu) = (1-\mu)^{d_i-1}\mu.
\end{equation*}
 In words: a distance $d_i$ is observed if there was no mutation for $d_i-1$ base pairs, which happens with probability $(1-\mu)^{d_i - 1}$, followed by a mutation, which happens with probability $\mu$.

 The goal of this exercise is to derive the maximum likelihood estimator (MLE) of $\mu$.

\begin{enumerate}[a)]
 \item Draw a DAG of this model.

 \sol{\begin{figure}[!h]\centering\begin{tikzpicture}
            \node[latent] (1) {$\mu$};
            \node[obs, below=of 1]    (D) {$d_i$};
            \plate[inner sep=0.25cm,yshift=0.1cm] {plate1} {(D)} {$i=1,\ldots,n$};
            \edge {1} {D};
            \end{tikzpicture}\end{figure}}

 \item Write out the full likelihood $\L(\mu)$ of the model.
 \sol{\begin{equation*}\L(\mu) = \prod_{i=1}^n \P(d_i|\mu) = \prod_{i=1}^n (1-\mu)^{d_i-1}\mu =  \mu^n (1-\mu)^{\sum_{i=1}^n d_i - n}\end{equation*}}

 \item To derive the MLE, we need to take the gradient $\nabla \L(\mu)$ of the likelihood function and set it to zero. This is much easier in the log. Write out the log-likelihood $\l(\mu) = \log \L(\mu)$ for this model.
 \sol{\begin{equation*}
       \l(\mu) = \log \L(\mu) = n \log \mu + \left(\sum_{i=1}^n d_i - n\right) \log (1-\mu)
      \end{equation*}}

 \item Calculate the gradient $\nabla \l(\mu) = \frac{\dd}{\dd \mu} \l(\mu)$.

 \sol{\begin{equation*}
       \frac{\dd}{\dd \mu} \l(\mu) = \frac{n}{\mu} - \frac{\sum_{i=1}^n d_i - n}{1-\mu}
      \end{equation*}}

  \item To get the MLE, set the gradient to zero and solve for $\mu$.

  \sol{\begin{align*}
        \frac{n}{\mu} - \frac{\sum_{i=1}^n d_i - n}{1-\mu} &= n(1-\mu) - \left(\sum_{i=1}^n d_i - n\right)\mu = n - \mu \sum_{i=1}^n d_i = 0\\
        \hat{\mu} &= \frac{n}{\sum_{i=1}^n d_i}
       \end{align*}}

  \item Write functions in R i) to simulate data under this model (Hint: use the function \texttt{rgeom}, but you need to add +1 due a slightly different definition of the geometric distribution in R), ii) to calculate the log-likelihood $\l(\mu)$, and iii) to calculate the MLE. Use these functions to simulate some data and then plot the likelihood surface for that data. Add both the MLE and the true parameter value to your plot. How well can you estimate $\mu$? How does the likelihood surface and estimation accuracy change with more data (i.e. higher $n$?)

  \sol{\solutions{ML_Exercises_2_MLE/ML_Exercises_2_MLE_Ex1.R}}


\end{enumerate}


%-------------------------------------------
% Exercise Normal mean
%-------------------------------------------

\item Assume that the amount of pollen produced by a single flower of a specific species is normally distributed. While the variance in produced pollen between flowers $\sigma^2$ is known and constant between populations, the mean number of pollen per flower $\mu_p$ varies among populations due to climatic differences.\\\smallskip In order to infer the mean number of pollen per plant $\mu_p$ for a specific population $p$, a dedicated PhD student spent years counting the pollen produced by $n_p$ plants from this population and obtained the measurements $\d=\{d_1, \ldots, d_{n_p}\}$.

\begin{enumerate}[a)]
\item Write down the likelihood function ${\cal L}(\mu_p) = \P(\d|\mu_p)$ of this model.

\sol{\begin{eqnarray*}\P(\d|\mu_p) &=& \prod_{i=1}^{n_p} \frac{1}{\sigma\sqrt{2\pi}} \exp\left(-\frac{(d_i-\mu_p)^2}{2\sigma^2}\right)
       = \left(\frac{1}{\sigma\sqrt{2\pi}}\right)^{n_p} \prod_{i=1}^{n_p}  \exp\left(-\frac{(d_i-\mu_p)^2}{2\sigma^2}\right)
     \end{eqnarray*}
The latter formulation puts all constant terms in front of the product to ease taking derivatives.}

\

\item Derive the maximum likelihood estimate (MLE) of $\hat{\mu_p} = \argmax_{\mu_p} {\cal L}(\mu_p)$.

\sol{To find the MLE of $\mu_p$, we have to take the derivative of the likelihood and set it to zero. This is much easier to do for the log likelihood. \begin{eqnarray*}
\ell(\mu_p) = \log {\cal L}(\mu_p) &=& \log \left(\frac{1}{\sigma\sqrt{2\pi}}\right)^{n_p} + \log \prod_{i=1}^{n_p}  \exp\left(-\frac{(d_i-\mu_p)^2}{2\sigma^2}\right)\\
&=& {n_p} \log \left(\frac{1}{\sigma\sqrt{2\pi}}\right) + \sum_{i=1}^{n_p} -\frac{(d_i-\mu_p)^2}{2\sigma^2}\\
 &=& - {n_p} \log (\sigma \sqrt{2\pi}) - \frac{1}{2\sigma^2}\sum_{i=1}^{n_p} (d_i-\mu_p)^2
\end{eqnarray*}

Note that the first term does not contain $\mu_p$. We thus get

\begin{equation*}
 \frac{d}{d\mu_p} \ell(\mu_p) = \frac{d}{d\mu_p}\left( - \frac{1}{2\sigma^2}\sum_{i=1}^{n_p} (d_i-\mu_p)^2\right)
\end{equation*}

Using chain rule we get
\begin{eqnarray*}
\frac{d}{d\mu_p} \ell(\mu_p) &=& \frac{2}{2\sigma^2} \sum_{i=1}^{n_p} (d_i - \mu_p) = \frac{1}{\sigma^2} \sum_{i=1}^{n_p} (d_i - \mu_p) = 0\\
 0 &=& \sum_{i=1}^{n_p} (d_i - \mu_p) = \sum_{i=1}^{n_p} d_i - {n_p}\mu_p\\
  \hat{\mu_p} &=& \frac{1}{{n_p}}\sum_{i=1}^{n_p} d_i
  \end{eqnarray*}}

\end{enumerate}

%-------------------------------------------
% Exercise Normal mean and sigma
%-------------------------------------------

\item Consider similar data sets $\d_p$ for several populations $p=1, \ldots, P$ of $n_p$ samples each. The goal is now to infer both $\mu_p$ for each population as well as $\sigma^2$.


\begin{enumerate}[a)]

\item Propose a model for the likelihood $\P(\d_1, \ldots, \d_P|\sigma^2, \mu_1, \ldots, \mu_P)$. Which independence assumptions are you willing to make? Describe them both through a DAG as well as a factorization of the likelihood.

\sol{Given the text above, it makes sense to assume that the data $\d_p \perp \d_j | \sigma^2, p \neq j$, and that $\mu_p \perp \mu_j, p \neq j$.
\begin{figure}[!h]\centering\begin{tikzpicture}
\node[latent] (1) {$\mu_p$};
\node[latent, right=of 1] (2) {$\sigma^2$};
\node[obs, below=of 1]    (D) {$d_i$};
\edge {1} {D};
\edge {2} {D};
\plate[inner sep=0.2cm,yshift=0.1cm, xshift=0.1cm] {plate1} {(D)} {$i=1,\ldots,n_p$};
\tikzset{plate caption/.append style={below=0.75cm of D}};
\plate[inner sep=0.35cm, xshift=-0.3cm] {plate2} {(1) (D)} {$p=1,\ldots,P$};
\end{tikzpicture}\end{figure}
Hence we can write
\begin{equation*}
 P(\d_1, \ldots, \d_P|\sigma^2, \mu_1, \ldots, \mu_P) = \prod_{p} \P(\d_p|\sigma^2, \mu_p)
\end{equation*}
}

\item Derive the maximum likelihood estimates (MLE) of $\hat{\mu_p} = \argmax_{\mu_p} \L(\mu_p), p=1, \ldots, P$ and $\hat{\sigma^2} = \argmax_{\sigma^2} \L(\sigma^2)$.

\sol{Let us first write the full likelihood ${\cal L}(\boldsymbol{\mu}, \sigma^2)$:
\begin{eqnarray*}
 {\cal L}(\sigma^2, \boldsymbol{\mu}) = \prod_{p} \P(\d_p|\sigma^2, \mu_p) = \prod_{p} \prod_{i=1}^{n_p} \frac{1}{\sqrt{2\pi\sigma^2}} \exp\left(-\frac{(d_{pi}-\mu_p)^2}{2\sigma^2}\right)
       = \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right)^{N} \prod_{p} \prod_{i=1}^{n_p}  \exp\left(-\frac{(d_{pi}-\mu_p)^2}{2\sigma^2}\right),
\end{eqnarray*}
where we used $N=\sum_p n_p$.
The log likelihood is
\begin{eqnarray*}
\ell(\sigma^2, \boldsymbol{\mu}) &=& - N \log \left(\sqrt{2\pi\sigma^2}\right) - \sum_{p} \sum_{i=1}^{n_p} \frac{(d_{pi}-\mu_p)^2}{2\sigma^2}\\
 &=& - \frac{N}{2} \left(\log (2\pi) + \log(\sigma^2) \right) - \frac{1}{2\sigma^2}\sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2.
 \end{eqnarray*}
 Let us take the derivative with respect to $\mu_p$, set it to zero and solve for $\mu_p$. Note that all $\mu_{j\neq p}$ are irrelevant when deriving for $\mu_p$:
 \begin{eqnarray*}
 \frac{d}{d\mu_p} \ell(\sigma^2, \boldsymbol{\mu}) &=& \frac{2}{2\sigma^2} \sum_{i=1}^{n_p} (d_{pi} - \mu_p) = 0\\
 0 &=& \sum_{i=1}^{n_p} (d_{pi} - \mu_p) = \sum_{i=1}^{n_p} d_{pi} - {n_p}\mu_p\\
  \hat{\mu_p} &=& \frac{1}{n_p}\sum_{i=1}^{n_p} d_{pi}
 \end{eqnarray*}
 Finally, we obtain the MLE of $\hat{\sigma^2}$ by taking the derivative with respect to $\sigma^2$, setting it to zero and solving for $\sigma^2$:
  \begin{eqnarray*}
 \frac{d}{d\sigma^2} \ell(\sigma^2, \boldsymbol{\mu}) &=& -\frac{N}{2\sigma^2} + \frac{1}{2{(\sigma^2)}^2} \sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2 = 0\\
 0 &=& -N + \frac{1}{\sigma^2} \sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2\\
  0 &=& -N\sigma^2 + \sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2\\
  N\sigma^2 &=&  \sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2\\
  \hat{\sigma^2} &=& \frac{1}{N} \sum_{p}\sum_{i=1}^{n_p} (d_{pi}-\mu_p)^2
 \end{eqnarray*}
}

\end{enumerate}

%---------------------

\end{enumerate}

\end{document}
