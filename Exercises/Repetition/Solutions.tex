\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[alwaysadjust]{paralist}
\usepackage[pdftex]{graphicx}
\DeclareMathOperator{\E}{\mathbb{E}}
\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

\usepackage[withSolutions]{optional}
%\usepackage[withoutSolutions]{optional}

%opening
\title{Exercises in Machine Learning}

\begin{document}

\maketitle

\section{Sampling from distributions}
Propose algorithms to sample from the following distributions, assuming that a random generator to generate random numbers uniformly distributed between 0 and 1 is available: $u \sim U[0,1]$. Implement these algorithms in R.

\begin{compactenum}[a)]
 \item From a distribution of the form $f(x)=ax^2$ in the interval $[0,10]$. Begin by solving for the correct $a$ such that $\int_0^{10} f(x) dx = 1$.\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item $\int_0^{10} f(x) dx = F(x) = \frac{a}{3}x^3 + C$
 \item Since $F(x)=0$, $C=0$.
 \item Since $F(10)=1$, $a=\frac{3}{1000}$
 \item The inverse $F^{-1}(y)=\left(\frac{3}{a}y\right)^{\frac{1}{3}}$ and hence an algorithm to sample $n$ times from this distribution is:\\
 \texttt{u <- runif(n); x <- (u*3/a)\textasciicircum(1/3);} 
 \end{compactitem}}
 \item From a triangular distribution between -1 and 1 with peak at 0.\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item There are, as usual, multiple solutions. But since there is no way around separating between the negative and positive case and since the distribution is mirrored at 0, an easy algorithm would be to simulate from the positive side only, and to then randomly select the sign in a second step.
 \item To simulate from the positive side, one needs to get the inverse of the integral. Note that $f(0)=2$ such that $\int_0^1 f(x)=1$, and hence $f(x)=-2x+2$
 \item $F(x)=\int f(x) = -x^2 + 2x + C$ with $C=0$ since $F(0)=0$.
 \item The inverse is thus $F^{-1}(y)=1- \sqrt(1-y)$ (obtained by solving the quadratic function and realizing that $1+\sqrt(1-y)$ is not a valid solution.
 \item An algorithm to sample $n$ times from this distribution is:\\
 \texttt{u <- runif(n); x <- 1 - sqrt(1-u);} 
 \end{compactitem}}
  \item From a mixture of two normal distributions with means -1 and 1 and standard deviations 2.\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item There are again multiple solutions, but a simple one might be to use a rejection algorithm. 
 \item Note that $f(x)=\frac{\phi(x, -1, 2) + \phi(x, 1, 2)}{2}$ where $\phi(x,\mu, \sigma)$ is the density of a normal distribution with mean $\mu$ and standard deviation $\sigma$. Since $f(x)$ is unbounded but computers are bounded, we need to decide on boundaries for the uniform proposal distribution. I suggest the interval $u\sim[-7,7]$, which includes all values up to 3 standard deviations away from the two means.
 \item We now need to choose an $M$ such that $MU(x)>f(x)$ for all $x$. Since $U(x)=\frac{1}{14}$ and $\phi(-1,-1,2)=\phi(1,1,2)<0.2$, a natural choice is $M=2.8$
 \item An algorithm to sample from this distribution is thus:\\
 \texttt{len <- 10000; M <- 2.8;\\u <- runif(len, min=-7, max=7);\\d <- (dnorm(u, mean=-1, sd=2) + dnorm(u, mean=1, sd=2))/2;\\x <- u[(d /(M*1/14)) > runif(len)];}
 \end{compactitem}}
\end{compactenum}
 
 
\section{Frequency of recessive allele}
Suppose that in a sample of $N$ individuals, $a<N$ suffer from a recessive genetic disorder. The goal is to find the maximum likelihood estimate of the frequency $q$ of the recessive allele. It is assumed that the alleles are in Hardy-Weinberg equilibrium in the population. The genotypes of the healthy individuals are unknown.

\begin{compactenum}[a)]
 \item Draw the directed acyclic graph (DAG) and write down the likelihood function of this model.\opt{withSolutions}{\\
  \textbf{Solution}\\
  \begin{figure}[!h]
 \includegraphics[width=6cm]{dag_alleles2.pdf}
 \end{figure}\\
 where $f_{aa}$ and $f_{hh}$ are the frequency of the homozygous affected and healthy genotypes in the population and $a$ the number of affected individuals. Note that $f_{ah} = 1-f_{aa} - f_{hh}$. \\
 Under this model, the likelihood is then given by
 \begin{equation*}
  P(a|q) = \int_0^1 \int_0^1 I(f_{aa}+f_{hh}<1) P(a|f_{aa}, f_{hh})P(f_{aa}, f_{hh}|q) df_{aa} df_{hh}
 \end{equation*}
 where $I(f_{aa}+f_{hh}<1)$ is an indicator function that is 1 if $f_{aa}+f_{hh}<1$ and 0 otherwise.} 

 \item Calculate the necessary equations for implementing an EM algorithm to solve this question. Start with the likelihood of the full data.
 \item Implement it in R and find the maximum likelihood estimate for $N=100$ and $a=5$. Compare it to the standard estimator of $q$ using HWE equations and comment.\opt{withSolutions}{\\
 \textbf{Solution}
 Note: the EM derivation is actually not as trivial as I thought, as we deal with a special case. Hence, there is no need to do this exercise.}
 
 \item Write down an MCMC algorithm to estimate $q$.\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item Assuming Hardy-Weinberg equilibrium, the likelihood is given by
 \begin{equation*}
 P(a|q) = \binom{N}{a}q^{2a}(1-q^2)^{N-a}                                                                      
 \end{equation*}
 \item When proposing new values $q'$ from $q$ using a symmetrix transition kernel and using a uniform prior on $q$, the hastings ration is then given by
 \begin{equation*}
  h = \frac{P(a|q')P(q')}{P(a|q)P(q)} = \frac{q'^{2a}(1-q'^2)^{N-a}}{q^{2a}(1-q^2)^{N-a}}
 \end{equation*}.
 \end{compactitem}}
 
 \item Implement the MCMC in R.\opt{withSolutions}{\\
 \textbf{Solution}\\
 \texttt{len <- 10000; \\
burnin <- 1000; \\
q <- numeric(len+1) \\
q[1] <- 0.5; \\
eps <- 0.05; \\
N <- 100; \\
a <- 5; \\
likelihoodNoBinom <- function(q, N, a){ \\
  return (q\textasciicircum(2*a)*(1-q*q)\textasciicircum(N-a)); \\
} \\
for(i in 1:len){ \\
  new <- q[i] + (runif(1)-0.5)*eps; \\
  if(new > 1){ new <- 2 - new; } \\
  if(new < 0){ new <- -new; } \\
  h = likelihoodNoBinom(new, N, a) / likelihoodNoBinom(q[i], N, a); \\
  if(runif(1)<h){ q[i+1] <- new; } \\
  else { q[i+1] <- q[i]; }   \\
} \\
plot(density(q[burnin:(len+1)]), xlim=c(0,1))}}
\end{compactenum}


\section{Running mice}
In an experiment, the heart rate of a mouse is measured every five minutes during a full day. Assume that the mouse has only three types of behaviors (B): sleeping (S), eating (E) and running (R). Assume further that the behavior follows a first order Markov Chain with the illustrated transition probabilities:

\begin{figure}[!h]
\includegraphics[width=6cm]{HMM_exercise.pdf}
\end{figure}

\begin{compactenum}[a)]
 \item What is the probability $x=P(B_t=R|B_{t-1}=R)$?\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item The sum of all probabilities leading from $R$ must sum to 1. Hence, $x=P(B_t=R|B_{t-1}=R)=0.7$
 \end{compactitem}
 \item What is the stationary distribution of the Markov Chain?\\
 \textbf{Solution}\begin{compactitem}
 \item The stationary distribution is defined as 
\begin{equation*}
 P_{\infty}(i) = \sum_{j \in \{S, E, R\}} P(B_t=i|B_{t-1}=i)P_{\infty}(j)
\end{equation*}
for $i \in \{S, E, R\}$.
\item In our case, this leads to the following system of equations:
\begin{align*}
P_{\infty}(S) &= 0.1 P_{\infty}(E) + 0.9 P_{\infty}(S)\\
P_{\infty}(E) &= 0.1 P_{\infty}(S) + 0.5 P_{\infty}(E) + 0.3 P_{\infty}(R)\\
P_{\infty}(R) &= 0.4 P_{\infty}(E) + 0.7 P_{\infty}(R)\\
1 &= P_{\infty}(S) + P_{\infty}(E) + P_{\infty}(R)
\end{align*}
\item Solving this system yields $S = E = 0.3$ and $R = 0.4$
 \end{compactitem}}
  
 \item Assume that the heart rate $h$ is a function of the behavior of the mouse, but rather noisy. Specifically, assume that $h_t$ is normally distributed with $h_t \sim {\cal N}(\mu_i, \sigma_i)$ for $i \in \{S, E, R\}$. The probability density function at $x$ is thus denoted by $\Phi(x, \mu_i, \sigma_i)$. Describe how to calculate the likelihood of a series of measurements $h_0, \ldots, h_T$ and assuming that the initial probability distribution of $h_0$ is given by the stationary distribution of the Markov Chain.\opt{withSolutions}{\\
 \textbf{Solution}\begin{compactitem}
 \item The model is best described as a hidden Markov model (HMM) where the hidden state denotes the behavior of the mouse and the observed data the heart rate.
 \item The likelihood of a HMM is obtained by first calculating the forward variable $\alpha_t(B_t=i)$ for each hidden state $i \in \{S,E,R\}$ and each time point $0, \ldots, T$ by first calculating $\alpha_0{i}$ as the product of the 
 \begin{equation*}
  \alpha_0(B_t=i) = \phi(h_0, \mu_i, \sigma_i)P_{\infty}(i)
 \end{equation*}
 and then the forward recursion for each $t = \{1, \ldots, T\}$ and each hidden state $i \in \{S,E,R\}$ as 
 \begin{equation*}
  \alpha_t(B_t=i) = \phi(h_0, \mu_i, \sigma_i) \sum_{j \in \{S,E,R\}} \alpha_{t-1}(j) P(B_t=i|B_{t-1}=j) 
 \end{equation*}
 \end{compactitem}}
\end{compactenum}


\end{document}
