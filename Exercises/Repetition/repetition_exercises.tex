\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[usenames,dvipsnames]{color}
\usepackage[alwaysadjust]{paralist}
\usepackage[pdftex]{graphicx}
\DeclareMathOperator{\E}{\mathbb{E}}
\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

\usepackage[withSolutions]{optional}
%\usepackage[withoutSolutions]{optional}

\newcommand{\sol}[2][]{\opt{withSolutions}{{\color{red} \textbf{#1} \texttt{#2}}}}
\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\def\P{\mathbb{P}}
\def\l{{\ell}}
\def\L{{\cal L}}

%opening
\title{Machine Learning - Refreshing Exercises}
\begin{document}

\maketitle

\begin{enumerate}[1.]

 %------------------------------------------------------------------------
% Question Factorizations
%------------------------------------------------------------------------

 \item Consider the joint distribution $\P(A,B, C,D,E,F)$.

\begin{enumerate}
\item How do you calculate $\P(B,C,D,E,F)$?
\sol{\\$\P(B,C,D,E,F) = \sum_A \P(A,B, C,D,E,F)$}

\item Write down three different factorizations of the full joint distribution $\P(A,B, C,D,E,F)$.
\sol[Possible solutions]{\\$\P(A,B, C,D,E,F) = \P(A)\P(B|A)\P(C|A,B)\P(D|A,B,C)\P(E|A,B,C,D)\P(F|A,B,C,D,E)$\\
 $\P(A,B, C,D,E,F) = \P(E)\P(F|E)\P(C|E,F)\P(D|C,E,F)\P(B|C,D,E,F)\P(A|B,C,D,E,F)$}

\item Assume that $A \bot B | C$, $D \bot F | E$ and $E \bot A | B$. Provide a factorization of the full joint distribution $\P(A,B, C,D,E,F)$ where these conditional independences help to simplify the equation.
\sol{\\$\P(A,B, C,D,E,F) =\P(B)\P(C|B)\P(A|B,C)\P(E|A,B,C)\P(F|A,B,C,E)\P(D|A,B,C,E,F)\\ = \P(B)\P(C|B)\P(A|C)\P(E|B,C)\P(F|A,B,C,E)\P(D|A,B,C,E)$}
\end{enumerate}

%------------------------------------------------------------------------
% Question True False
%------------------------------------------------------------------------

 \item Which of the following statements are true? Which are false? Explain why.
 \begin{enumerate}
  \item The maximum likelihood estimate is the best possible estimator.
  \sol{\\True and false. That very much depends on your world view. In a frequentist setting, the MLE is an optimal estimator. But in a Bayesian world, it is not.}

  \item The Expectation-Maximization (EM) algorithm is guaranteed to find the maximum likelihood estimate (MLE).
  \sol{\\False! The EM algorithm is guaranteed to always increase the likelihood. But if the likelihood surface is very rugged (i.e. has many different peaks), the EM may find a local peak instead of the MLE.}

  \item A grid search to find the MLE is problematic in high dimensions.
  \sol{\\True. In high dimensions, the number of grid points to evaluate becomes very large. This is why algorithms to either directly find the MLE (e.g. EM) or to generate samples from the posterior (e.g. MCMC) have been developed.}

  \item The reason why Markov Chain Monte Carlo (MCMC) chains are used a lot in Bayesian statistics is because the product of the prior and the likelihood is difficult to calculate.
  \sol{\\False. The product of the likelihood and the prior has to be calculated also for an MCMC. Also, it is usually not difficult to calculate. The true reason is two fold. 1) It allows to generate samples from the posterior and hence gets around the issue of a grid in high dimensions. 2) The probability of the data $\P(D) = \int \P(D|\theta)\P(\theta)$, which is very difficult to calculate in many cases, cancels out from the hastings ration. Note: in general, products are OK, integrals are not!}

  \item The values of the last iteration of a Markov Chain Monte Carlo (MCMC) chain are closer to the true values than the values in the middle of the chain.
  \sol{\\False! An MCMC is generating random (but autocorrelated) samples from the posterior distribution. There is no reason why the last value should be a better sample than any other value in the chain (excluding the burnin phase).}

  \item A Directed Acyclic Graph (DAG) shows causal relations.
  \sol{\\False. A DAG shows conditional independence, not causality. But if causality is known, it makes a lot of sens to parameterize a model such that causality is reflected by the DAG.}
 \end{enumerate}

%------------------------------------------------------------------------
% Question Insects
%------------------------------------------------------------------------

\item Consider a species of insects in which males and females are very hard to distinguish anatomically without killing the individuals. However, it is known that males and females have different probabilities $\theta_F$ and $\theta_M$ to fly away when exposed to a specific chemical substance X. In an experiment to infer the fraction of females $f$ in the population, $N$ individuals from a population are drawn, exposed to X, and the behavior $b_i \in \{0,1\}$ of each individual $i=\{1, \ldots, N\}$ is recorded where $b_i=0$ indicates that individual $i$ stayed (did not fly away). Let us denote the unknown sex of each sampled individual by $s_i \in \{M,F\}$.
\begin{enumerate}
\item Draw a directed acyclic graph (DAG) of this model.
\sol{ \begin{figure}[!h]
 \centering
 \includegraphics[width=6cm]{dag_insects.pdf}
 \end{figure}}

\item Write down the likelihood $\L(f)=P(\boldsymbol{B}|f)$ with $\boldsymbol{B}=\{b_i, \ldots, b_N\}$.
\sol{\begin{compactitem}
 \item The likelihood of one individual is given by summing over both sexes $$\P(b_i|f)=\sum_{z\in\{M,F\}}\P(b_i|s_i=z)\P(s_i=z|f)=b_i\big(\theta_Ff+\theta_M(1-f)\big) + (1-b_i)\big((1-\theta_F)f+(1-\theta_M)(1-f)\big)$$
 \item Since all individuals are independent when conditioning on $f$, $$\L(f)=\P(\boldsymbol{B}|f)=\prod_{i=1}^N  \Big[ b_i\big(\theta_Ff+\theta_M(1-f)\big) + (1-b_i)\big((1-\theta_F)f+(1-\theta_M)(1-f)\big) \Big]$$
 \end{compactitem}}

\item Write down the complete log likelihood of the model $\l_c(f)= \log \P(\boldsymbol{B}, \boldsymbol{S}|f)$ with $\boldsymbol{B}=\{b_i, \ldots, b_N\}$ and $\boldsymbol{S}=\{s_i, \ldots, s_N\}$.
\sol{\begin{compactitem}
 \item The complete likelihood is simpler, as we do not need to integrate over the sex. Hence we get, for one individual $\P(b_i, s_i)=\P(b_i|s_i)\P(s_i|f)$
 \item Since individuals are independent, we need to take the product across all individuals. In the log, this gets us $$\l_c(f)= \log \P(\boldsymbol{B}, \boldsymbol{S}|f) = \sum_i^N \log \P(b_i|s_i) + \sum_i^N \log \P(s_i|f) $$
 \end{compactitem}}

%ToDo: next time ask them to calculate it!
\item Describe in words how one should proceed to implement an EM algorithm to obtain the maximum likelihood estimate of $f$.
\sol{\begin{compactitem}
 \item The principle of the algorithm is to first (E step) calculate the expected complete data likelihood given the observation and the old parameter $\bar{f}$, denoted by $Q(f, \bar{f})$. This obtained as the weighted average over all possible hidden states.
 \item Then, in the M step, the new parameter estimate of $f$ is obtained a sthe value of $f$ that maximizes $Q(f, \bar{f})$ is obtained by taking the derivative of $Q(f, \bar{f})$ and setting it to zero.
 \item The procedure is repeated until the estimates of $f$ do not change any more.
 \end{compactitem}}
\item When running the EM, do you expect the starting point to influence the result? If yes, why? If not, why not?
\sol{\begin{compactitem}
 \item Yes, very much so. The reason is that the likelihood surface is multi peaked.
 \item This is easy to see as first, all sexes can be switched along with the values of $\theta_F$ and $\theta_M$ to obtain the same likelihood.
 \item Second, since there is only a single measurement per individual, a model in which there are only males and $\theta_M$ corresponds to the fraction of individuals that flew away in the total sample would also maximize the likelihood.
 \item A way out of the dilemma would be to use \textit{a prioi} information on $f$, $\theta_F$ and / or $\theta_M$ and infer the parameters in a Bayesian framework.
 \end{compactitem}}
\end{enumerate}

%------------------------------------------------------------------------
% Question Pineapples
%------------------------------------------------------------------------

\item Consider an experiment in which the effect $\varphi$ of a fertilizer on the weight of pineapples was established. In the experiment, each plant $i=\{1, \ldots, N\}$ was provided with a different amount of fertilizer $f_i$ and the weight of the pineapple $w_i$ they produced was measured. The goal is now to estimate the effect of the fertilizer in a Bayesian framework using an MCMC and a simple model under which the weight of a pineapple is a function of the amount of fertilizer provided with a Gaussian (normal) noise. Specifically, it is assumed that the weight of a pineapple is given by
\begin{equation*}
w_i = w_0 + \varphi \log (1+f_i) + \epsilon
\end{equation*}
 where $\epsilon \sim {\cal N}(0, \sigma)$ is normally distributed with mean 0 and standard deviation $\sigma$. Under this model, the likelihood of a single measurement is given by the density of a normal distribution with mean $\mu = w_0 + \varphi \log (1+f_i)$ and standard deviation $\sigma$. Uniform priors will be used on $w_0$ and $\sigma$, but the prior on $\varphi$ shall be normal with mean 0 and standard deviation 1. The density of a normal distribution with mean $\mu$ and standard deviation $\sigma$ is given by
\begin{equation*}
 f(x) = \displaystyle \frac{1}{\sigma \sqrt{2\pi}}e^{-\frac{(x - \mu)^2}{2\sigma^2}}
\end{equation*}

 \begin{enumerate}
\item Write down the likelihood $\L(\varphi, w_0, \sigma)=P(w_1, \ldots, w_N|\varphi, w_0, \sigma, f_1,\ldots, f_N)$ for the full data, assuming plants to be independent.
\sol{\begin{compactitem}
 \item Since plants are independent, the full likelihood is just the product across the likelihood of each individual.
 $$\L(\varphi, w_0, \sigma)=\P(w_1, \ldots, w_N|\varphi, w_0, \sigma, f_1,\ldots, f_N)=\prod_{i=1}^N \P(w_i|\varphi, w_0, \sigma, f_1) = \prod_{i=1}^N \displaystyle \frac{1}{\sigma \sqrt{2\pi}}e^{-\frac{(w_i - w_0 - \varphi \log (1+f_i))^2}{2\sigma^2}} $$
 \item Note that this is a great example in which the natural log of the likelihood is MUCH simpler: $$\l(\varphi, w_0, \sigma) = -N\log(\sigma \sqrt{2\pi}) + \sum_{i=1}^N -\frac{(w_i - w_0 - \varphi \log (1+f_i))^2}{2\sigma^2}$$
 \end{compactitem}}

\item Let's now develop an MCMC in which each parameter is updated individually. Propose a symmetric proposal kernel for each parameter that respects the nature of the parameter.
\sol{\begin{compactitem}
 \item There are multiple options, including a uniform or normal kernel, both with mean of the current value. The uniform kernel for $\varphi$, for instance, could look like $ \varphi' = \varphi + ux - x/2$, where $\varphi'$ would be the new value, $u$ is a uniform random deviate within $[0,1]$ and $x$ is the size of the proposal interval.
 \item However, we have to be aware that $w_0$ and $\sigma$ cannot be negative, and hence we need to mirror their values at 0.
 \end{compactitem}}

\item Write down the hastings ratios for each parameter update and simplify as much as possible.
\sol{\begin{compactitem}
 \item Assuming a symmetric proposal kernel, the hastings ratio is given by \begin{align*}h_{\varphi} &= \min \left(1, \frac{\P(w_1, \ldots, w_N|\varphi', w_0, \sigma, f_1,\ldots, f_N) \cdot \P(\varphi')}{\P(w_1, \ldots, w_N|\varphi, w_0, \sigma, f_1,\ldots, f_N) \cdot \P(\varphi)} \right)\\ 
&=\min \left(1, \frac{\prod_{i=1}^N \displaystyle \frac{1}{\sigma \sqrt{2\pi}}e^{-\frac{(w_i - w_0 + \varphi' \log (1+f_i))^2}{2\sigma^2}} \cdot \frac{1}{\sqrt{2\pi}}e^{-\frac{\varphi'^2}{2}}}{\prod_{i=1}^N \displaystyle \frac{1}{\sigma \sqrt{2\pi}}e^{-\frac{(w_i - w_0 + \varphi \log (1+f_i))^2}{2\sigma^2}} \cdot \frac{1}{\sqrt{2\pi}}e^{-\frac{\varphi^2}{2}}} \right)\\
&=\min \left(1, \frac{\prod_{i=1}^N e^{-\frac{(w_i - w_0' + \varphi' \log (1+f_i'))^2}{2\sigma'^2}} \cdot e^{-\frac{\varphi'^2}{2}}}{\prod_{i=1}^N e^{-\frac{(w_i - w_0 + \varphi \log (1+f_i))^2}{2\sigma^2}} \cdot e^{-\frac{\varphi^2}{2}}} \right)\end{align*}
\item Note again that calculating the hastings ratio in the log would ease some calculations a lot, as there would be less calls to the exp function.
 \end{compactitem}}

\item Describe in words what is meant by convergence of an MCMC and how you could test for it.
\sol{Convergence means that the samples generated follow the stationary distribution of the Markov chain. Or, in other words, an MCMC chain that has converged will not change the sample distribution when running for longer. A simple way for testing this is to run multiple chain sin parallel and to check if they result in the same distribution. A classic statistics testing for this is the Gelman \& Rubin statistics.}

\item Describe in words why the beginning of an MCMC chain is usually discarded as burnin.
\sol{The problem is that an MCMC chain may be starting in a very unlikely position. So in order for the chain to converge, it has to be run for a very long time. Imagine, for instance, it is started in a location with posterior probability $10^{-100}$. Since the first sample is from that region, then chain mus run for at least $10^{100}$ iterations to properly reflect this probability. Also, when a chain is started in a bad location, the likelihood might be so flat that it takes the chain for ever to reach the ``good'' parameter region, further increasing the problem of forgetting its starting location. And easy way to find a good starting location is to let the MCMC find it itself. For that reason, we just let the MCMC run for a bit until it has found a good starting location, and are then using the samples only from this point onward (and hence discarding the initial samples as burnin).}
\end{enumerate}

%------------------------------------------------------------------------
% Question Axon
%------------------------------------------------------------------------

\item Consider a stretch of an axon of a neural cell that is either in resting state (R), produces an action potential (A) or is in the refractory period (Z). Over some time, the voltage between the inner and outer space of the axon is measured, yet with a big deal of noise. Specifically, let us assume that the measurement in the different states  yields a value that is normally distributed with mean of the true voltage in those states $V_Z$, $V_A$ and $V_R$, respectively, and with a standard deviation of $\sigma$.

Let us further assume that the states of the axon can be well described using the following Markov Model with a time interval of 0.1 seconds (a change in state is possible only every 0.1 seconds):
  \begin{figure}[!h]
  \centering
 \includegraphics[width=6cm]{Axon_HMM.pdf}
 \end{figure}

 \begin{enumerate}
 \item If it was known that the axon is on average (stationary distribution) 50\% of the time in the resting state, what would be the transition probability $x$? What  the stationary distribution in states A and Z?
 \sol{\begin{compactitem}
  \item To solve this problem, we first need to write down the conditions for convergence (note: they can all be simplified a lot):
  \begin{align*}
   1 &= \pi_R + \pi_A + \pi_Z\\
   \pi_A &= 0.8\pi_A + x \pi_R &\rightarrow \pi_A&= 5x \pi_R\\
   \pi_R &= (1-x)\pi_R + 0.2 \pi_z &\rightarrow \pi_R&= \frac{0.2}{x}\pi_Z\\
   \pi_Z &= 0.8 \pi_Z + 0.2 \pi_A &\rightarrow \pi_Z&= \pi_A\\
  \end{align*}
  \item Next, we need to solve this set of equation for x as a function of $\pi_R$. An easy way to do this is to replace $\pi_Z$ with $\pi_A$ in the first equation, and solving it for $\pi_A$: $$1 = \pi_R + 2\pi_A \rightarrow \pi_A = \frac{1-\pi_R}{2}$$
  \item Finally, we can substitute this into the second equation and solve for x: $$ \frac{1-\pi_R}{2} = 5x\pi_R \rightarrow x = \frac{1-\pi_R}{10 \pi_R} = 0.1$$
      \end{compactitem}}

 \item Consider that the voltage was measure every 0.1 seconds $n$ times in a row. How do you calculate the likelihood under the model for this data? Describe briefly in words, and then write pseudo \texttt{R} code assuming that the measurements are in a vector \texttt{n} and the transition probabilities are in a matrix \texttt{P} such that \texttt{P[i][j]} gives the probability to jump from state \texttt{i} to state \texttt{j}.
 \sol{\begin{compactitem}
       \item Since this models corresponds to a Hidden Markov Model, we can use the forward algorithm to calculate the likelihood of this model.
       \item To run the forward algorithm, we need the transition matrix (given by P) and the emission probability, which is given by a normal distribution. We can thus use the function dnorm() of R to calculate the emission probabilities.
       \item An issue is always how to start. It often makes sense to assume that the HMM chain starts from the stationary distribution, and we will assume here that the stationary distribution is available in the vector S.
       \item The R code would thus look like this (assuming R=1, A=2, Z=3):\\
\script{\textgreater\ \#Calculate first forward values\\
  \textgreater\ for(i in 1:3)\{\\
  \textgreater\ \ \ forward[i] <- S * dnorm(n[1], mean=V[i], sd=sigma);\\
 \textgreater\ \}
  \\
  \textgreater\ \#now make recursion\\
 \textgreater\ for(l in 2:len)\{\\
 \textgreater\ \ \ \#swap entries\\
  \textgreater\ \ \ forward\_old = forward;\\
  \textgreater\ \ \ \#calculate forward\\
  \textgreater\ \ \ for(i in 1:3)\{\\
  \textgreater\ \ \ \ \ forward[i] <- 0;\\
  \textgreater\ \ \ \ \ for(j in 1:3)\{\\
  \textgreater\ \ \ \ \ \ \ forward[i] <- forward[i] + (forward\_old[j] * P[j][i]);\\
 \textgreater\ \ \ \ \ \}\\
 \textgreater\ \ \ \ \ forward[i] <- forward[i] * dnorm(n[l], mean=V[i], sd=sigma);\\
 \textgreater\ \ \ \}\\
 \textgreater\ \}\\
 \textgreater\ LL <- log(sum(forward));}
      \end{compactitem}
}

 \item What would change if the measurements were taken only every second?
 \sol{Not much. One would still have to calculate the forward algorithm as described above. However, since there is no data at some points, the emission probabilities for those time points will always be 1. An elegant alternative to this is to take the matrix P to the power of 10, and then directly jump always ten steps at once. This works since 10 transitions are well described by multiplying the transition matrix ten times.}
 \item Describe how you would implement an MCMC to estimate $\sigma$ and all $V_k$ with $k\in\{R,A,Z\}$.
 \sol{This is done as for every MCMC:\begin{compactenum}[1)]
    \item Implement a function to propose new parameters values using a symmetric transition kernel.
    \item Choose a random starting position.
    \item Now loop through the chain length, always repeating the following steps:\begin{compactenum}[I.]
    \item Propose a new value (it is probably best to update only one parameter in each iteration).
    \item Calculate the likelihood with this parameter combination using the forward algorithm.
    \item calculate the hastings ratio $h$ as the ratio of the likelihood times the ration of the prior density for the updated parameter.
    \item Throw a random number to accept or reject the proposed parameter with probability $h$.
    \item Record the current parameter values.
    \end{compactenum}
    \end{compactenum}}

 \end{enumerate}

\end{enumerate}




\end{document}
