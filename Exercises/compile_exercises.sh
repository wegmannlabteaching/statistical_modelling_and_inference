#!/bin/bash
#compile all exercises with solutions

#takes as argument a list 
these=$(echo $@)

if [ -z "$these" ]; then
	these=$(ls -d ML_Exercises_[0-9]* | sed 's/ML_Exercises_//' | sort -n | awk '{print "ML_Exercises_" $1}')
else 
	these=$(echo "$these" | sort -n | tr ' ' '\n' | awk '{print "ML_Exercises_" $1 "_*"}' | tr '\n' ' ' | sort -n)
fi

for fol in $these; do
	echo -n "Compiling ${fol} ..."
	cd $fol	
	
	#with solutions	(compile twice to get references right)
	sed -i 's/\\documentclass{\.\.\/exercises}/\\documentclass\[solutions\]{\.\.\/exercises}/' ${fol}.tex
	pdflatex ${fol}.tex > ${fol}.solutions.log
	pdflatex ${fol}.tex > ${fol}.solutions.log
	mv ${fol}.pdf ${fol}_solutions.pdf

	#without solutions (compile twice to get references right)
	sed -i 's/\\documentclass\[solutions\]{\.\.\/exercises}/\\documentclass{\.\.\/exercises}/' ${fol}.tex
	pdflatex ${fol}.tex > ${fol}.log
	pdflatex ${fol}.tex > ${fol}.log
	
	#clean up
	rm *.aux *.log *.out
	cd ..
	
	echo " done!"
done

