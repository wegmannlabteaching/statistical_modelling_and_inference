\documentclass{../exercises}

\exerciseTitle{10}{Empirical Bayes}

\def\z{\boldsymbol{z}}
\def\n{\boldsymbol{n}}
\def\bmu{\boldsymbol{\mu}}

\begin{document}

\maketitle

\begin{enumerate}[1.]
\item Consider $M$ loci, consisting each of aligned DNA sequences of length $L$ of two species that diverged $T$ generations ago ($T$ is known). Each of these $m = 1, \ldots, M$ loci shall differ at $n_m$ and be identical at $L-n_m$ bases. We assume that these differences are shaped by the standard mutation rate $\mu_0$ for most loci, but that some loci show a higher mutation rate $\mu_1$. The relevant mutation rate class $\mu_{z_m}$ for each locus shall be denoted by $z_m = \{0,1\}$. The probability of $z_m$ is given by a Bernouilli distribution with parameter $\pi$:
\begin{equation*}
 \P(z_m |\pi) = \pi ^{z_m} (1-\pi)^{1-z_m} = \begin{cases}
                            1-\pi & \quad \mbox{if $z_m=0$}\\
                            \pi & \quad \mbox{if $z_m=1$}
                           \end{cases}.
\end{equation*}

Assuming an infinite sites model, the number of differences $n_m$ is Poisson distributed as $n_m \sim \mathrm{Pois}(2TL\mu_{z_m})$ such that
\begin{equation*}
 \P(n_m | \mu_{z_m}) = \frac{1}{n_m!}(2TL\mu_{z_m} )^{n_m} e^{-2TL\mu_{z_m}}.
\end{equation*}

The goal is now to obtain a Bayesian estimate of the mutation class of each locus. Under a fully Bayesian approach, this would imply the following steps:
\begin{itemize}
 \item Choosing prior distributions on $\bmu=(\mu_0, \mu_1)$ and $\pi$.
 \item Obtaining posterior samples $\P(\z,\bmu,\pi|\n)$, where $\n=(n_1, \ldots, n_M)$.
 \item Marginalizing the posterior distribution as
$$P(z_m | \n) = \int \P(\z, \bmu, \pi | \n) \mathrm{ d}  \boldsymbol{\theta}_{-m}; \quad \boldsymbol{\theta}_{-m} = (\boldsymbol{z}_{-m}, \bmu, \pi ),$$
where $\boldsymbol{z}_{-m}$ indicates all $\boldsymbol{z}$ except $z_m$.
\end{itemize}

Here, we will take an Empirical Bayes approach by obtaining maximum likelihood estimates of $\hat{\bmu}$ and $\hat{\pi}$, and then evaluating the posterior $\P(z_m | n_m, \hat{\bmu}, \hat{\pi})$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Empiricöl Bayes solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{enumerate}[a)]
\item Draw a DAG of this model.

\sol{\begin{tikzpicture}
            \node[latent] (pi) {$\pi$};
            \node[latent, right = of pi] (z) {$z_m$};
            \node[obs, right=of z] (n) {$n_m$};
            \node[latent, above = of n] (mu) {$\bmu$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate1} {(z) (n)} {$m=1,\ldots,M$};
            \edge {pi} {z};
            \edge {z} {n};
            \edge {mu} {n};
            \end{tikzpicture}}
\item Write out the posterior distribution of $z_m$ given known values $\hat{\bmu}$ and $\hat{\pi}$.

\sol{
\begin{equation*}
 \P(z_m | n_m, \hat{\bmu}, \hat{\pi}) = \frac{\P(n_m|\hat{\mu}_{z_m})\P(z_m|\hat{\pi})}{\sum_{k \in \{0,1\}} \P(n_m|\hat{\mu}_k)\P(k|\hat{\pi})}.
\end{equation*}
}
\item To obtain the MLE estimates $\hat{\bmu}$ and $\hat{\pi}$, we need an EM algorithm. Begin by writing out the likelihood $\L(\bmu, \pi)$, the complete data likelihood ${\cal L}_c(\bmu, \pi)$ and log complete data likelihood $\ell_c(\bmu, \pi)$ for this model.

\sol{
The likelihood $\L(\bmu, \pi)$ is calculated by integrating over the unknown hidden state:
\begin{eqnarray*}
\L(\bmu, \pi) &=& \P(\n|\bmu, \pi) = \prod_{m=1}^M  \sum_{z_m} \P(n_m|\mu_{z_m}) \P(z_m| \pi).\\
{\cal L}_c(\bmu, \pi) &=& \P(\n, \boldsymbol{z} | \bmu, \pi) = \P(\n | \boldsymbol{\mu_{\boldsymbol{z}}}) \P(\boldsymbol{z} | \pi) = \prod_{m=1}^M \P(n_m | \mu_{z_m}) \P(z_m|\pi).\\
\ell_c(\bmu, \pi) &=& \log {\cal L}_c(\bmu, \pi) = \sum_{m=1}^M \left[\log \P(n_m | \mu_{z_m}) + \log \P(z_m|\pi)\right]\\
 &=& \sum_{m=1}^M \left[ -\log(n_m!) + n_m \log(2TL\mu_{z_m}) - 2TL\mu_{z_m} + z_m \log \pi + (1-z_m) \log (1-\pi) \right].
\end{eqnarray*}
}

\item Write out $Q(\bmu, \pi | \bmu', \pi')= \E \left[\ell_c(\bmu, \pi)|\n, \bmu', \pi' \right]$.  This expectation is obtained as a weighted average over all possible hidden states $\boldsymbol{z}$, weighted by their probability given $\bmu', \pi'$ and the data $\n$. You may use the term $\P(z_m|n_m, \bmu', \pi')$ to indicate the weights.

\sol{
\begin{equation*}
Q(\bmu, \pi | \bmu', \pi') =
\sum_{m=1}^M  \sum_{z_m} \left[ -\log(n_m!) + n_m \log(2TL\mu_{z_m}) - 2TL\mu_{z_m} + z_m \log \pi + (1-z_m) \log (1-\pi) \right] \P(z_m|n_m, \bmu', \pi').
\end{equation*}
}
\item Express the weights $\P(z_m|n_m, \bmu', \pi')$ in terms of the defined probabilities $\P(n_m|\mu_{z_m}')$ and $\P(z_m|\pi')$.

\sol{
\begin{eqnarray*}
 \P(z_m|n_m, \bmu', \pi') &=& \frac{\P(n_m|\mu_{z_m}') \P(z_m |\pi')}{\P(n_m)}\\
 &=& \frac{\P(n_m|\mu_{z_m}') \P(z_m |\pi')}{\sum_{k\in\{0,1\}} \P(n_m|\mu_{k}') \P(k |\pi')}
\end{eqnarray*}
}
\item For the maximization step, we need to find the values that maximize $Q(\bmu, \pi | \bmu', \pi')$. Find derivative of $Q(\bmu, \pi | \bmu', \pi')$ with respect to $\mu_0$, $\mu_1$ and $\pi$, respectively.

\sol{
Note that the weights $\P(z_m|n_m, \bmu', \pi')$ do not depend on the parameters $\bmu, \pi$, but on the old parameters $\bmu', \pi'$, and are hence constants. There is thus no need to write them out during the derivation.
The derivative with respect to $\mu_0$ then is
\begin{eqnarray*}
 \frac{\partial Q(\bmu, \pi | \bmu', \pi')}{\partial \mu_0} &=& \sum_{m=1}^M  \left( \frac{n_m}{\mu_0} - 2TL \right) \cdot \P(z_m = 0|n_m, \bmu', \pi')\\
 &=& \frac{1}{\mu_0}\sum_{m=1}^M  n_m \P(z_m = 0|n_m, \bmu', \pi') - 2TL \sum_{m=1}^M  \P(z_m = 0|n_m, \bmu', \pi').
\end{eqnarray*}
The derivative with respect to $\mu_1$ is analogous:
\begin{equation*}
 \frac{\partial Q(\bmu, \pi | \bmu', \pi')}{\partial \mu_1} = \frac{1}{\mu_1}\sum_{m=1}^M  n_m \P(z_m = 1|n_m, \bmu', \pi') - 2TL \sum_{m=1}^M  \P(z_m = 1|n_m, \bmu', \pi').
\end{equation*}
The derivative with respect to $\pi$ is
\begin{equation*}
 \frac{\partial Q(\bmu, \pi | \bmu', \pi')}{\partial \pi} = \sum_{m=1}^M \left[ \frac{1}{\pi} \cdot \P(z_m = 1|n_m, \bmu', \pi') - \frac{1}{1-\pi} \cdot \P(z_m = 0|n_m, \bmu', \pi') \right].
\end{equation*}
}

\item Now set these derivatives to zero and solve the resulting system of linear equations for $\mu_0$, $\mu_1$ and $\pi$.

\sol{
\begin{eqnarray*}
 \hat{\mu}_0 &=& \frac{\displaystyle \sum_{m=1}^M n_m \P(z_m = 0|n_m, \bmu', \pi')}{\displaystyle 2TL \sum_{m=1}^M \P(z_m = 0|n_m, \bmu', \pi') }\\
 \hat{\mu}_1 &=& \frac{\displaystyle \sum_{m=1}^M n_m \P(z_m = 1|n_m, \bmu', \pi')}{\displaystyle 2TL \sum_{m=1}^M \P(z_m = 1|n_m, \bmu', \pi') }\\
 \hat{\pi} &=& \frac{1}{M}\sum_{m=1}^M \P(z_m = 1|n_m, \bmu', \pi')
\end{eqnarray*}
}
\item \label{item:simulate} Implement a function to simulate data under this model. The function should take as input the number of sequences $M$, the time since divergence $T$, the length of the sequences $L$, the probability $\pi$ as well as the vector $\bmu$, and return $\boldsymbol{z}$ and the number of observed differences $\n$. Simulate some data with $M=1000$, $T=10^5, L=10^3$, $\pi=0.3$, $\mu_0=10^{-8}$ and $\mu_1=10^{-7.5}$.

\sol{See link. \solutions{ML_Exercises_10_EmpiricalBayes/ML_Exercises_10_EmpiricalBayes.R}
}
\item Implement the EM algorithm to estimate $\hat{\mu}_0$, $\hat{\mu}_1$ and $\hat{\pi}$ in \texttt{R}.

\sol{See link. \solutions{ML_Exercises_10_EmpiricalBayes/ML_Exercises_10_EmpiricalBayes.R}
}
\item Finally, compute the posterior on $\boldsymbol{z}$ with Empirical Bayes, using the point estimates $\hat{\mu_0}$, $\hat{\mu_1}$ and $\hat{\pi}$ that were obtained with the EM. Plot the posterior distribution of $\P(z_m = 1|n_m, \hat{\bmu}, \hat{\pi})$. Does the algorithm correctly recover the simulated mutation class?

\sol{See link. \solutions{ML_Exercises_10_EmpiricalBayes/ML_Exercises_10_EmpiricalBayes.R}
}

\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fully Bayesian solution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item (Optional) Implement the full Metropolis-Hastings MCMC algorithm for this model to obtain posterior samples of $\boldsymbol{z}$, $\bmu$ and $\pi$ using the following prior distributions:
\begin{equation*}
\mu_0,\mu_1 \sim \mathrm{Exp}(\lambda); \quad \pi \sim {\cal U}(0, 1).
\end{equation*}

\begin{enumerate}[a)]
\item Write out the posterior density $\P(\boldsymbol{z}, \bmu, \pi|\n)$, without proportionality constant.

\sol{
Since the prior distribution on $\pi$ is uniform, it drops out.
\begin{equation*}
\P(\n|\boldsymbol{z}, \bmu, \pi) \propto \P(\mu_0|\lambda)\P(\mu_1|\lambda)\prod_{m=1}^M \P(n_m|\mu_{z_m}) \P(z_m| \pi).
\end{equation*}
}

\item Write out the relevant Hastings ratios for the single parameter updates $\mu_0 \rightarrow \mu_0'$, $\mu_1 \rightarrow \mu_1'$, $z_m \rightarrow z_m'$ and $\pi \rightarrow \pi'$. Assume symmetric proposal kernels for all updates.

\sol{
\begin{eqnarray*}
h_{\mu_0} &=& \prod_{m=1}^M \frac{\P(n_m | \mu_0')^{1-z_m}}{\P(n_m | \mu_0)^{1-z_m}}\\
h_{\mu_1} &=& \prod_{m=1}^M \frac{\P(n_m | \mu_1')^{z_m}}{\P(n_m | \mu_1)^{z_m}}\\
h_{z_m} &=& \frac{\P(n_m | \mu_{z_m'}) \P(z_m' | \pi)}{\P(n_m | \mu_{z_m})\P(z_m | \pi) }\\
h_\pi &=& \prod_{m=1}^M \frac{\P(z_m| \pi')}{\P(z_m| \pi)}.
\end{eqnarray*}
}
\item Implement the Metropolis-Hastings MCMC algorithm for this model. To prevent the MCMC from switching all $z_m$ forth and back, impose the condition that new values of $\mu_1'$ must be strictly larger than $\mu_0$. Specifically, propose $\mu_1'$ inside a while loop until $\mu_1'  > \mu_0$.

\sol{See link. \solutions{ML_Exercises_10_EmpiricalBayes/ML_Exercises_10_EmpiricalBayes.R}
}
\item Estimate parameters for the data simulated in Exercise \ref{item:simulate}. As starting values, use the same values as you use to initialize the EM. Compare the posterior distribution of $\boldsymbol{z}$ obtained by the MCMC to Empirical Bayes, and try out different simulation parameters.
\end{enumerate}

\end{enumerate}
\end{document}
