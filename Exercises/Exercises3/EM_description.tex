\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\DeclareMathOperator{\E}{\mathbb{E}}
\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

%opening
\title{EM Algorithm}

\begin{document}

\maketitle

\section{Two Coins}

Suppose we have two biased coins $A$ and $B$ that produce a \textit{head} with unknown probabilities $\theta_A$ and $\theta_B$, respectively. In an experiment, a coin $z$ is chosen at random ($P(z=A)=\frac{1}{2}$) and always thrown $m$ times and the number \textit{heads} was recorded. Suppose we obtain the results (number of \textit{heads}) of $n$ such experiments as $\boldsymbol{x}=\{x_1, \ldots, x_n\}$.

If it was known for each experiment which coin was used, the likelihood would be easy to compute. Let us denote by $z_i=\{A,B\}$ the coin used in experiment $i=\{1, \ldots, n\}$. The likelihood of $\theta=\{\theta_A, \theta_B\}$ is then given by
\begin{equation*}
 P(\boldsymbol{x}|\boldsymbol{z}, \theta)=\displaystyle \prod_{i=1}^n \binom{m}{x_i} \theta_{z_i}^{x_i}(1-\theta_{z_i})^{m-x_i},
\end{equation*}
where $\boldsymbol{z}=\{z_1, \ldots, z_L\}$ is the vector of coins used.

In case $\boldsymbol{z}$ is unknown, the likelihood of $\theta$ is given by integrating over the unknown hidden state
\begin{equation*}
  P(\boldsymbol{x}|\theta)=\displaystyle \prod_{i=1}^n \left( \sum_{k \in \{A,B\}} \frac{1}{2} \theta_{k}^{x_i}(1-\theta_{k})^{m-x_i}\right)
\end{equation*}

In such cases, the EM algorithm can be used to find the maximum likelihood estimate $\hat{\theta}$. To use this algorithm, we first have to specify the complete data likelihood
\begin{equation*}
 \ell_c = P(\boldsymbol{x}, \boldsymbol{z}|\theta)=P(\boldsymbol{x}| \boldsymbol{z},\theta)P(\boldsymbol{z}|\theta)=\displaystyle \prod_{i=1}^n P(x_i| z_i,\theta)P(z_i)
\end{equation*}
The log of the complete likelihood is then
\begin{eqnarray*}
 \log(\ell_c)&=&\log P(\boldsymbol{x}| \boldsymbol{z},\theta) + \log P(\boldsymbol{z}|\theta)\\
 &=& \displaystyle \sum_{i=1}^n \left( \log P(x_i| z_i,\theta) + \log P(z_i) \right)\\
 &=& \displaystyle \sum_{i=1}^n  \left(x_i\log \theta_{z_i} + (m-x_i) \log (1-\theta_{z_i}) + \log \binom{m}{x_i} + \log P(z_i)\right)\\
 &=& \displaystyle \sum_{i=1}^n  \left(x_i\log \theta_{z_i} + (m-x_i) \log (1-\theta_{z_i}) \right) + C, \quad C \bot \theta
\end{eqnarray*}

\subsection{E-step}
Let us first define the expected complete data likelihood
\begin{equation*}
 Q(\theta, \bar{\theta})=\E\left[\log \ell_c|\boldsymbol{x}, \bar{\theta} \right],
\end{equation*}
where $\bar{\theta}$ is the current estimate of $\theta$. This expectation is obtained as a weighted average over all possible hidden states $\boldsymbol{z}$, weighted by their probability given $\bar{\theta}$ and the data $\boldsymbol{x}$ as
\begin{equation}\label{eq:Q}
Q(\theta, \bar{\theta}) =
\displaystyle \sum_{i=1}^n  \sum_{k\in\{A,B\}} \left( \left[x_i\log \theta_{k} + (m-x_i) \log (1-\theta_{k})\right] \cdot P(z_i=k|x_i, \bar{\theta}) \right)
\end{equation}
Note that the constant term that is independent of $\theta$ has been dropped, as it not relevant when maximizing $\ell_c$. Further and with $k\in\{A,B\}$,
\begin{eqnarray}
 P(z_i=k|x_i, \bar{\theta}) &=& \displaystyle \frac{P(x_i|z_i=k, \bar{\theta})P(z_i=k)}{P(x_i|\bar{\theta})}\nonumber\\
 &=& \displaystyle \frac{P(x_i|z_i=k, \bar{\theta})P(z_i=k)}{\displaystyle\sum_{j\in\{A,B\}} P(x_i|z_i=j, \bar{\theta})P(z_i=j)}\nonumber\\
&=&  \displaystyle \frac{\displaystyle \binom{m}{x_i} \bar{\theta}_k^{x_i}(1-\bar{\theta}_k)^{m-x_i} \frac{1}{2} }{\displaystyle \frac{1}{2} \binom{m}{x_i} \sum_{j\in\{A,B\}} \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}  }\nonumber\\
&=& \frac{\bar{\theta}_k^{x_i}(1-\bar{\theta}_k)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}} \label{eq:weight}
\end{eqnarray}


Substituting eq.  \ref{eq:weight} into eq. \ref{eq:Q} we obtain
\begin{eqnarray*}
 Q(\theta, \bar{\theta}) &=&
\displaystyle \sum_{i=1}^n  \sum_{k\in\{A,B\}} \left( \left[x_i\log \theta_{k} + (m-x_i) \log (1-\theta_{k})\right] \cdot \frac{\theta_k^{x_i}(1-\theta_k)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}} \theta_j^{x_i}(1-\theta_j)^{m-x_i}} \right) \\
&=& \displaystyle \sum_{i=1}^n \left( \left[x_i\log \theta_A + (m-x_i) \log (1-\theta_A)\right] \cdot \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}} \right)\\
&+& \displaystyle \sum_{i=1}^n \left( \left[x_i\log \theta_B + (m-x_i) \log (1-\theta_B)\right] \cdot \frac{\bar{\theta}_B^{x_i}(1-\bar{\theta}_B)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}} \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}} \right)
\end{eqnarray*}

\subsection{M-step}
The goal of the M-step is to find the values of $\theta$ that maximize $ Q(\theta, \bar{\theta})$,
\begin{equation*}
\hat{\theta}=\{\hat{\theta}_A, \hat{\theta}_B\} = \arg \max_{\theta} Q(\theta, \bar{\theta})
\end{equation*}

$\hat{\theta}$ will then serve as the new estimates of $\theta$ for the next iteration. To find the values that maximize $Q(\theta, \bar{\theta})$, we need to find the derivative of $Q(\theta, \bar{\theta})$ with respect to $\theta_A$ and $\theta_B$, respectively, and setting them to zero. Let's start with the derivation with respect to $\theta_A$. Using
\begin{equation}\label{eq:logderivrule1}
 \displaystyle \frac{d}{dx}a\log x= \frac{a}{x}
\end{equation}
and
\begin{equation}\label{eq:logderivrule2}
 \displaystyle \frac{d}{dx}a\log (b-x)= -\frac{a}{b-x}
\end{equation}
we obtain
\begin{eqnarray*}
 \displaystyle \frac{\partial Q(\theta, \bar{\theta})}{\partial \theta_A} &=&  \displaystyle \sum_{i=1}^n \left( \left( \frac{x_i}{\theta_A} - \frac{m-x_i}{1-\theta_A} \right) \cdot \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}\right)\\
  &=& \frac{1}{\theta_A} \left( \displaystyle \sum_{i=1}^n \frac{x_i \bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}\right) - \frac{1}{1-\theta_A} \left( \displaystyle \sum_{i=1}^n \frac{(m-x_i) \bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}\right)=0,
\end{eqnarray*}
which is of the form
\begin{eqnarray}
 \frac{U}{x} - \frac{V}{1-x}&=&0\nonumber\\
 U(1-x) - Vx&=&0\nonumber\\
 x &=& \frac{U}{U + V}\label{eq:UVlogic}
\end{eqnarray}

Re-substituting $U$ and $V$ we obtain
\begin{eqnarray*}
 \hat{\theta}_A = \arg \max_{\theta_A} Q(\theta, \bar{\theta}) &=& \frac{\displaystyle \sum_{i=1}^n x_i \cdot  \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}{\displaystyle \sum_{i=1}^n x_i \cdot \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}} + \displaystyle \sum_{i=1}^n (m-x_i) \cdot \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}\\
 &=& \frac{\displaystyle \sum_{i=1}^n x_i \cdot \frac{\bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}{\displaystyle \sum_{i=1}^n m \cdot\frac{ \bar{\theta}_A^{x_i}(1-\bar{\theta}_A)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}
\end{eqnarray*}

Analogously,
\begin{equation*}
 \hat{\theta}_B = \arg \max_{\theta_B} Q(\theta, \bar{\theta}) = \frac{\displaystyle \sum_{i=1}^n x_i \cdot \frac{\bar{\theta}_B^{x_i}(1-\bar{\theta}_B)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}{\displaystyle \sum_{i=1}^n m \cdot\frac{ \bar{\theta}_B^{x_i}(1-\bar{\theta}_B)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}}}
\end{equation*}


\section{Genotype Likelihoods}
In an next-generation sequencing experiment, multiple reads usually span the same locus for an individual and it is the aim to infer either the genotypes of a set of individuals at that locus and / or population genetic parameters such as the allele frequency from such data.
Suppose we observe $r_i$ reference (R) and $a_i$ alternative (A) alleles for individual $i$. The likelihood given the unknown genotype $g_i=\{0,1,2\}$ indicating the number of reference alleles (RR=2, RA=1, AA=0) and the error rate $\epsilon$ is then given by
\begin{eqnarray*}
 P(r_i, a_i|g_i, \epsilon) &=&\displaystyle \prod_{j=1}^{r_i}\left[ (1-\epsilon)\frac{g_i}{2} + \epsilon \frac{2-g_i}{2}\right] \displaystyle \prod_{j=1}^{a_i}\left[ (1-\epsilon)\frac{2-g_i}{2} + \epsilon \frac{g_i}{2}\right]\\
 &=& \left(\frac{1}{2}\right)^{r_i+a_i} \left[(1-\epsilon)g_i + \epsilon (2-g_i)\right]^{r_i} \left[(1-\epsilon)(2-g_i) + \epsilon g_i\right]^{a_i}\\
  &=& \left(\frac{1}{2}\right)^{r_i+a_i}  \left[g_i(1-2\epsilon)+2\epsilon\right]^{r_i} \left[g_i(2\epsilon-1)-2\epsilon+2\right]^{a_i}
\end{eqnarray*}

Assuming the population is in Hardy-Weinberg equilibrium at that locus and that the frequency of the reference allele is $f$,
\begin{equation*}
 P(g_i|f)=\binom{2}{g_i}f^{g_i}(1-f)^{2-g_i}=\left\{ \begin{array}{ll}
                          (1-f)^2 \quad &\mbox{if $g_i=0$}\medskip \\
                          2f(1-f) \quad &\mbox{if $g_i=1$}\medskip \\
                          f^2 \quad &\mbox{if $g_i=2$}\medskip \\
                           \end{array}
\right.
\end{equation*}

The full likelihood of $f$ for data observed at $N$ individuals is then
\begin{equation*}
 P(\boldsymbol{r}, \boldsymbol{a}|f,\epsilon)=\displaystyle \prod_{i=1}^N \left[ \sum_{g_i=0}^2P(r_i, a_i|g_i, \epsilon) P(g_i|f)\right],
\end{equation*}

where $\boldsymbol{r}=\{r_1, \ldots, r_N\}$ and $\boldsymbol{a}=\{a_1, \ldots, a_N\}$ are vectors of the number of reference and ancestral reads for all individuals $i=\{1, \ldots, N\}$.

\bigskip
The likelihood of the complete data $\{\boldsymbol{r}, \boldsymbol{a}, \boldsymbol{g}\}$ with $\boldsymbol{g}=\{g_1, \ldots, g_N\}$ for all individuals  $i=\{1, \ldots, N\}$ is then
\begin{equation*}
\ell_c=P(\boldsymbol{r}, \boldsymbol{a}, \boldsymbol{g}|f,\epsilon)=P(\boldsymbol{r}, \boldsymbol{a}|\boldsymbol{g},\epsilon)P(\boldsymbol{g}|f)= \prod_{i=1}^N \Big[P(r_i, a_i|g_i, \epsilon) P(g_i|f)\Big]
\end{equation*}
and
\begin{eqnarray}
 \log\ell_c &=& \log P(\boldsymbol{r}, \boldsymbol{a}|\boldsymbol{g},\epsilon) + \log P(\boldsymbol{g}|f)= \sum_{i=1}^N \Big[ \log P(r_i, a_i|g_i, \epsilon) + \log P(g_i|f)\Big]\nonumber \\
&=& \sum_{i=1}^N \left[ \log \left(\frac{1}{2^{r_i+a_i}} \left[g_i(1-2\epsilon)+2\epsilon\right]^{r_i} \left[g_i(2\epsilon-1)-2\epsilon+2\right]^{a_i}\right)+ \log \left(\binom{2}{g_i}f^{g_i}(1-f)^{2-g_i} \right) \right]\nonumber\\
&=& \sum_{i=1}^N \left[ \log \frac{1}{2^{r_i+a_i}} + r_i \log \left[g_i(1-2\epsilon)+2\epsilon \right] + a_i \log \left[g_i(2\epsilon-1)-2\epsilon+2\right] \right.\nonumber\\
& &+ \left. \log \binom{2}{g_i} + g_i \log f + (2-g_i) \log (1-f) \right]\nonumber\\
&=& \sum_{i=1}^N \left( r_i \log \left[g_i(1-2\epsilon)+2\epsilon\right] + a_i \log \left[g(2\epsilon-1)-2\epsilon+2\right] + g_i \log f + (2-g_i) \log (1-f) \right) + C\label{eq:geno:llc},
\end{eqnarray}
where $C \bot f,\epsilon$.
\subsection{E-step}
Let us define the expected complete data likelihood as
\begin{equation*}
 Q(\theta, \bar{\theta})=\E\left[\log \ell_c|\boldsymbol{r}, \boldsymbol{a}, \bar{\theta} \right],
\end{equation*}

where $\theta=\{f, \epsilon\}$ and where $\bar{\theta}=\{\bar{f}, \bar{\epsilon\}}$ is the current estimate of $\theta$. This expectation is obtained as a weighted average over all possible hidden states $\boldsymbol{g}$, weighted by their probability given $\bar{\theta}$ as
\begin{equation}\label{eq:geno:Q}
  Q(\theta, \bar{\theta}) =\sum_{i=1}^N \sum_{k=0}^2 \left[ \left( \log P(r_i, a_i|g_i=k,\epsilon) + \log P(g_i=k|f) \right) \cdot P(g_i=k|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]
\end{equation}
with $k\in\{0,1,2\}$. Note that the constant term that is independent of $f$ and $\epsilon$ has been dropped again, as it not relevant when maximizing $\ell_c$. Further,
\begin{eqnarray}
 P(g_i=k|r_i, a_i,\bar{f}, \bar{\epsilon}) &=& \displaystyle \frac{P(r_i, a_i | g_i=k, \bar{f}, \bar{\epsilon})P(g_i=k|\bar{f},\bar{\epsilon})}{\displaystyle \sum_{j =0}^2 P(r_i, a_i | g_i=j, \bar{f}, \bar{\epsilon})P(g_i=j|\bar{f},\bar{\epsilon})}
 = \displaystyle \frac{P(r_i, a_i | g_i=k, \bar{\epsilon})P(g_i=k|\bar{f})}{\displaystyle \sum_{j =0}^2 P(r_i, a_i | g_i=j, \bar{\epsilon})P(g_i=j|\bar{f})}\nonumber\\
  &=&  \displaystyle \frac{ \displaystyle \frac{1}{2^{r_i+a_i}} \left[k(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[k(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{k}\bar{f}^k(1-\bar{f})^{2-k}}{\displaystyle \sum_{j =0}^2  \displaystyle \frac{1}{2^{r_i+a_i}} \left[j(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[j(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{j}\bar{f}^j(1-\bar{f})^{2-j} }\nonumber\\
   &=&  \frac{ \displaystyle  \left[k(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[k(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{k}\bar{f}^k(1-\bar{f})^{2-k}}{\displaystyle \sum_{j =0}^2  \displaystyle \left[j(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[j(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{j}\bar{f}^j(1-\bar{f})^{2-j} }\\
   &=& \frac{ \displaystyle  \left[k(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[k(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{k}\bar{f}^k(1-\bar{f})^{2-k}}{2^{r_i+a_i}\big(\bar{\epsilon}^{r_i}(1-\bar{\epsilon})^{a_i}(1-\bar{f})^2 + (1-\bar{\epsilon})^{r_i}\bar{\epsilon}^{a_i}\bar{f}^2 \big) + 2\bar{f}(1-\bar{f})}\label{eq:geno:weight},
\end{eqnarray}
where the last step is obtained by evaluating the sum for $j=0$, $j=1$ and $j=2$.
\subsection{M-step}
The goal of the M-step is to find the values of $f$ and $\epsilon$ that maximize $ Q(\theta, \bar{\theta})$,
\begin{equation*}
\hat{\theta}=\{\hat{f}, \hat{\epsilon}\} = \arg \max_{\theta} Q(\theta, \bar{\theta})
\end{equation*}

$\hat{\theta}=\{\hat{f}, \hat{\epsilon}\}$ will then serve as the new estimates of the parameters for the next iteration. To find the values that maximize $Q(\theta, \bar{\theta})$, we need to find the derivative of $Q(\theta, \bar{\theta})$ with respect to $f$ and $\epsilon$, respectively, and setting them to zero. Let's start with the derivation with respect to $f$. Since $P(g_i=k|r_i, a_i,\bar{f}, \bar{\epsilon})$ is constant with respect to $f$ (but not $\bar{f}$ !), using eq. \ref{eq:logderivrule1}, \ref{eq:logderivrule2} and \ref{eq:geno:llc} we obtain
\begin{eqnarray}
 \frac{\partial Q(\theta, \bar{\theta})}{\partial f} &=&  \sum_{i=1}^N \sum_{g_i=0}^2 \left[ \frac{\partial}{\partial f} \left( \log P(r_i, a_i|g_i,\epsilon) + \log P(g_i|f) \right) \cdot P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]\nonumber \\
 &=& \sum_{i=1}^N \sum_{g_i=0}^2 \left[ \frac{\partial}{\partial f} \left( r_i \log \left[g_i(1-2\epsilon)+2\epsilon\right] + a_i \log \left[g_i(2\epsilon-1)-2\epsilon+2\right] + g_i \log f + (2-g_i) \log (1-f) \right)\right.\nonumber\\
 & & \cdot P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) \bigg]\nonumber\\
 &=& \sum_{i=1}^N \sum_{g_i=0}^2 \left[ \left( \frac{g_i}{f} - \frac{2-g_i}{1-f}\right) \cdot P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]\nonumber\\
 &=& \frac{1}{f} \sum_{i=1}^N \sum_{g_i=0}^2 g_i P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) - \frac{1}{1-f}  \sum_{i=1}^N \sum_{g_i=0}^2 (2-g_i) P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon})=0
\end{eqnarray}
Using the logic presented when deriving eq. \ref{eq:UVlogic} and using \ref{eq:geno:weight}, we then obtain
\begin{eqnarray*}
 \hat{f}=\arg \max_{f} Q(\theta, \bar{\theta}) &=& \displaystyle \frac{\displaystyle \sum_{i=1}^N \sum_{g_i=0}^2 g_i P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon})}{2\displaystyle \sum_{i=1}^N \sum_{g_i=0}^2 P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon})} = \displaystyle \frac{\displaystyle \sum_{i=1}^N \sum_{g_i=0}^2 g_i P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon})}{2N}\\
 &=& \frac{1}{2N}\displaystyle \sum_{i=1}^N \left[ \frac{ \displaystyle \sum_{g_i=0}^2 g_i\left[g_i(1-2\bar{\epsilon})+2\bar{\epsilon}\right]^{r_i} \left[g_i(2\bar{\epsilon}-1)-2\bar{\epsilon}+2\right]^{a_i}\binom{2}{g_i}\bar{f}^{g_i}(1-\bar{f})^{2-g_i}}{2^{r_i+a_i}\big(\bar{\epsilon}^{r_i}(1-\bar{\epsilon})^{a_i}(1-\bar{f})^2 + (1-\bar{\epsilon})^{r_i}\bar{\epsilon}^{a_i}\bar{f}^2 \big) + 2\bar{f}(1-\bar{f}) }\right]
\end{eqnarray*}
Let us next obtain the derivation of $Q(\theta, \bar{\theta})$ with respect to $\epsilon$. Since both $P(g_i|f)$ and $P(g_i=k|r_i, a_i,\bar{f}, \bar{\epsilon})$ is constant with respect to $\epsilon$, using eq. \ref{eq:logderivrule1}, \ref{eq:logderivrule2} and \ref{eq:geno:llc} and the chain rule of derivation we obtain
\begin{eqnarray*}
 \frac{\partial Q(\theta, \bar{\theta})}{\partial \epsilon} &=& \sum_{i=1}^N \sum_{g_i=0}^2 \left[ \frac{\partial}{\partial \epsilon} \left( r_i \log \left[g_i(1-2\epsilon)+2\epsilon\right] + a_i \log \left[g_i(2\epsilon-1)-2\epsilon+2\right] \right) \cdot P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]\\
  &=& \sum_{i=1}^N \sum_{g_i=0}^2 \left[ \left( \frac{2r_i(1-g_i)}{g_i(1-2\epsilon)+2\epsilon}
 + \frac{2a_i(g_i-1)}{g_i(2\epsilon-1)-2\epsilon+2}\right)
 \cdot P(g_i|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]\\
\end{eqnarray*}
Note that all terms will be zero when $g_i=1$, and hence we can rewrite the sum as a term with $g_i=0$ and one with $g_i=2$
\begin{eqnarray*}
 \frac{\partial Q(\theta, \bar{\theta})}{\partial \epsilon} &=& \sum_{i=1}^N \left[ \left( \frac{2r_i}{2\epsilon}
 - \frac{2a_i}{2-2\epsilon}\right)
 \cdot P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon}) + \left( \frac{2a_i}{2\epsilon}-\frac{2r_i}{2-2\epsilon} \right)
 \cdot P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]\\
 &=& \sum_{i=1}^N \left[ \left( \frac{r_i}{\epsilon}
 - \frac{a_i}{1-\epsilon}\right)
 \cdot P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon}) + \left( \frac{a_i}{\epsilon}-\frac{r_i}{1-\epsilon} \right)
 \cdot P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon}) \right]=0,
\end{eqnarray*}
which can be rewritten as
\begin{equation*}\label{eq:geno:hate:almost}
\frac{1}{\epsilon}\displaystyle \sum_{i=1}^N \Big[r_i P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon}) + a_i P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon})\Big] - \frac{1}{1-\epsilon} \displaystyle \sum_{i=1}^N \Big[ r_i P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon}) + a_i P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon})\Big] = 0
\end{equation*}

Again using the logic presented in eq. \ref{eq:UVlogic}, we obtain
\begin{equation*}
 \hat{\epsilon} =\arg \max_{\epsilon} Q(\theta, \bar{\theta}) = \displaystyle \frac{ \displaystyle \sum_{i=1}^N \Big[r_i P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon}) + a_i P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon})\Big]}{
 \displaystyle \sum_{i=1}^N \Big[(r_i+a_i) \Big(P(g_i=0|r_i, a_i,\bar{f}, \bar{\epsilon}) + P(g_i=2|r_i, a_i,\bar{f}, \bar{\epsilon})\Big)\Big]}
\end{equation*}


\end{document}
