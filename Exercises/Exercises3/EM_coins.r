#set variables
n <- 20; #number of experiments
m <- 10; #number of throws per experiment
theta <- c(0.2, 0.8);

epsilon <- 10^-8; #condition to stop optmization
rep <- 5; #number of replicates from random starting points
randomStart <- FALSE; #choose starting points at random

#-----------------------------------------
#Function to simulate data
#-----------------------------------------
simCoinExperiment<-function(m, theta, n){
  #first simulate which coins were used
  cc <- rbinom(n, 1, 0.5) + 1;
  #simulate number of heads per experiment
  hh <- rbinom(n, m, theta[cc]);
  return(data.frame("coin"=cc, "heads"=hh));
}


#-----------------------------------------
#use grid to calculate likelihood surface
#-----------------------------------------

LL_fast <- function(x, m, theta){
  return(sum(log(0.5*(theta[1]^x*(1-theta[1])^(m-x) + theta[2]^x*(1-theta[2])^(m-x)))));
}

calcLLSurface <- function(x=1, m, nPoints=100){
  thetaA <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
  thetaB <- seq(0, 1, length.out=nPoints+2)[2:(nPoints+1)];
  
  LL <- matrix(0, nrow=nPoints, ncol=nPoints);
  for(i in 1:nPoints){
    for(j in 1:nPoints){
      LL[i,j] <- LL_fast(x, m, c(thetaA[i], thetaB[j]));
    }
  }
  
  return(list(thetaA=thetaA, thetaB=thetaB, LL=LL));
}

#-----------------------------------------
#Function to run EM
#-----------------------------------------
calc_P_z_given_x_theta <- function(x, m, theta){
  #x is a vector
  #return matrix
  res <- matrix(0, ncol=2, nrow=length(x));
  
  #now calc for each state and each observed x
  res[,1] <- theta[1]^x*(1-theta[1])^(m-x);
  res[,2] <- theta[2]^x*(1-theta[2])^(m-x);
  return(res / rowSums(res));
}

runEM<-function(x, m, start, epslion=10^-8, maxIteration=100){
  #prepare storage
  theta <- matrix(0, ncol=2, nrow=maxIteration);
  theta[1,] <- start;
  
  #run EM
  for(i in 2:maxIteration){
    #estimate new theta
    P_z_given_x_oldTheta <- calc_P_z_given_x_theta(x, m, theta[i-1,]);
    theta[i,1] <- sum(x * P_z_given_x_oldTheta[,1]) / (m * sum(P_z_given_x_oldTheta[,1]));
    theta[i,2] <- sum(x * P_z_given_x_oldTheta[,2]) / (m * sum(P_z_given_x_oldTheta[,2]));
    
    #decide if we break
    if(sum( abs(theta[i,] - theta[i-1,]) < epsilon ) == 2){
      print(paste("Required", i, "iterations"));
      return(theta[1:i,])
    }
  }
  return(theta);
}

#-----------------------------------------
#Function to make nice plots
#-----------------------------------------
initPlot<-function(x, m, theta){
  plot(0, type='n', xlab="Theta A", ylab="Theta B", xlim=c(0,1), ylim=c(0,1));
  
  #plot LL surface
  surface <- calcLLSurface(x, m, 200);
  contour(surface$thetaA, surface$thetaB, surface$LL, zlim=c(quantile(surface$LL, probs=c(0.00, 1.0))), nlevels=100, add=TRUE, col='black', drawlabels=FALSE, lty=1, lwd=0.5);
  
  #points(theta[1], theta[2], pch=19, col='red')
  points(theta[1], theta[2], pch=13, col='red', cex=5)  
}

plotEM <- function(EM, col='black'){
  l <- length(EM[,1]);
  points(EM[1,1], EM[1,2], pch=19, col=col)
  points(EM[l,1], EM[l,2], pch=19, col=col)
  lines(EM[1:l,1], EM[1:l,2], col=col);
}


#-----------------------------------------
#Simulate data, run EM and plot
#-----------------------------------------
#simulate data
data<-simCoinExperiment(m, theta, n);

#init plot
initPlot(data$heads, m, theta);

#run EM and plot
if(randomStart){
  for(i in 1:rep){
    res<-runEM(data$heads, m, runif(2), epsilon);
    plotEM(res);
  }
} else {
  nstep<-round(sqrt(rep));
  start <- matrix(0, ncol=2, nrow=(nstep)^2);
  where <- seq(0.19, 0.21, length.out=nstep);
  start[,1]<-rep(where, nstep)
  start[,2]<-rep(where, each=nstep)
  for(i in 1:(nstep*nstep)){  
    res<-runEM(data$heads, m, start[i,], epsilon);
    plotEM(res);
  }
}





