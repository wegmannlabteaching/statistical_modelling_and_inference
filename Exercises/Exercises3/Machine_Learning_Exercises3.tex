\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

%\usepackage[withSolutions]{optional}
\usepackage[withoutSolutions]{optional}

\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.2cm}\\ {\color{red} \texttt{#1}}}\bigskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}


%opening
\title{Machine Learning - Exercises 3}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{EM Algorithm}
Suppose we have two biased coins $A$ and $B$ that produce a \textit{head} with unknown probabilities $\theta_A$ and $\theta_B$, respectively. In an experiment, a coin $z$ is chosen at random ($\P(z=A)=\frac{1}{2}$) and always thrown $m$ times and the number \textit{heads} was recorded. Suppose we obtain the results (number of \textit{heads}) of $n$ such experiments as $\boldsymbol{x}=\{x_1, \ldots, x_n\}$.

If it was known for each experiment which coin was used, the likelihood would be easy to compute. Let us denote by $z_i=\{A,B\}$ the coin used in experiment $i=\{1, \ldots, n\}$. The likelihood of $\theta=\{\theta_A, \theta_B\}$ is then given by
\begin{equation*}
 \P(\boldsymbol{x}|\boldsymbol{z}, \theta) =  \displaystyle \prod_{i=1}^n \P(x_i|z_i, \theta) =  \displaystyle \prod_{i=1}^n \binom{m}{x_i} \theta_{z_i}^{x_i}(1-\theta_{z_i})^{m-x_i},
\end{equation*}
where $\boldsymbol{z}=\{z_1, \ldots, z_L\}$ is the vector of coins used.

In case $\boldsymbol{z}$ is unknown, the likelihood of $\theta$ is given by integrating over the unknown hidden state
\begin{equation*}
  \P(\boldsymbol{x}|\theta) = \displaystyle \prod_{i=1}^n \left[ \sum_{k \in \{A,B\}} \P(x_i|z_i=k, \theta) \P(z_i=k) \right] = \displaystyle \prod_{i=1}^n \left[ \frac{1}{2} \sum_{k \in \{A,B\}}  \theta_{k}^{x_i}(1-\theta_{k})^{m-x_i}\right]
\end{equation*}

In such cases, the EM algorithm can be used to find the maximum likelihood estimate $\hat{\theta}$. 

\begin{enumerate}[1.]
\item The first step in developing an EM algorithm is to calculate the complete data log likelihood $\ell_c$ for this model. Remember that the complete data likelihood treats the latent variable (the coin which was uses) as data.

\begin{enumerate}[a)]
\item Begin by first writing out the complete data likelihood ${\cal L}_c$ for this model.\sol{\begin{equation*}
 {\cal L}_c = P(\boldsymbol{x}, \boldsymbol{z}|\theta)=\P(\boldsymbol{x}| \boldsymbol{z},\theta)P(\boldsymbol{z}|\theta)=\displaystyle \prod_{i=1}^n \P(x_i| z_i,\theta)\P(z_i)
\end{equation*}}

\item What is the complete data log likelihood $\ell_c = \log {\cal L}_c$ of this model?\sol{\begin{eqnarray*} \log{\cal L}_c = \ell_c &=&\log \P(\boldsymbol{x}| \boldsymbol{z},\theta) + \log \P(\boldsymbol{z}|\theta)\\
 &=& \displaystyle \sum_{i=1}^n \left( \log P(x_i| z_i,\theta) + \log \P(z_i) \right)\\
 &=& \displaystyle \sum_{i=1}^n  \left(\log \binom{m}{x_i} + x_i\log \theta_{z_i} + (m-x_i) \log (1-\theta_{z_i}) + \log \P(z_i)\right)\\
 &=& \displaystyle \sum_{i=1}^n  \left(x_i\log \theta_{z_i} + (m-x_i) \log (1-\theta_{z_i}) \right) + C, \quad C \bot \theta\end{eqnarray*}}

\end{enumerate}
\item The next step, the so called ``Expectation Step'' or ``E-Step'' consists of calculating the expected complete data log likelihood $\ell_c$ given the data $\boldsymbol{x}$ and the current estimates of the parameters $\bar{\theta}$:
\begin{equation*}
 Q(\theta, \bar{\theta})=\E\left[\ell_c|\boldsymbol{x}, \bar{\theta} \right],
\end{equation*}

\begin{enumerate}[a)]

\item Write out $Q(\theta, \bar{\theta})=\E\left[\log \ell_c|\boldsymbol{x}, \bar{\theta} \right]$. This expectation is obtained as a weighted average over all possible hidden states $\boldsymbol{z}$, weighted by their probability given $\bar{\theta}$ and the data $\boldsymbol{x}$. You may use the term $\P(z_i=k|x_i, \bar{\theta})$ to indicate the appropriate weight.\sol{\begin{equation}Q(\theta, \bar{\theta}) =
\displaystyle \sum_{i=1}^n  \sum_{k\in\{A,B\}} \left( \left[x_i\log \theta_{k} + (m-x_i) \log (1-\theta_{k})\right] \cdot \P(z_i=k|x_i, \bar{\theta}) \right) + C\end{equation}
Note that $C$ indicates constant terms independent of $\theta$ that will not be relevant when maximizing $\ell_c$.}

\item An important step is to express the weights $\P(z_i=k|x_i, \bar{\theta})$ in terms of the defined probabilities $\P(x|z, \theta)$ and $\P(z)$. This is achieved using Bayes formula. \sol{\begin{eqnarray*}
 P(z_i=k|x_i, \bar{\theta}) &=& \displaystyle \frac{P(x_i|z_i=k, \bar{\theta})P(z_i=k)}{P(x_i|\bar{\theta})}\nonumber = \displaystyle \frac{P(x_i|z_i=k, \bar{\theta})P(z_i=k)}{\displaystyle\sum_{j\in\{A,B\}} P(x_i|z_i=j, \bar{\theta})P(z_i=j)}\\
&=&  \displaystyle \frac{\displaystyle \frac{1}{2} \binom{m}{x_i} \bar{\theta}_k^{x_i}(1-\bar{\theta}_k)^{m-x_i} }{\displaystyle \frac{1}{2} \binom{m}{x_i} \sum_{j\in\{A,B\}} \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}  } = \frac{\bar{\theta}_k^{x_i}(1-\bar{\theta}_k)^{m-x_i} }{\displaystyle \sum_{j\in\{A,B\}}  \bar{\theta}_j^{x_i}(1-\bar{\theta}_j)^{m-x_i}} \label{eq:weight}
\end{eqnarray*}}

\end{enumerate}
\item The final step, the so called ``Maximization Step'' or ``M-Step'' consists of maximizing $Q(\theta, \bar{\theta})$ in respect to the parameters:

\begin{equation*}
\hat{\theta}=\{\hat{\theta}_A, \hat{\theta}_B\} = \arg \max_{\theta} Q(\theta, \bar{\theta})
\end{equation*}

$\hat{\theta}$ will then serve as the new estimates of $\theta$ for the next iteration. To find the values that maximize $Q(\theta, \bar{\theta})$, we need to find the derivative of $Q(\theta, \bar{\theta})$ with respect to $\theta_A$ and $\theta_B$, respectively, and setting them to zero.

\begin{enumerate}[a)]

\item  Let's start with the derivation with respect to $\theta_A$. Note that the weights $\P(z_i=k|x_i, \bar{\theta})$ do not depend on the parameters $\theta$ (but the old parameters $\bar{\theta}$) and are hence constants. There is thus no need to write them out during the derivation. Also, remember that 
\begin{equation} \displaystyle \frac{d}{dx}a\log x= \frac{a}{x} \quad \mbox{and} \quad \displaystyle \frac{d}{dx}a\log (b-x)= -\frac{a}{b-x}.
\end{equation}
\sol{Note that all terms that do not contain $\theta_A$ drop out. \begin{eqnarray*}
 \displaystyle \frac{\partial Q(\theta, \bar{\theta})}{\partial \theta_A} &=&  \displaystyle \sum_{i=1}^n \left[ \left( \frac{x_i}{\theta_A} - \frac{m-x_i}{1-\theta_A} \right) \cdot \P(z_i=A|x_i, \bar{\theta}_A)\right]\\
  &=& \frac{1}{\theta_A} \displaystyle \sum_{i=1}^n x_i \P(z_i=A|x_i, \bar{\theta}_A)  
  - \frac{1}{1-\theta_A} \displaystyle \sum_{i=1}^n (m-x_i)\P(z_i=A|x_i, \bar{\theta}_A).
\end{eqnarray*}}

\item Now also calculate the derivation with respect to $\theta_B$. \sol{This is completely analogous: \begin{eqnarray*}
 \displaystyle \frac{\partial Q(\theta, \bar{\theta})}{\partial \theta_B} =
  \frac{1}{\theta_B} \displaystyle \sum_{i=1}^n x_i \P(z_i=B|x_i, \bar{\theta}_B)  
  - \frac{1}{1-\theta_B} \displaystyle \sum_{i=1}^n (m-x_i)\P(z_i=B|x_i, \bar{\theta}_B).
\end{eqnarray*}}

\item Now set these derivatives to zero and solve the resulting system of linear equations for $\theta_A$ and $\theta_B$.\sol{Note that the two equations each only contain either $\theta_A$ or $\theta_B$, and can thus be solved\\ independently. Also, note that both equations are of the form \begin{eqnarray*}
 \frac{1}{x}U - \frac{1}{1-x}V&=&0 \quad \mbox{and hence} \quad x = \frac{U}{U + V}
\end{eqnarray*}
We thus get:
\begin{eqnarray*}\hat{\theta}_A = \arg \max_{\theta_A} Q(\theta, \bar{\theta}) &=& \frac{\displaystyle \sum_{i=1}^n x_i \P(z_i=A|x_i, \bar{\theta}_A)}  {\displaystyle \sum_{i=1}^n x_i \P(z_i=A|x_i, \bar{\theta}_A) + \displaystyle \sum_{i=1}^n (m-x_i) \P(z_i=A|x_i, \bar{\theta}_A)} = \frac{\displaystyle \sum_{i=1}^n x_i \P(z_i=A|x_i, \bar{\theta}_A)}{\displaystyle m\sum_{i=1}^n \P(z_i=A|x_i, \bar{\theta}_A)}\end{eqnarray*}
And analogously
\begin{eqnarray*}\hat{\theta}_B = \arg \max_{\theta_B} Q(\theta, \bar{\theta}) = \frac{\displaystyle \sum_{i=1}^n x_i \P(z_i=B|x_i, \bar{\theta}_B)}{\displaystyle m\sum_{i=1}^n \P(z_i=B|x_i, \bar{\theta}_B)}\end{eqnarray*}
where $\P(z_i=k|x_i, \bar{\theta}_k)$ is evaluated as you found above.}

\end{enumerate}

\item With all equations at hand, let's implement this EM algorithm in \texttt{R} and test it with simulations. 

\begin{enumerate}[a)]
\item Implement a function to simulate data under the model. The function should take as input the number of experiments $n$, the number of draws per coin $m$ as well as vector $\theta=\{\theta_A, \theta_B\}$, and return a data frame where the first column lists the coin used (use 1 and 2 instead of A and B) and the the number of heads that were observed.

\sol{\script{simCoinExperiment <- function(m, theta, n)\{\\
  \hspace*{1cm} \#first simulate which coins were used\\
  \hspace*{1cm} cc <- rbinom(n, 1, 0.5) + 1;\\
  \hspace*{1cm} \#simulate number of heads per experiment\\
  \hspace*{1cm} hh <- rbinom(n, m, theta[cc]);\\
  \hspace*{1cm} return(data.frame("coin"=cc, "heads"=hh)); \}}}


\item Next, implement a function to calculate the weights $\P(z_i=k|x_i, \bar{\theta}_k)$. This function should get the vector $\boldsymbol{x}$ with all the data, the number of draws per coin $m$ as well as vector $\theta=\{\theta_A, \theta_B\}$, and return a matrix with one row per experiment ($n$ rows) and one column per coin (two columns).
\sol{\script{calc\_P\_z\_given\_x\_theta <- function(x, m, theta)\{\\
\hspace*{1cm} \#x is a vector, return matrix\\
  \hspace*{1cm} res <- matrix(0, ncol=2, nrow=length(x));\\  
  \hspace*{1cm} \#now calc for each state and each observed x\\
  \hspace*{1cm} res[,1] <- theta[1]\^x*(1-theta[1])\^(m-x);\\
  \hspace*{1cm} res[,2] <- theta[2]\^x*(1-theta[2])\^(m-x);\\
  \hspace*{1cm} return(res / rowSums(res));\\
\} }}

\item Finally, write a function to run the EM until convergence. This function should get as input the vector $\boldsymbol{x}$, the number of draws $m$, a vector with initial values for $\theta=\{\theta_A, \theta_B\}$ and an accuracy threshold $\epsilon$ such that the EM is stopped if the values do not change by more than it in one iteration.
\sol{\script{runEM <- function(x, m, start, epsilon=10\^-8, maxIterations=100)\{
  \hspace*{1cm} \#prepare storage\\
  \hspace*{1cm} theta <- matrix(0, ncol=2, nrow=maxIterations);\\
  \hspace*{1cm} theta[1,] <- start;\\
  \hspace*{1cm} \#run EM\\
  \hspace*{1cm} for(i in 2:maxIteration)\{\\
    \hspace*{2cm} \#estimate new theta\\
    \hspace*{2cm} P\_z\_given\_x\_oldTheta <- calc\_P\_z\_given\_x\_theta(x, m, theta[i-1,]);\\
    \hspace*{2cm} theta[i,1] <- sum(x * P\_z\_given\_x\_oldTheta[,1]) / (m * sum(P\_z\_given\_x\_oldTheta[,1]));\\
    \hspace*{2cm} theta[i,2] <- sum(x * P\_z\_given\_x\_oldTheta[,2]) / (m * sum(P\_z\_given\_x\_oldTheta[,2]));\\
    \hspace*{2cm} \#decide if we break\\
    \hspace*{2cm} if(sum( abs(theta[i,] - theta[i-1,]) < epsilon )==2)\{\\
      \hspace*{3cm} return(theta[1:i,]);\\
    \hspace*{2cm} \}\\
  \hspace*{1cm} \}\\
  \hspace*{1cm} return(theta);\\
\} }}

\item How fast does the E converge with $\epsilon = 10^{-8}$?\sol{If done right, the EM should converge within about 25 iterations,}

\end{enumerate}

\end{enumerate}

\end{document}
