library(rjson);
library(readr);
library(stringr);
library(lubridate);
library(ISOweek);

########################################
#compile course content as HTML for moodle with links
########################################

#dates: Monday, Monday, Wednesday except some weeks
weekDaysDefault <- c(0,0,2);
weekDays <- list(weekDaysDefault, c(0,0), weekDaysDefault, weekDaysDefault, weekDaysDefault, weekDaysDefault, c(), weekDaysDefault);
for(i in 1:length(weekDays)){
  weekDays[[i]] <- weekDays[[i]] + (i-1) * 7;
}
dates <- c(as.Date("2025-02-17") + unlist(weekDays));
           
         
#other settings
bitbucketLink <- "https://bitbucket.org/wegmannlabteaching/statistical_modelling_and_inference/raw/master/Exercises/";


#####################################
#read data
#####################################
structure <- fromJSON(file="structure.json")$halfDayBlocks;
numBlocks <- length(structure);

if(numBlocks != length(dates)){
 stop("Number of blocks does not match number of provided dates!");
}

#####################################
# Functions to compile HTM
#####################################

#prepare htm
htm <- "";
solutions <- numeric(0);

#function to parse an exercise link
getExerciseLink <- function(exerciseNumber, solution=FALSE){
  file <- list.files("../", pattern=paste0("ML_Exercises_", exerciseNumber, "_"));
  if(solution){
    return( paste0("<a href=\"", bitbucketLink, file, "/", file, "_solutions.pdf\"  target=\"_blank\">", file, "_solutions.pdf</a>") );
  } else {
   return(paste0("<a href=\"", bitbucketLink, file, "/", file, ".pdf\"  target=\"_blank\">", file, ".pdf</a>") );
  }
};

getExerciseLinks <- function(exerciseNumbers, solution=FALSE){
  res <- character(length(exerciseNumbers));
  for(i in seq_along(exerciseNumbers)){
    res[i] <- getExerciseLink(exerciseNumbers[i], solution);
  }
  return(res);
};

makeList <- function(entries){
  return(paste0("<ul><li>", paste0(entries, collapse="</li><li>"), "</li></ul>"));
};

#####################################
# compile HTML
#####################################

#prepare htm
htm <- "";
solutions <- numeric(0);
#loop over blocks
for(i in 1:numBlocks){
  # write block header
  htm <- paste0(htm, "<h4>", i, ") ", format(dates[i], "%A %d.%m.%Y"), ": ", structure[[i]]$Title, "</h4>\n<p>\n");
  
  #topics
  if(length(structure[[i]]$Topics) > 0){
    htm <- paste0(htm, makeList(structure[[i]]$Topics));
  }
  
  #chapters
  if(length(structure[[i]]$Chapters) > 0){
    htm <- paste0(htm, "<b>Chapters:</b> ", structure[[i]]$Chapters, "<br/>\n");
  }
  
  #Exercises
  if(length(structure[[i]]$Exercises) > 0){
    htm <- paste0(htm, "<b>Exercises:</b>");
    if(length(structure[[i]]$Exercises) > 1){
      htm <- paste0(htm, makeList(getExerciseLinks(structure[[i]]$Exercises, FALSE)));
    } else {
      htm <- paste0(htm, " ", getExerciseLink(structure[[i]]$Exercises[1], FALSE), "<br/>\n");
    }
    solutions <- c(solutions, structure[[i]]$Exercises);
  }
  
  #videos
  if(length(structure[[i]]$Videos) > 0){
    htm <- paste0(htm, "<b>Videos: </b>", paste0(paste0("<a href=\"", structure[[i]]$Videos, "\"  target=\"_blank\">Video</a>"), collapse=", "));
  } 
  
  
  #done
  htm <- paste0(htm, "</p>\n\n");
}

#Add solutions to exercises
solutions <- sort(solutions);
htm <- paste0(htm, "<h4>Solution to Exercises</h4>", makeList(getExerciseLinks(solutions, TRUE)));

#write to file
write_file(htm, "structure.htm");

