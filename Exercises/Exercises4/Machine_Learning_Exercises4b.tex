\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color, xcolor}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}
\usepackage{fontawesome}
\usepackage[hidelinks]{hyperref}


\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

\usepackage[withSolutions]{optional}
%\usepackage[withoutSolutions]{optional}

\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.2cm}\\ {\color{red} \texttt{#1}}}\bigskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

%--------------------------------
% Video / code link in margin
%--------------------------------
\definecolor{linkcolor}{HTML}{9a173b}

\newcommand{\codesymbol}{{\textcolor{linkcolor}{\LARGE \faFileCodeO}}}
\newcommand{\baseUrlBitbucket}{https://bitbucket.org/wegmannlab/statistical_modelling_and_inference/raw/master/}

\newcommand{\solutions}[1]{\marginpar{\href{\baseUrlBitbucket Exercises/Exercises4/#1}{\codesymbol}}}

\newcommand{\externallink}[2]{\textcolor{linkcolor}{\href{#1}{#2}}}

\everymath{\displaystyle}

%----------------------------------------------------------
%Mathematical Operators and Symbols
%----------------------------------------------------------
\def\P{\mathbb{P}}
\def\L{{\cal L}}
\def\N{{\cal N}}
\def\l{{\ell}}


\def\mmu{\boldsymbol{\mu}}
\def\x{\boldsymbol{x}}
\def\f{\boldsymbol{f}}
\def\g{\boldsymbol{g}}

%opening
\title{Machine Learning - Exercises 4}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{Markov Chain Monte Carlo}



\begin{enumerate}[1.]

\item In this exercise, we will illustrate that MLE estimates are potentially biased and that these biases may be be overcome is a Bayesian setting. Consider $M$ measurements $\x_i=(x_{i1}, \ldots, x_{im})$ for each of $n$ experiments $i=1, \ldots, n$. These measurements shall be normally distributed with means $\mu_i$ specific to each experiment, but a common variance $\sigma^2$, such that

\begin{equation*}
 x_{ij} \sim \N(\mu_i, \sigma^2), j=1,\ldots,m.
\end{equation*}

The goal is now to infer the variance $\sigma^2$ from the full data.

\begin{enumerate}[a)]
\item Write out the full likelihood $\L(\mmu, \sigma^2)=\P(\x|\mmu, \sigma^2)$ where $\x=(\x_1, \ldots, \x_n)$ and $\mmu=(\mu_1, \ldots, \mu_n)$.\sol{\begin{equation*}
    \L(\mmu, \sigma^2)=\P(\x|\mmu, \sigma^2) = \prod_{i=1}^n \prod_{j=1}^m \P(x_{ij}|\mu_i, \sigma^2) = \left(\frac{1}{\sqrt{2 \pi \sigma^2}}\right)^{nm} \exp{\left(-\frac{1}{2\sigma^2} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2\right)}.
\end{equation*}
}

\item\label{item:normal:implement} Write a function to simulate data under this model in R. Simulate data for $n=100$ experiments and $m=2$ values each. Simulate the $\mu_i \sim \N(0, 1)$ and set $\sigma^2=1$.
\sol{\script{simData <- function(n, m)\{\\
  \hspace*{1cm} \#simulate mu\\
  \hspace*{1cm} mu <- rnorm(n);\\
  \hspace*{1cm} \#simulate x\_ij\\
  \hspace*{1cm} x <- matrix(NA, nrow = n, ncol = m);\\
  \hspace*{1cm} for (i in 1:n)\\
  \hspace*{2cm} x[i,] <- rnorm(m, mu[i]);\\
  \hspace*{1cm}return(data.frame(x=x, mu=mu));\\
\};}}

\item Derive the maximum likelihood estimators $\hat{\sigma}^2$ and $\hat{\mu}_i$.\sol{Let's start with the log-likelihood:
\begin{equation*}
    \l(\mmu, \sigma^2) = -\frac{nm}{2}\log \left(2 \pi \right) - nm\log \sigma - \frac{1}{2\sigma^2}\sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}
The derivative with respect to $\mu_i$ is:
\begin{equation*}
    \frac{\partial}{\partial \mu_i} \l(\mmu, \sigma^2) = \frac{1}{\sigma^2} \sum_{j=1}^m (x_{ij} - \mu_i).
\end{equation*}
Set it to zero and solve for $\mu_i$. This results in:
\begin{equation*}
    \hat{\mu_i} = \frac{1}{m} \sum_{j=1}^m x_{ij}.
\end{equation*}
The derivative with respect to $\sigma^2$ is:
\begin{equation*}
    \frac{\partial}{\partial \sigma^2} \l(\mmu, \sigma^2) = -\frac{nm}{\sigma} + \sigma^{-3} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}
Set it to zero and solve for $\sigma^2$. This results in:
\begin{equation*}
    \hat{\sigma}^2 = \frac{1}{nm} \sum_{i=1}^n \sum_{j=1}^m (x_{ij} - \mu_i)^2.
\end{equation*}}
\item Implement a function in R to generate these estimates and estimate the values from the data simulated under \ref{item:normal:implement}. Repeat the process of simulating and estimating to convince yourself that the estimator $\hat{\sigma}^2$ is biased (underestimation).
\sol{\script{MLE\_mu <- function(x)\{\\
  \hspace*{1cm}m <- ncol(x);\\
  \hspace*{1cm}return(1/m * rowSums(x));\\
\}\\
MLE\_sigma2 <- function(x, mu)\{\\
  \hspace*{1cm}m <- ncol(x);\\
  \hspace*{1cm}n <- nrow(x);\\
  \hspace*{1cm}return((1/(n*m)) * sum(rowSums((x - mu)\textasciicircum 2)));\\
\};}}

\item Let us next develop a Bayesian inference scheme assuming uniform priors $\mu_i \propto 1$ and $\sigma^2 \propto 1$. To generate estimates, we will resort to an MCMC using the Metropolis-Hastings algorithm. For this, propose symmetric proposal kernels and derive the necessary Hastings ratios to update each $\mu_l$ and $\sigma^2$ individually.
\sol{Proposal kernels:
\begin{equation*}
    q(\mu_i,\mu'_i) \sim {\cal N}(\mu_i, \sigma_{\mu_i}^2), \quad q(\sigma^2,\sigma'^2) \sim {\cal N}(\sigma^2, \sigma_{\sigma^2}^2),
\end{equation*}
where $q(\sigma^2,\sigma'^2)$ must be mirrored at 0 to respect the interval $(0,\infty)$.
\begin{equation*}
   h_{\mu_i} = \min \left(1, \prod_{j=1}^m \frac{\P(x_{ij}|\mu_i, \sigma^2)}{\P(x_{ij}|\mu_i, \sigma^2)} \right), \quad 
   h_{\sigma^2} = \min \left(1, \prod_{i=1}^n \prod_{j=1}^m \frac{\P(x_{ij}|\mu_i, \sigma^2)}{\P(x_{ij}|\mu_i, \sigma^2)} \right).
\end{equation*}}

\item Implement the MCMC scheme and use it to infer the posterior $\P(\sigma^2|\x)$ on the data simulate under \ref{item:normal:implement}. Make sure the chain is converging by monitoring the trace. Is the posterior also biased?
\sol{See link \solutions{exercise_4_b_ex1_ML.R} for an example on how to implement such an MCMC. \\
It is clearly visible that the posterior is not biased, but estimates $\sigma_2$ very well even if $M$ is small. This is because the Bayesian inference integrates over the uncertainty in $\mmu$. The MLE of $\mu_i$ are generally estimated too close to the data, especially if $M$ is small. \\ Therefore, the variance $\sigma^2$ is estimated too small. This issue cannot be overcome by increasing the number of experiments $n$. Only increasing the number of measurements $M$ results in more accurate estimates of $\mu_i$ and hence also $\sigma^2$.}
\end{enumerate}

\item\label{item:eAndF} Consider a sample of genotypes $g_{i}=0,1,2$ estimated at one locus for $n$ individuals $i=1, \ldots, n$ sampled from a randomly mating population in Hardy-Weinberg equilibrium. The observed genotypes $g_{i}$ may differ from the true underlying genotypes $\gamma_{i}=0,1,2$ due to some genotyping errors that occur at rate $\epsilon$ such that 

\begin{equation*}
 \P(g |\gamma, \epsilon) = \begin{cases}
                            1-\epsilon & \quad \mathrm{if } g=\gamma\\
                            \frac{\epsilon}{2} & \quad \mathrm{if } g\neq\gamma
                           \end{cases}.
\end{equation*}

Since the population is in Hardy-Weinberg equilibrium, we have 
\begin{equation*}
 \P(\gamma_{i}|f) = \binom{2}{\gamma_{i}} f^{\gamma_{i}} (1-f)^{2-\gamma_{i}} = \begin{cases}
                                                   (1-f)^2 & \quad \mathrm{if } \gamma_{i}=0\\
                                                   2f(1-f) & \quad \mathrm{if } \gamma_{i}=1\\
                                                   f^2 & \quad \mathrm{if } \gamma_{i}=2\\
                                                  \end{cases},
\end{equation*}

where $f$ is the unknown allele frequency.

The goal is now to infer the error rate $\epsilon$ and the allele frequency $f$ in a Bayesian setting by means of a Metropolis-Hastings algorithm. Let us assume an exponential prior distribution on $\epsilon$
\begin{equation*}
 \P(\epsilon) \sim \lambda_{\epsilon} e^{-\epsilon\lambda_{\epsilon}},
\end{equation*}
and a Beta distribution on $f$:
\begin{equation*}
 f \sim \mathrm{Beta}(a,a).
\end{equation*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\begin{enumerate}[a)]
\item Write out the full likelihood $\L(\epsilon, f)=\P(\g | \epsilon, f)$ of this model by integrating out the true genotypes $\gamma_{il}$. Here, $\g$ denotes the full set of observed genotypes.\sol{\begin{equation*}
\L(\epsilon, f)=\P(\g | \epsilon, f)= \prod_{i=1}^n \sum_{\gamma = 0}^2 \P(g_{i}|\gamma_{i},\epsilon) \P(\gamma_{i}|f).
\end{equation*}}
\item Propose symmetric proposal kernels for all single-parameter updates, i.e. for updating $\epsilon$ and $f$ individually.
\sol{\begin{equation*}
    q(f,f') \sim {\cal N}(f, \sigma_f^2), \quad q(\epsilon,\epsilon') \sim {\cal N}(\epsilon, \sigma_\epsilon^2),
\end{equation*}
mirrored at 0 and 1 to respect the interval $[0,1]$.}
\item Write out the simplified Hastings-ratios for single-parameter updates, assuming the symmetric proposal kernels proposed above.
\sol{\begin{equation*}
h_\epsilon = \min \left( 1, \frac{\P(\epsilon')}{\P(\epsilon)} \prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{i}|\gamma_{i}, \epsilon')\P(\gamma_{i}|f)}{\sum_{\gamma = 0}^2 \P(g_{i}|\gamma_{i}, \epsilon) \P(\gamma_{i}|f)} \right), \quad
h_{f} = \min \left(1, \frac{\P(f')}{\P(f)}\prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{i}|\gamma_{i}, \epsilon)\P(\gamma_{i}|f')}{\sum_{\gamma = 0}^2 \P(g_{i}|\gamma_{i}, \epsilon) \P(\gamma_{i}|f)} \right).
\end{equation*}
Because of the sum over $\gamma$, we cannot simplify the likelihood any further.}
\item\label{item:simulate} Implement a function in R to simulate data under this model. Then simulate data with $\epsilon=0.1$ and $a=0.5$ for $n=20$ individuals.
\sol{\script{simData <- function(e, a, n)\{\\
  \hspace*{1cm} \# simulate allele frequency\\
  \hspace*{1cm} f <- rbeta(1, a, a);\\
  \hspace*{1cm} \# simulate true genotypes\\
  \hspace*{1cm} gamma <- rbinom(n, 2, f);\\
  \hspace*{1cm} \# store error as matrix: probability to change from one genotype to another one\\
  \hspace*{1cm} errors <- matrix(c(1-e, e/2, e/2,\\
  \hspace*{4.5cm}                   e/2, 1-e, e/2,\\
  \hspace*{4.5cm}                   e/2, e/2, 1-e),\\
  \hspace*{4.5cm}                 nrow = 3, ncol = 3);\\
  \hspace*{1cm} genotypes <- numeric(n);\\
  \hspace*{1cm} for (i in 1:n)\{\\
  \hspace*{2cm} genotypes[i] <- sample(0:2, size=1, prob=errors[gamma[i]+1,]); \\
  \hspace*{1cm} \}\\
  \hspace*{1cm} return(data.frame(g=genotypes, f=f));\\
  \};\\}}
\item Write a function to obtain an initial guess of $f$. Use the maximum likelihood estimates (MLE) $\hat{f}_l$ under the assumption that $\epsilon=0$.\sol{If $\epsilon=0$, no genotyping errors occur and the observed genotypes $g_i$ will hence be the same as the true genotypes $\gamma_i$. We can therefore simplify the likelihood: 
\begin{equation*}
    \L(f) = \P(\g | f)= \prod_{i=1}^n \P(g_{i}|f) = \prod_{i=1}^n \binom{2}{g_{i}} f^{g_{i}} (1-f)^{2-g_{i}}.
\end{equation*}
The simplified log-likelihood then is:
\begin{equation*}
    \l(f) = \sum_{i=1}^n \log\binom{2}{g_{i}} + \log f \sum_{i=1}^n g_{i} + \log(1-f) (2n - \sum_{i=1}^n g_{i}).
\end{equation*}
Take the derivative:
\begin{equation*}
    \frac{\partial}{\partial f} \l(f) = \frac{1}{f} \sum_{i=1}^n g_{i} - \frac{1}{1-f}( 2n - \sum_{i=1}^n g_{i}).
\end{equation*}
Now set the derivative to zero and solve for $f$. This results in:
\begin{equation*}
    \hat{f} = \frac{\sum_{i=1}^n g_{i}}{2n}.
\end{equation*}}

\item Implement the Metropolis-Hastings MCMC algorithm for this model.
\sol{\solutions{exercise_4_b_ex2_ML.R}}
\item Estimate parameters for the data simulated under \ref{item:simulate}. Plot the trace to verify proper convergence and adjust your proposal kernels if necessary. Then, plot both the two dimensional posterior for $\epsilon$ and $f$, as well as the two marginal posterior distributions.
\sol{\solutions{exercise_4_b_ex2_ML.R}}
\end{enumerate}

\item (Optional) We will now extend our model from Exercise \ref{item:eAndF} to multiple loci. Consider a sample of genotypes $g_{il}=0,1,2$ estimated at $L$ loci $l=1, \ldots, L$ for $n$ individuals $i=1, \ldots, n$ sampled from a randomly mating population in Hardy-Weinberg equilibrium. The observed genotypes $g_{il}$ may differ from the true underlying genotypes $\gamma_{il}=0,1,2$ due to some genotyping errors that occur at rate $\epsilon$ such that 

\begin{equation*}
 \P(g |\gamma, \epsilon) = \begin{cases}
                            1-\epsilon & \quad \mathrm{if } g=\gamma\\
                            \frac{\epsilon}{2} & \quad \mathrm{if } g\neq\gamma
                           \end{cases}.
\end{equation*}

Since the population is in Hardy-Weinberg equilibrium, we have 
\begin{equation*}
 \P(\gamma_{il}|f_l) = \binom{2}{\gamma_{il}} f_l^{\gamma_{il}} (1-f_l)^{2-\gamma_{il}} = \begin{cases}
                                                   (1-f_l)^2 & \quad \mathrm{if } \gamma_{il}=0\\
                                                   2f_l(1-f_l) & \quad \mathrm{if } \gamma_{il}=1\\
                                                   f_l^2 & \quad \mathrm{if } \gamma_{il}=2\\
                                                  \end{cases},
\end{equation*}

where $f_l$ is the unknown allele frequency at locus $l$. Let us finally assume that the allele frequencies are Beta distributed with shape parameter $a$ as

\begin{equation*}
 f_l \sim \mathrm{Beta}(a,a).
\end{equation*}

The goal is now to infer the error rate $\epsilon$, the shape parameter $a$ as well as all $f_l$ in a Bayesian setting by means of a Metropolis-Hastings algorithm. Let us assume exponential prior distributions on both parameters
\begin{equation*}
 \P(\epsilon) \sim \lambda_{\epsilon} e^{-\epsilon\lambda_{\epsilon}}, \quad \P(a) \sim \lambda_a e^{-(1-a)\lambda_a}.
\end{equation*}


(Model inspired by Bresadola \textit{et al.} 2020).

\begin{enumerate}[a)]
\item Write out the full likelihood $\L(\epsilon, \f, a)=\P(\g | \epsilon, \f, a )$ of this model by integrating out the true genotypes $\gamma_{il}$. Here, $\g$ denotes the full set of observed genotypes.\sol{\begin{equation*}
\L(\epsilon, \f, a)=\P(\g | \epsilon, \f, a )= \prod_{l=1}^L \P(f_l|a) \prod_{i=1}^n \sum_{\gamma = 0}^2 \P(g_{il}|\gamma_{il},\epsilon) \P(\gamma_{il}|f_l).
\end{equation*}}
\item Propose symmetric proposal kernels for all single-parameter updates, i.e. for updating $\epsilon$, $a$ and each $f_l$ individually.
\sol{\begin{equation*}
    q(f_l,f_l') \sim {\cal N}(f, \sigma_f^2), \quad q(\epsilon,\epsilon') \sim {\cal N}(\epsilon, \sigma_\epsilon^2), \quad q(a,a') \sim {\cal N}(a, \sigma_a^2),
\end{equation*}
mirrored at 0 and 1 to respect the interval $[0,1]$ for $f_l$ and $\epsilon$ and mirrored at 0 to respect the interval $(0,\infty)$ for $a$.}
\item Write out the simplified Hastings-ratio for single-parameter updates, assuming the symmetric proposal kernels proposed above.
\sol{Parameter $\epsilon$:
\begin{eqnarray*}
h_\epsilon &=& \frac{\P(\epsilon')}{\P(\epsilon)}\prod_{l=1}^L \prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma_{il}, \epsilon')\P(\gamma_{il}|f_l)}{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma_{il}, \epsilon) \P(\gamma_{il}|f_l)}.\\
\end{eqnarray*}
Parameter $a$:
\begin{equation*}
h_a = \frac{\P(a')}{\P(a)}\prod_{l=1}^L \frac{\P(f_{l}|a')}{\P(f_{l}|a)}.
\end{equation*}
Parameter $f_l$:
\begin{equation*}
h_{f_l} = \frac{\P(f_l'|a)}{\P(f_l|a)}\prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma_{il}, \epsilon)\P(\gamma_{il}|f_l')}{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma_{il}, \epsilon) \P(\gamma_{il}|f_l)}.
\end{equation*}
Because of the sum over $\gamma$, we cannot simplify the likelihood any further.}
\item\label{item:simulate_multiL} Implement a function in R to simulate data under this model. Then simulate data with $\epsilon=0.1$ and $a=0.5$ for $n=20$ individuals and $L=100$ loci.
\sol{\script{simData <- function(e, a, n, L)\{\\
  \hspace*{1cm} \# simulate allele frequency\\
  \hspace*{1cm} f <- rbeta(L, a, a);\\
  \hspace*{1cm} \# simulate true genotypes\\
  \hspace*{1cm}  gamma <- matrix(NA, nrow = n, ncol = L)\\
  \hspace*{1cm} for (l in 1:L)\{\\
  \hspace*{2cm} gamma[,l] <- rbinom(n, 2, f[l])\\
  \hspace*{1cm} \}\\
  \hspace*{1cm} \# store error as matrix: probability to change from one genotype to another one\\
  \hspace*{1cm} errors <- matrix(c(1-e, e/2, e/2,\\
  \hspace*{4.5cm}                   e/2, 1-e, e/2,\\
  \hspace*{4.5cm}                   e/2, e/2, 1-e),\\
  \hspace*{4.5cm}                 nrow = 3, ncol = 3);\\
  \hspace*{1cm}  genotypes <- matrix(NA, nrow = n, ncol = L);\\
  \hspace*{1cm} for (i in 1:n)\{\\
  \hspace*{2cm}    for (l in 1:L)\{\\
   \hspace*{3cm}     genotypes[i,l] <- sample(0:2, size=1, prob=errors[gamma[i,l]+1,]); \\
   \hspace*{2cm}   \}\\
   \hspace*{1cm} \}\\
  \};\\}}
\item\label{item:MLE_f} Write functions to obtain initial guesses of the parameters $f_l$ and $a$. For $f_l$, use the maximum-Likelihood estimates (MLEs) $\hat{f}_l$ under the assumption that $\epsilon=0$. For $a$, MLEs for the beta distribution do not have a general closed form solution. Instead, we will make use of method of moments estimators, based on the first moments (sample mean $\Bar{f}$ and variance $\Bar{v}$) of $\hat{\f}$:
\begin{equation*}
    \hat{a} = \Bar{f} \left(\frac{\Bar{f}}{(1-\Bar{v})} - 1 \right).
\end{equation*}\sol{The MLE for $f_l$ is exactly the same as in Exercise \ref{item:MLE_f}:
\begin{equation*}
    \hat{f_l} = \frac{\sum_{i=1}^n g_{il}}{2n}.
\end{equation*}
}

\item Implement the Metropolis-Hastings MCMC algorithm for this model.
\sol{\solutions{exercise_4_b_ex3_ML.R}}
\item Estimate parameters for the data simulated under \ref{item:simulate_multiL}. Plot the trace to verify proper convergence and adjust your proposal kernels if necessary. Then, plot both the two dimensional posterior for $\epsilon$ and $a$, as well as the two marginal posterior distributions.
\sol{\solutions{exercise_4_b_ex3_ML.R}}
\end{enumerate}


\end{enumerate}

\end{document}
