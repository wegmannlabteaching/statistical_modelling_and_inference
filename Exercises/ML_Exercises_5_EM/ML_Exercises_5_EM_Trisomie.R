#------------------------------
# simulate
#------------------------------
getBinomProbs <- function(beta){
  p_A = c(
      1 / (1 + beta),
      2/3 / (2/3 + beta)
  )
  return(cbind(p_A, 1-p_A))
}

simulate <- function(I, lambda_N, beta, pi){
  # simulate trisomies
  tau <- runif(I) < pi
  
  # simulate N_i
  N <- rpois(I, lambda_N)
  
  # simulate n_i
  p <- getBinomProbs(beta)
  n_B <- rbinom(I, N, p[tau + 1, 2])
  
  return(data.frame(N = N, n_A = N - n_B, n_B = n_B, tau = tau))
}


#------------------------------
# LL
#------------------------------
calculateLL <- function(beta, pi, data){
  p <- getBinomProbs(beta)
  return(
    sum(log(
      (1-pi) * dbinom(data$n_A, data$N, p[1,1])
        + pi * dbinom(data$n_A, data$N, p[2,1])
    ))
  )
}

calculateLLSurface <- function(beta, pi, data){
  # calc LL
  LL <- matrix(0, nrow = length(beta), ncol = length(pi))
  for(i in 1:length(pi)){
    LL[,i] <- sapply(beta, calculateLL, pi[i], data)
  }
  return(LL)
}

plotLLSurface <- function(data, col = 'black', nPoints = 100, nlevels = 10, relZlim = 1000){
  # set grid
  pi <- seq(0, 1, length.out = nPoints)
  logbeta <- seq(-1, 1, length.out = nPoints)
  beta <- exp(logbeta)
  
  # calculate LL surface
  LLSurface <- calculateLLSurface(beta, pi, data)
  
  # adjust zlim to have nice contours
  minLL <- max(LLSurface) - relZlim;
  zlim <- c(minLL, max(LLSurface));
  
  # plot as contour lines
  plot(0, type='n', xlab=expression(log(beta)), ylab=expression(pi), xlim=range(logbeta), ylim=range(pi));
  box()
  contour(logbeta, pi, LLSurface, zlim=zlim, nlevels=nlevels, add=TRUE, col=col, drawlabels=FALSE, lty=1, lwd=0.5);
}

#------------------------------
# EM
#------------------------------
calcEMWeights <- function(data, beta, pi){
  # need to work in log to prevent underflow
  p <- getBinomProbs(beta)
  w <- cbind(
      log(1-pi) + data$n_A * log(p[1,1]) + data$n_B * log(p[1,2]),
      log(pi) + data$n_A * log(p[2,1]) + data$n_B * log(p[2,2])
  )
  w <- exp(w - apply(w,1,max));
  return(w / rowSums(w))
}


runEM <- function(data, initBeta = 0.5, initPi = 0.1, maxIterations = 10, deltaLL = 10^-5){
  # prepare storage
  EM <- data.frame(beta = rep(NA, maxIterations), pi = rep(NA, maxIterations), LL = rep(NA, maxIterations))
  EM[1,] <- c(initBeta, initPi, calculateLL(initBeta, initPi, data));
  
  # iterate
  for(i in 2:maxIterations){
    # calculate weights
    w <- calcEMWeights(data, EM$beta[i-1], EM$pi[i-1])
    
    # update pi
    EM$pi[i] <- sum(w[,2]) / nrow(w)
    
    # update beta
    a <- -sum(data$n_A)
    b <- 5/3 * sum(data$n_B) - sum(data$N * (2/3*w[,1] + w[,2]))
    c <- 2/3 * sum(data$n_B)
    
    EM$beta[i] <- (-b - sqrt(b^2 - 4 * a * c)) / (2 * a)
    
    # calc LL
    EM$LL[i] <- calculateLL(EM$beta[i], EM$pi[i], data)
    
    # check for convergence
    if(EM$LL[i] - EM$LL[i-1] < deltaLL){
      return(EM[1:i,])
    }
  }
  
  return(EM)
}

#------------------------------
# guess initial parameters: MLE no trisomie
#------------------------------
guess <- function(data){
  e <- data$n_B / data$n_A
  beta <- median(e)
  pi <- sum(e > 2*beta - min(e)) / nrow(data)
  return(list(beta = beta, pi = pi))
}

#------------------------------
# run
#------------------------------
I <- 100
lambda_N <- 100
pi <- 0.1
beta <- 1

data <- simulate(I, lambda_N, beta, pi)

plotLLSurface(data, col = 'black', nPoints = 100, nlevels = 20, relZlim = 100)
points(log(beta), pi, col = 'purple', pch = 19)

plot(data$n_A, data$n_B, col = data$tau+1)

#EM <- runEM(data, initbeta = beta, initPi = pi)
init <- guess(data)
runEM(data, initBeta = init$beta, initPi = init$pi, maxIterations = 1000)

runEM(data, initBeta = exp(-0.4), initPi = 0.9)


# LL surface fr
#a <- seq(0.3, 0.6, length.out=1000)
#plot(a, sapply(a, LL, pi, data))




