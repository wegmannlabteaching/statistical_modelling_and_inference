\documentclass{../exercises}

\exerciseTitle{5}{Expectation-Maximization Algorithm}

\begin{document}

\maketitle


\def\n{\boldsymbol{n}}
\def\btau{\boldsymbol{\tau}}
\def\bmu{\boldsymbol{\mu}}
\def\bsigma{\boldsymbol{\sigma}}
\def\bpi{\boldsymbol{\pi}}
\def\bN{\boldsymbol{N}}

\begin{enumerate}[1.]
 \item Consider two chromosomes $A$ and $B$, of which the first shall always be diploid while the latter shall be mostly diploid but may occur as a trisomie in a few individuals (three rather than two copies). The goal of this exercise is to develop a classifier that enables the identification of such trisomies from sequencing data.

 Let $\tau_i=0$ and $\tau_i=1$ indicate whether individual $i$ suffers from a trisomie at Chromosome $B$ and let $\pi$ indicate the frequency of such trisomies in a population, such that $\P(\tau_i=1|\pi) = \pi$. Let $N_i$ denote the number of sequencing reads of individual $i=1, \ldots, I$, and $n_i=(n_{Ai}, n_{Bi})$ the sequencing reads of individual $i$ that mapped against Chromosomes $A$ and $B$, respectively, with $N_i = n_{Ai} + n_{Bi}$. Let one copy of Chromosome $B$ attract $\beta$ times as many reads as a copy of Chromosome $A$. In the diploid case ($\tau_i=0$), there are two copies of each chromosome and a random read thus maps against Chromosome $B$ with probability $\frac{2\beta}{2 + 2\beta} = \frac{\beta}{1+\beta}$. In case of a trisomie ($\tau_i=1$), there are three copies of Chromosome $B$ and a random read thus maps against Chromosome $B$ with probability $\frac{3\beta}{2+3\beta} = \frac{\beta}{2/3+\beta}$.

 The goal is now to develop a machine learning strategy that allows for the joint estimation of $\beta$ and $\pi$ from sequencing data $\n=(n_1, \ldots, n_I)$ of $I$ individuals.

\begin{enumerate}[a)]
 \item What are the probabilities that a random read maps against Chromosome $A$ in the two cases $\tau_i=0$ and $\tau_i=1$? Simplify these probabilities to a single fraction.

 \sol{For $\tau_i=0$ and $\tau_i=1$, the probabilities are $1-\frac{\beta}{1+\beta} = \frac{1}{1+\beta}$ and $1-\frac{\beta}{2/3 + \beta} = \frac{2/3}{2/3 + \beta}$, respectively.}

 \item Draw a DAG of the proposed model. Which variables are \emph{latent variables}?

  \sol{\begin{figure}[!h]\centering\begin{tikzpicture}
            \node[latent] (1) {$\pi$};
            \node[latent, right = of 1] (2) {$\tau_i$};
            \node[latent, above=of 2] (3) {$\beta$};
            \node[obs, right=of 2]    (D) {$n_i$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate1} {(2) (D)} {$i=1,\ldots,I$};
            \edge {1} {2};
            \edge {2} {D};
            \edge {3} {D};
            \end{tikzpicture}\end{figure} The variables $\tau_i$ are considered latent variables.}

 \item Define the binomial probability $\P(n=(n_{A}, n_{B}) | N, \tau, \beta)$ under the above assumptions. \label{trisomie:proBn}

 \sol{$$\P(n=(n_{A}, n_{B}) | N, \tau, \beta) = \begin{cases}
                                                   \binom{N}{n_A}\left(\frac{1}{1+\beta}\right)^{n_{A}}\left(\frac{\beta}{1+\beta}\right)^{n_{B}} \quad &\iif \tau=0\\
                                                   \binom{N}{n_A}\left(\frac{2/3}{2/3 + \beta}\right)^{n_{A}}\left(\frac{\beta}{2/3 + \beta}\right)^{n_{B}} \quad &\iif \tau=1
                                                  \end{cases}$$}

 \item Write out the log-probability $\log \P(n=(n_{A}, n_{B}) | N, \tau, \beta)$, which will be helpful later.

 \sol{$$\log \P(n=(n_{A}, n_{B}) | N, \tau, \beta) = \begin{cases}
                                                   \log \binom{N}{n_A} + n_B \log \beta - N \log(1+\beta) \quad &\iif \tau=0\\
                                                   \log \binom{N}{n_A} + n_A \log 2/3 + n_B \log \beta - N \log \left(2/3 + \beta\right) \quad &\iif \tau_=1
                                                  \end{cases}$$}

 \item Write out the likelihood $\L(\beta, \pi)=\P(\n|\bN,\beta, \pi)$ under this model and express it using the probabilities $\P(\tau|\pi)$ and $\P(n | N, \tau, \beta)$. Simplify by spelling out the sum across $\tau_i$ and $\P(\tau|\pi)$, but not $\P(n |N, \tau, \beta)$ (i.e. do not write out the definition you found in \ref{trisomie:proBn}). Convince yourself that there is no hope in finding an analytical solution for the MLE $(\hat{\beta},\hat{\pi}) = \argmax_{\beta,\pi} \L(\beta, \pi)$.

 \sol{$$\L(\beta, \pi)=\P(\n|\bN,\beta, \pi) = \prod_{i=1}^{I} \sum_{\tau=0}^1 \P(n_i | N_i, \tau, \beta)\P(\tau|\pi) = \prod_{i=1}^{I} \left[ \P(n_i | N_i,\tau=0, \beta)(1-\pi) + \P(n_i | N_i, \tau=1, \beta)\pi \right]$$
 There is no hope to find an analytical solution of the MLE since the log-likelihood will be
 $$\l(\beta, \pi) = \sum_{i=1}^I \log\left( \P(n_i | N_i, \tau=0, \beta)(1-\pi) + \P(n_i | N_i, \tau=1, \beta)\pi \right)$$
 and hence the partial derivatives of $\l(\beta,\pi)$ will contain the denominators $$\P(n_i | N_i, \tau=0, \beta)(1-\pi) + \P(n_i | N_i, \tau=1, \beta)\pi,$$ which are unique for each individual $i$.}

 \item Begin now to derive the Expectation-Maximization algorithm for this model. The first step is to write out the complete data likelihood $\L_c(\beta,\pi)=\P(\n, \btau | \bN, \beta, \pi)$, which treats the latent variables as observed data. Write out $\L_c(\beta,\pi)$ using the probabilities $\P(\tau|\pi)$ and $\P(n | N, \tau, \beta)$, without spelling them out.

 \sol{$$\L_c(\beta,\pi) = \prod_{i=1}^I \P(n_i| N_i, \tau_i, \beta)\P(\tau_i|\pi)$$}

 \item Determine the log complete data likelihood $\l_c(\beta,\pi) = \log \L_c(\beta, \pi)$.

 \sol{$$\l_c(\beta,\pi) = \sum_{i=1}^I \left[ \log \P(n_i | N_i, \tau_i, \beta) + \log \P(\tau_i|\pi) \right]$$}

 \item Next, write out the $Q$ function $Q(\beta,\pi|\beta', \pi') = \E[\l_c(\beta,\pi)|\n,\beta', \pi']$. As above, use the probabilities $\P(\tau|\pi)$ and $\P(n | \tau, \beta)$. Then simplify by spelling out the sum across all $\tau_i$ and $\P(\tau|\pi)$, but not $\P(n | N, \tau, \beta)$. Also, there is no need to spell out the weights $\P(\tau|n_i, \beta', \pi')$, which you may denote by $w_{0i}$ and $w_{1i}$ for the cases $\tau=0$ and $\tau=1$, respectively.

 \sol{\begin{align*}Q(\beta,\pi|\beta', \pi') &= \sum_{\btau} \l_c(\beta,\pi)\P(\btau|\n, \beta', \pi')\\
 &= \sum_{i=1}^I \sum_{\tau_i} \left[ \log \P(n_i|N_i, \tau_i, \beta) + \log \P(\tau_i|\pi) \right]\P(\tau_i|n_i, \beta', \pi')\\
 &= \sum_{i=1}^I \Big( \left[ \log \P(n_i|N_i, \tau_i=0, \beta) + \log \P(\tau_i=0|\pi) \right] w_{0i}
 + \left[ \log \P(n_i|N_i, \tau_i=1, \beta) + \log \P(\tau_i=1|\pi) \right] w_{1i} \Big) \end{align*}}

 \item Express the weights $\P(\tau|n_i, \beta', \pi')$ using the probabilities $\P(\tau|\pi)$ and $\P(n | N, \tau, \beta)$.

 \sol{$$\P(\tau|n_i, \beta', \pi') = \frac{\P(n_i | N_i, \tau, \beta')\P(\tau|\pi')}{\P(n_i | \beta', \pi')} = \frac{\P(n_i | N_i, \tau, \beta')\P(\tau|\pi')}{\P(n_i | N_i, \tau=0, \beta')(1-\pi') +\P(n_i | N_i, \tau=1, \beta')\pi}$$}

 \item Find the value $\hat{\beta}=\argmax_{\beta} Q(\beta,\pi|\beta', \pi')$ that maximizes the $Q$ function. Note that the values $\beta'$ and $\pi'$ are known constants.

 \sol{\begin{align*}
       \frac{\partial}{\partial \beta} Q(\beta,\pi|\beta', \pi') &= \sum_{i=1}^I \Big( w_{0i}\frac{\dd}{\dd \beta}\log \P(n_i|N_i, \tau_i=0, \beta) + w_{1i}\frac{\dd}{\dd \beta} \log \P(n_i|N_i, \tau_i=1, \beta) \Big)\\
       &= \sum_{i=1}^I \left[ w_{0i} \left( \frac{n_{Bi}}{\beta} - \frac{N_i}{1+\beta} \right) + w_{1i}\left( \frac{n_{Bi}}{\beta} - \frac{N_i}{2/3+\beta} \right) \right]\\
       &= \sum_{i=1}^I \left[ \frac{n_{Bi}}{\beta} - N_i \left( \frac{w_{0i}}{1+\beta} + \frac{w_{1i}}{2/3 + \beta} \right) \right] = 0,
      \end{align*}
      where we used $w_{0i}+w_{1i} = 1$ to simplify the first term in the sum. Solving for $\beta$ yields
      \begin{align*}
       \sum_{i=1}^I \big[ n_{Bi} (1+\beta)(2/3+\beta) - N_i\left[ w_{0i}\beta(2/3+\beta) + w_{1i}\beta(1+\beta) \right] \big] &= 0\\
       \sum_{i=1}^I \big[ n_{Bi} (2/3 +\beta5/3 +\beta^2) - N_i\left[ w_{0i}(\beta2/3+\beta^2) + w_{1i}(\beta+\beta^2) \right] \big] &= 0\\
       \beta^2 \sum_{i=1}^I [n_{Bi} - N_i(w_{0i} + w_{1i})] + \beta \sum_{i=1}^I [ n_{Bi}5/3 - N_i(w_{01} 2/3 + w_{1i}) ] + \sum_{i=1}^I n_{Bi}2/3 &= 0\\
       -\beta^2 \sum_{i=1}^I n_{Ai} + \beta \sum_{i=1}^I [ n_{Bi}5/3 - N_i(w_{01} 2/3 + w_{1i}) ] + 2/3\sum_{i=1}^I n_{Bi} = & 0
      \end{align*}
      Defining
      \begin{align*}
       a &= -\sum_{i=1}^I n_{Ai}\\
       b &= 5/3 \sum_{i=1}^I  n_{Bi} - \sum_{i=1}^I N_i(w_{0i} 2/3 + w_{1i})\\
       c &= 2/3\sum_{i=1}^I n_{Bi},
      \end{align*}
      this quadratic equation has the solutions
      \begin{equation*}
       \hat{\beta} = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a},
      \end{equation*}
      of which only the solution
      \begin{equation*}
       \hat{\beta} = \frac{-b - \sqrt{b^2 - 4ac}}{2a}
      \end{equation*}
      results in $\beta > 0$ (convince yourself numerically later).
}

\item Find the value $\hat{\pi}=\argmax_{\pi} Q(\beta,\pi|\beta', \pi')$ that maximizes the $Q$ function.

 \sol{\begin{align*}
       \frac{\partial}{\partial \pi} Q(\beta,\pi|\beta', \pi') &= \sum_{i=1}^I \Big( w_{0i} \frac{\dd}{\dd \pi}\log (1-\pi) + w_{1i} \frac{\dd}{\dd \pi} \log \pi  \Big)\\
       &= \sum_{i=1}^I \left[ \frac{w_{1i}}{\pi} - \frac{w_{0i}}{1-\pi} \right] = 0\\
       \end{align*}
       Solving for $\pi$ yields
      \begin{align*}
       0 &= \sum_{i=1}^I \left[ w_{1i}(1-\pi) - w_{0i}\pi \right] = \sum_{i=1}^I \left[ w_{1i} -\pi \right] = \sum_{i=1}^I w_{1i} -\pi I\\
       \hat{\pi} &= \frac{1}{I} \sum_{i=1}^I w_{1i}\\
      \end{align*}
 }

 \item Implement the EM algorithm for this model using the derived equations. Test it using simulations, for which you may simulate $N_i \sim \mathrm{Pois}(\lambda_N)$ with a suitable choice for the average number of reads $\lambda_N$ (e.g. $\lambda_N= 1000$).

 \sol{\solutions{ML_Exercises_5_EM/ML_Exercises_5_EM_Trisomie.R}}

 \item You will probably note that the likelihood surface has two peaks and that solution the EM finds is sensitive to the staring location. How could you obtain robust estimates of the initial values that are cheap to calculate?

 \sol{Since $\beta = \E\left[ \frac{n_{Ai}}{n_{Bi}}\right]$ in case of $\tau=0$, one might get a crude estimates of $\beta$ by calculating $\hat{\beta}_i = n_{Ai} / n_{Bi}$ for each individual $i$. Assuming less than half of all individuals have trisomies, the median $\hat{\beta} =\mathrm{med}(\hat{\beta}_i)$ across all $\hat{\beta}_i$  might then be a robust starting value. For $\pi$, one might use the fraction of $\hat{\beta}_i$ that are excessively large, for instance the fraction for which $\hat{\beta}_i > \hat{\beta} + \left(\hat{\beta} - \min(\hat{\beta}_i)\right)$. }

\end{enumerate}


%------------------------------------
% Coins
%------------------------------------

\item Consider two biased coins $A$ and $B$ that produce a \textit{Head} with unknown probabilities $\theta_A$ and $\theta_B$, respectively. In an experiment, a coin $z$ is chosen with probabilities $\P(z = A|\pi)=\pi$ and $\P(z = B\pi)=1-\pi$. The chosen coin is then thrown $m$ times and the number \textit{Heads} $x$ is recorded. Assuming throws to be independent given a particular value of $\theta$, we have
\begin{equation*}
 \P(x|\theta) = \binom{m}{x} \theta^{x}(1-\theta)^{m-x}
\end{equation*}


Suppose we obtain the results of $n$ such experiments as $\x=\{x_1, \ldots, x_n\}$. The goal is now to develop a machine learning approach that infers $\theta_A$, $\theta_B$ and $\pi$ jointly from such data. For this we will use the EM algorithm.

\begin{enumerate}[a)]

 \item Draw a DAG of the proposed model. Which variables are \emph{latent variables}?

  \sol{\begin{figure}[!h]\centering\begin{tikzpicture}
            \node[latent] (1) {$\pi$};
            \node[latent, right = of 1] (2) {$z_i$};
            \node[obs, right=of 2]    (D) {$x_i$};
            \node[latent, above=of 2] (A) {$\theta_A$};
            \node[latent, above=of D] (B) {$\theta_B$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate1} {(2) (D)} {$i=1,\ldots,n$};
            \edge {1} {2};
            \edge {2} {D};
            \edge {A} {D};
            \edge {B} {D};
            \end{tikzpicture}\end{figure} The variables $z_i$ are considered latent variables.}

\item Write out the likelihood $\L(\pi, \btheta)$ for this model, where $\btheta=(\theta_A, \theta_B)$. No need to spell out the probabilities $\P(z|\pi)$ and $\P(x | \theta)$.

\sol{\begin{equation*}
  \L(\pi, \btheta) = \P(\x|\pi, \btheta) = \prod_{i=1}^n \left[ \P(x_i|\theta_A) \P(z_i=A|\pi) + \P(x_i|\theta_B) \P(z_i=B|\pi) \right]
\end{equation*}}

\item Write out the complete data likelihood $\L_c(\pi, \btheta)$. No need to spell out the probabilities $\P(z|\pi)$ and $\P(x | \theta)$.
\sol{\begin{equation*}
 \L_c(\pi, \btheta) = \P(\x,\boldsymbol{z}| \pi \theta) =  \prod_{i=1}^n \P(x_i| \theta_{z_i})\P(z_i|\pi),
\end{equation*}
where $\boldsymbol{z}=\{z_1, \ldots, z_n\}$ is the vector of coins used.}

\item Write out the log complete data likelihood $\l_c(\pi, \btheta) = \log \L_c(\pi, \btheta)$ of this model. No need to spell out the probabilities $\P(z|\pi)$ and $\P(x | \theta)$.
\sol{\begin{equation*} l_c(\pi, \btheta) = \sum_{i=1}^n \left[ \log P(x_i| \theta_{z_i}) + \log \P(z_i|\pi) \right]
\end{equation*}}
 
\item Write out the $Q$ function $Q(\pi,\btheta|\pi',\btheta')=\E\left[\l_c(\pi,\btheta)|\x, \pi' \btheta' \right]$. Spell out the sum across the latent variable and the probability $\P(z|\pi)$, but not $\P(z|\pi)$. You may use $w_{Ai}$ and $w_{Bi}$ to indicate the weights $\P(z_i=A|x_i,\pi', \btheta')$ and $\P(z_i=B|x_i,\pi', \btheta')$, respectively.
\sol{\begin{align*}Q(\theta, \bar{\theta}) &= \sum_{i=1}^n  \sum_{k\in\{A,B\}}  \left[ \log P(x_i| \theta_k) + \log \P(z_i=k|\pi) \right]\P(z_i=k|x_i,\pi', \btheta') \\
&= \sum_{i=1}^n \Big[ (\log P(x_i| \theta_A) + \log \pi ) w_{Ai} + ( \log P(x_i| \theta_B) + \log (1-\pi)) w_{Bi}\Big]
\end{align*}}

\item Express the weights $w_{ki} =\P(z_i=k|x_i, \pi', \btheta')$ in terms of the probabilities $\P(x|\theta)$ and $\P(z|\pi)$. No need to spell out the probabilities $\P(z|\pi)$ and $\P(x | \theta)$.
\sol{\begin{equation*}\label{eq:weight}
 P(z_i=k|x_i, \pi', \btheta') =  \frac{P(x_i|\btheta_k')P(z_i=k|\pi')}{P(x_i|\pi', \btheta')} =  \frac{P(x_i|\btheta_k')P(z_i=k|\pi')}{\sum_{j\in\{A,B\}} P(x_i|\btheta_j')P(z_i=j|\pi')}
\end{equation*}}

\item Find the value $\hat{\pi} = \argmax_{\pi} Q(\pi,\btheta|\pi',\btheta')$ that will then serve as the new estimates of $\pi$ for the next iteration of the EM algorithm.

\sol{\begin{align*}
      \frac{\partial}{\partial \pi} Q(\pi,\btheta|\pi',\btheta') &= \sum_{i=1}^n \Big[ w_{Ai} \frac{\dd}{\dd \pi} \log \pi + w_{Bi} \frac{\dd}{\dd \pi} \log (1-\pi) \Big]\\
      &= \sum_{i=1}^n \Big[ \frac{w_{Ai}}{\pi} - \frac{w_{Bi}}{1- \pi} \Big] = 0\\
      0 &= \sum_{i=1}^n \Big[ w_{Ai} (1-\pi) - w_{Bi} \pi \Big] = \sum_{i=1}^n \Big[ -\pi  + w_{Ai} \Big] = -n\pi +  \sum_{i=1}^n w_{Ai}\\
      \hat{\pi} &= \frac{1}{n}\sum_{i=1}^n w_{Ai}
     \end{align*}}

\item  Find the value $\hat{\theta_A} = \argmax_{\theta_A} Q(\pi,\btheta|\pi',\btheta')$.
\sol{\begin{align*}
  \frac{\partial}{\partial \theta_A} Q(\pi,\btheta|\pi',\btheta') &= \sum_{i=1}^n w_{Ai} \frac{\dd}{\dd \theta_A} \log P(x_i| \theta_A)
  = \sum_{i=1}^n w_{Ai} \Big[ \frac{\dd}{\dd \theta_A} x_i \log \theta_A + \frac{\dd}{\dd \theta_A} (m-x_i) \log (1-\theta_A) \Big]\\
  &= \sum_{i=1}^n w_{Ai} \Big[ \frac{x_i}{\theta_A} - \frac{m-x_i}{1-\theta_A} \Big] = 0\\
  0 &= \sum_{i=1}^n w_{Ai} \Big[ x_i (1-\theta_A) + (m-x_i)\theta_A \Big] = \sum_{i=1}^n w_{Ai} \Big[ x_i - m\theta_A \Big] = -\theta_A m\sum_{i=1}^n w_{Ai} + \sum_{i=1}^n w_{Ai}x_i\\
  \hat{\theta_A} &= \frac{\sum_{i=1}^n w_{Ai}x_i}{m\sum_{i=1}^n w_{Ai}}
\end{align*}}

\item Find the value $\hat{\theta_B} = \argmax_{\theta_B} Q(\pi,\btheta|\pi',\btheta')$.

\sol{This is analogous to the case of $\theta_A$: \begin{equation*}
  \hat{\theta_B} = \frac{\sum_{i=1}^n w_{Bi}x_i}{m\sum_{i=1}^n w_{Bi}}
\end{equation*}}

\item With all equations at hand, implement this EM algorithm in \texttt{R} and test it with simulations.

\sol{\solutions{ML_Exercises_5_EM/ML_Exercises_5_EM_Coins.R}}

\end{enumerate}


%------------------------------------
% Gaussian micture models
%------------------------------------

\item A default method for clustering are Gaussian mixture models, under which data $\d=(d_1, \ldots, d_n)$ is explained by $K$ Gaussian distributions with their own means and variance (or mean vector and variance-covariance matrix in higher dimensions). We will develop such Gaussian mixture models for the one dimensional case. Let $\bmu=(\mu_1,\ldots, \mu_K)$ and $\bsigma^2 = (\sigma^2_1,\ldots, \sigma^2_K)$ denote the means and variances of the $K$ distributions, which are mixed with proportions $\bpi = (\pi_1, \ldots, \pi_K)$. Note that $\sum_{k=1}^K \pi_k = 1$.

\begin{enumerate}[a)]

\item Draw a DAG of such a mixture model. Name the latent variables $z_i, i=1,\ldots, n$.

  \sol{\begin{figure}[!h]\centering\begin{tikzpicture}
            \node[latent] (1) {$\bpi$};
            \node[latent, right = of 1] (2) {$z_i$};
            \node[obs, right=of 2]    (D) {$d_i$};
            \node[latent, above=of 2] (A) {$\bmu$};
            \node[latent, above=of D] (B) {$\bsigma$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate1} {(2) (D)} {$i=1,\ldots,n$};
            \edge {1} {2};
            \edge {2} {D};
            \edge {A} {D};
            \edge {B} {D};
            \end{tikzpicture}\end{figure}}


\item Write out the likelihood $\L(\bpi, \bmu, \bsigma^2)$ for this model. No need to spell out the probabilities $\P(d|\mu,\sigma^2)$.

\sol{\begin{equation*}
      \L(\bpi, \bmu, \bsigma^2) = \prod_{i=1}^n \sum_{z=1}^K \pi_z \P(d_i|\mu_z, \sigma^2_z)
     \end{equation*}}

\item Write out the $Q$ function $Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}')$. No need to spell out the probabilities $\P(d|\mu,\sigma^2)$.

\sol{ We first need to write out the log complete data likelihood
\begin{align*}
 \L_c(\bpi, \bmu, \bsigma^2) &= \prod_{i=1}^n \pi_{z_i} \P(d_i|\mu_{z_i}, \sigma^2_{z_i})\\
 \l_c(\bpi, \bmu, \bsigma^2) &= \sum_{i=1}^n \Big[ \log \pi_{z_i} + \log \P(d_i|\mu_{z_i}, \sigma^2_{z_i}) \Big],
\end{align*}
from which we get
\begin{align*}
  Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') &= \E\left[\l_c(\pi, \bmu, \bsigma^2) | \d, \bpi', \bmu', {\bsigma^2}'\right]\\
  &= \sum_{i=1}^n \sum_{z=1}^K \Big[ \log \pi_z + \log \P(d_i|\mu_z, \sigma^2_z) \Big] \P(z|\d, \bpi', \bmu', {\bsigma^2}')\\
  &= \sum_{i=1}^n \sum_{z=1}^K \Big[ \log \pi_z + \log \P(d_i|\mu_z, \sigma^2_z) \Big] w_{zi},
\end{align*}
where we denote by $w_{zi}$ the weights $\P(z|\d, \bpi', \bmu', {\bsigma^2}')$.}

\item Find the values $\hat{\mu_k} = \argmax_{\mu_k} Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}')$.

\sol{
\begin{align*}
 \frac{\partial}{\partial \mu_k} Q(\pi, \bmu, \bsigma^2|\pi', \bmu', {\bsigma^2}') &= \sum_{i=1}^n w_{ki} \frac{\dd}{\dd \mu_k} \log \P(d_i|\mu_k, \sigma^2_k)\\
 &= \sum_{i=1}^n w_{ki} \frac{\dd}{\dd \mu_k} \left[ \frac{(d_i-\mu_k)^2}{2\sigma^2_k} \right] = \sum_{i=1}^n w_{ki} \left[ \frac{-2(d_i-\mu_k)\mu_k}{2\sigma^2_k} \right] = 0\\
 0 &= \sum_{i=1}^n w_{ki} \left[ d_i-\mu_k \right] = \sum_{i=1}^n w_{ki} d_i - \mu_k\sum_{i=1}^n w_{ki}\\
 \hat{\mu_k} &= \frac{\sum_{i=1}^n w_{ki} d_i}{\sum_{i=1}^n w_{ki}}
\end{align*}}

\item Find the values $\hat{\sigma}^2_k = \argmax_{\sigma^2_k} Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}')$.

\sol{
\begin{align*}
 \frac{\partial}{\partial \sigma^2_k} Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') &= \sum_{i=1}^n w_{ki} \frac{\dd}{\dd \sigma^2_k} \log \P(d_i|\mu_k, \sigma^2_k)\\
 &= \sum_{i=1}^n w_{ki} \frac{\dd}{\dd \sigma^2_k} \left[ -\frac{1}{2} \log \sigma^2_k +  \frac{(d_i-\mu_k)^2}{2\sigma^2_k} \right] =  \sum_{i=1}^n w_{ki} \left[ -\frac{1}{2\sigma^2_k} +  \frac{(d_i-\mu_k)^2}{2(\sigma^2_k)^2}  \right] = 0\\
 0 &= \sum_{i=1}^n w_{ki} \left[ -\sigma^2_k +  (d_i-\mu_k)^2 \right] = -\sigma^2_k \sum_{i=1}^n w_{ki} + \sum_{i=1}^n w_{ki} (d_i-\mu_k)^2\\
 \hat{\sigma}^2_k &= \frac{\sum_{i=1}^n w_{ki} (d_i-\mu_k)^2}{\sum_{i=1}^n w_{ki}}
\end{align*}}


\item Finding the value $\hat{\pi_k} = \argmax_{\pi_k} Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}')$ is a bit trickier due to the constraint $\sum_{k=1}^K \pi_k = 1$. Solve it thus first for the case of two mixtures with probabilities $\pi_1=\pi$ and $\pi_2=1-\pi$.

\sol{
\begin{align*}
 \frac{\partial}{\partial \pi} Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') &= \sum_{i=1}^n \sum_{z=1}^K w_{zi} \frac{\dd}{\dd \pi} \log \pi_z\\
 &= \sum_{i=1}^n \left[ \frac{w_{1i}}{\pi} - \frac{w_{2i}}{1-\pi} \right] = 0\\
 0 &= \sum_{i=1}^n \left[ w_{1i} (1-\pi) - w_{2i}\pi \right] = -n\pi + \sum_{i=1}^n w_{1i}\\
 \hat{\pi} &= \frac{1}{n} \sum_{i=1}^n w_{1i}
\end{align*}}

\item For the case with an arbitrary number of mixtures, we need to find the values $\hat{\bpi},\hat{\lambda} = \argmax_{\bpi,\lambda} Q_(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}')$ of the Langrangian
\begin{equation*}
  Q_L(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') = Q(\bpi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') + \lambda \left(1-\sum_{k=1}^K \pi_k = 1 \right)    ,                                                                                                                                                                 \end{equation*}
which ensure that maximizing it adheres to the constraint $\sum_{k=1}^K \pi_k = 1$.

\sol{
\begin{align*}
 \frac{\partial}{\partial \pi_k} Q_L(\pi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') &= \sum_{i=1}^n w_{ki} \frac{\dd}{\dd \pi_k}\Big[ \log \pi_k + \lambda \pi_k\Big] =  \sum_{i=1}^n w_{ki}\Big[ \frac{1}{\pi_k} + \lambda\Big] = \sum_{i=1}^n w_{ki}\Big[ 1 + \lambda \pi_k\Big] = 0\\
 \frac{\partial}{\partial \lambda} Q_L(\pi, \bmu, \bsigma^2|\bpi', \bmu', {\bsigma^2}') &= 1-\sum_{k=1}^K \pi_k = 0.
\end{align*}
Solving this system of $K+1$ equations for $\pi_k$ yields the unsurprising result
\begin{equation*}
\hat{\pi_k} = \frac{1}{n} \sum_{i=1}^n w_{ki}
\end{equation*}}

\item With all equations at hand, implement this EM algorithm in \texttt{R} and test it with simulations.

\sol{\solutions{ML_Exercises_5_EM/ML_Exercises_5_EM_Gaussian.R}}


\item Let us extend the above Gaussian mixture model to $k$ mixtures.


\end{enumerate}

\end{enumerate}

\end{document}
