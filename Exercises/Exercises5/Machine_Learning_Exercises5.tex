\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.3}

%\usepackage[withSolutions]{optional}
\usepackage[withoutSolutions]{optional}

\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.2cm}\\ {\color{red} \texttt{#1}}}\bigskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}


%opening
\title{Machine Learning - Exercises 5}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{Approximate Bayesian Computation (ABC)}


\begin{enumerate}[1.]
\item Consider a sample $\boldsymbol{x} = \{x_1, \ldots, x_m\}$ of $m$ values drawn from a normal distribution with mean $\mu$ and variance $\sigma^2$. The goal of this exercises is now to infer $\mu$ and $\sigma^2$ using am Approximate Bayesian Computation (ABC) approach, and to compare the results to the true posterior distributions.
 
\begin{enumerate}[a)]
\item Implement a function that takes a sample $\boldsymbol{x}$ as input and returns a vector of summary statistics calculated on these samples. The statistics to be included should be i) the sample mean, ii) the sample variance, iii) the median of the sample, iv) the minimum and v) the maximum value, as well as vi) the difference between these.\sol{\script{calc.stats <- function (x)\{\\
 \hspace*{1cm} S <- c(mean(x), var(x), median(x), range(x), max(x)-min(x));\\
 \hspace*{1cm} names(S) <- c("mean", "var", "median", "min", "max", "range");\\
 \hspace*{1cm} return(S);\\
 \}}}

\item Next implement a function to simulate data for a fixed $\mu$ and $\sigma$. This function should simulate a sample of size $m$ from a normal distribution with these parameters, and then use the function written above to calculate and return summary statistics for each simulation. Then, write a function to generate a large set of $N$ simulations. For each simulation, draw parameter values randomly from uniform priors $\mu \sim U[-10,10]$ and $\sigma^2 \sim U(0,10]$, and then use the simulation function to simulate a sample and calculate summary statistics. The output of that function should then be a data.frame with the parameters and summary statistics of all simulations. Note that you can use the function \texttt{apply} to speed up the simulation step.
\sol{\script{one.sim <- function(x)\{\\
 \hspace*{1cm} return(calc.stats(rnorm(sampleSize, mean = x[1], sd=sqrt(x[2]))));\\
\}\\
make.sim <- function(N, m)\{\\
 \hspace*{1cm} P <- data.frame(mu=runif(N, min=-10, max=10), sigma2=runif(N, min=0 , max=10));\\
 \hspace*{1cm} S <- t(apply(P, 1, one.sim));\\
 \hspace*{1cm} return(data.frame(cbind(P,S)));\\
\}}}

\item Next, implement a function to generate posterior samples. This function should take as an input a large set of simulations $S$, a vector of observed statistics $s_{o}$ calculated on some observed data for which estimates are to be produced, and a tolerance $\delta$. The function should then perform the following ABC steps:
\begin{enumerate}[i]
\item Standardize the statistics of all simulations to have the same mean and variance. Do not forget to standardize the observed statistics the same way!
\item Calculate the Euclidean distance between $s_o$ and each simulation.
\item Identify the fraction $\delta$ of all simulations with smallest distances.
\item Return these accepted simulations in a new data structure.
\end{enumerate}\sol{\script{get.posterior.samples <- function(Sims, S.obs, tolerance)\{\\
 \hspace*{1cm} \#standardize stats\\
\hspace*{1cm} nStats <- length(S.obs);\\
\hspace*{1cm} nParams <- dim(Sims)[2] - nStats;\\
\hspace*{1cm} Sims.stats <- Sims[,(nParams+1):dim(Sims)[2]];\\
\hspace*{1cm} for(i in nStats)\{\\
\hspace*{2cm} mean <- mean(Sims[,i]);\\
\hspace*{2cm} sd <- sd(Sims[,i]);\\
\hspace*{2cm} Sims.stats[,i] <- (Sims[,i] - mean) / sd;\\
\hspace*{2cm} S.obs[i] <- (S.obs[i] - mean) / sd;\\
\hspace*{1cm} \}\\
\\
\hspace*{1cm} \#calc euclidian dist\\
\hspace*{1cm} x <- t(Sims.stats) - S.obs;\\
\hspace*{1cm} distances <- sqrt(colSums(x*x));\\
\\
\hspace*{1cm} \#do rejection\\
\hspace*{1cm} distances.sorted <- sort(distances, index.return=T);\\
\hspace*{1cm} bestSims <- distances.sorted\textdollar ix[1:round(tolerance*dim(Sims)[1])];\\
\hspace*{1cm} return(Sims[bestSims,]);\\
\} }}

\item Finally implement a function to estimate posterior densities and to compare the ABC posteriors to the true posteriors. This function should take as input the structure with the accepted simulations, as well as the true $\mu$ and $\sigma^2$ you used to generate $s_o$. The function should then:
\begin{enumerate}[i]
\item Generate ABC posterior estimates using Kernel density estimation on the accepted parameters.
\item Plot these posterior distributions.
\item Compare them to the true posterior distributions, which are:
\begin{itemize}
 \item For $\mu$ a normal distribution with mean equal to the sample mean and standard deviation given by the true $\sigma^2/m$.
 \item For $\sigma^2$ a scaled $\chi^2$ distribution as
 \begin{equation}
  \frac{m-1}{\sigma^2} \chi^2 \left(\frac{x(m-1)}{sigma^2}, m-1\right).
 \end{equation}
  You can evaluate the density of the $\chi^2$ distribution using the function \texttt{dchisq}.
 \end{itemize}
\end{enumerate}\sol{\script{plot.posterior <- function(Post, true.mu, true.sigma2, observed.mean, m)\{\\
\hspace*{1cm} par(mfrow=c(1,2), mar=c(4.2,4,1.2,0.2));\\
\hspace*{1cm} \#mu\\
\hspace*{1cm} x <- seq(-10,10, length.out=100);\\
\hspace*{1cm} plot(x, dnorm(x,mean=observed.mean, sd=sqrt(true.sigma2/m)), type='l', main="mu", xlab="mu", ylab="Posterior density");\\
\hspace*{1cm} lines(rep(true.mu,2), par("usr")[3:4], lty=2);\\
\hspace*{1cm} lines(density(Post\textdollarmu), main="mu", col='red');\\
  \\
\hspace*{1cm} \#sigma2\\
\hspace*{1cm} x <- seq(0,10, length.out=100);\\
\hspace*{1cm} plot(x, (m-1)/true.sigma2 * dchisq(x*(m-1)/true.sigma2, m-1), main="sigma2", type='l', , xlab="sigma2", ylab="Posterior density");\\
\hspace*{1cm} lines(rep(true.sigma2,2), par("usr")[3:4], lty=2);\\
\hspace*{1cm} lines(density(Post\textdollarsigma2), xlim=c(0,4), col="red");\\
\}}}

\item Now use your functions to test the accuracy of ABC in this example. To do so, generate some observed data set by choosing a true $\mu$ and $\sigma^2$, for which you simulate data and calculate the observed summary statistics that you store in the vector $s_o$. Then infer the parameters using your other functions. How does the choice of tolerance and the number of simulations affect the accuracy of your estimates?\sol{Estimates should get better if a large number of simulations is used with a relatively small tolerance. However, you will need at least several hundred if not thousand of posterior sample to get a smooth posterior estimate.}

\item Do you need all summary statistics? And if not, which ones are sufficient?\sol{Actually, the sample mean and the same variance are sufficient statistics, and hence by only using those you should get better estimates, as the other statistics are just adding noise.}

\end{enumerate}

\end{enumerate}

\end{document}
