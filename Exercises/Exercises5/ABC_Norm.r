#arguments
m <- 20;
N <- 100000;
tolerance <- 0.01;
true.mu <- 0;
true.sigma2 <- 2;

#function to calc summary statistics
calc.stats <- function (x){
  S <- c(mean(x), var(x), median(x), range(x), max(x)-min(x));
  names(S) <- c("mean", "var", "median", "min", "max", "range")
  return(S[1:2]);
}

#functions to generate simulations
one.sim <- function(x, m){
  return(calc.stats(rnorm(m, mean = x[1], sd=sqrt(x[2]))));
}

make.sim <- function(N, m){
  P <- data.frame(mu=runif(N, min=-10, max=10), sigma2=runif(N, min=0 , max=10));
  S <- t(apply(P, 1, one.sim, m=m));
  return(data.frame(cbind(P,S)));
}

#function to get posterior samples
get.posterior.samples <- function(Sims, S.obs, tolerance){
  #standardize stats
  nStats <- length(S.obs);
  nParams <- dim(Sims)[2] - nStats;
  Sims.stats <- Sims[,(nParams+1):dim(Sims)[2]];
  for(i in 1:nStats){
    mean <- mean(Sims[,i]);
    sd <- sd(Sims[,i]);
    Sims.stats[,i] <- (Sims[,i] - mean) / sd;
    S.obs[i] <- (S.obs[i] - mean) / sd;
  }
  
  #calc euclidian dist
  x <- t(Sims.stats) - S.obs;
  distances <- sqrt(colSums(x*x));
  plot(density(distances), xlim=c(0, max(distances)+1));
  
  #do rejection
  distances.sorted <- sort(distances, index.return=T);
  bestSims <- distances.sorted$ix[1:round(tolerance*dim(Sims)[1])];
  return(Sims[bestSims,]);
}

#function to plot posterior distributions
plot.posterior <- function(Post, true.mu, true.sigma2, observed.mean, observed.sigma2, m){
  par(mfrow=c(1,2), mar=c(4.2,4,1.2,0.2))
  #mu
  x <- seq(-10,10, length.out=100);
  plot(x, dnorm(x,mean=observed.mean, sd=sqrt(observed.sigma2/m)), type='l', main="mu", xlab="mu", ylab="Posterior density");
  lines(rep(true.mu,2), par("usr")[3:4], lty=2);
  lines(density(Post$mu), main="mu", col='red');
  
  #sigma2
  x <- seq(0,10, length.out=100);
  plot(x, (m-1)/observed.sigma2 * dchisq(x*(m-1)/observed.sigma2, m-1), main="sigma2", type='l', xlab="sigma2", ylab="Posterior density");
  lines(rep(true.sigma2,2), par("usr")[3:4], lty=2)
  lines(density(Post$sigma2), xlim=c(0,4), col="red");
}


#generate simulations
Sims <- make.sim(N, m);

#generate observed data
S.obs <- one.sim(c(true.mu, true.sigma2), m);

#run ABC
Post <- get.posterior.samples(Sims, S.obs, tolerance);
#pdf("/data/posteriors.pdf", width=8, height=6);
plot.posterior(Post, true.mu, true.sigma2, S.obs[1], S.obs[2], m);
#dev.off();


