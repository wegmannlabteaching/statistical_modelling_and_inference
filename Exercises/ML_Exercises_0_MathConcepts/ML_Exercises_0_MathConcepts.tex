\documentclass{../exercises}
\usepackage{cancel}

\exerciseTitle{0}{Refresher on Probabilities \& Analysis}



\begin{document}

\maketitle

\subsection{Probability theory}

Remember the following key rules when manipulating probabilities:

\begin{enumerate}[P1)]
 \item We denote by $\P(A = a)$ the \emph{probability} that the random variable $A$ takes on event $a$. If obvious from the context, we may use the shorthand $\P(a)=\P(A=a)$ or denote by $\P(A)$ the probability of any event $A$ (see below).
 
 
 We will also use the shorthand $\P(x)=\P(X=x)$ and denote by $\P(X)$ the probability of any value of $X$ (see below).
 \item For a random variable $A=\{a_1, a_2, \ldots, a_k\}$ that takes on events $a_1, \ldots, a_k$, the probability that either event $a_1$ or event $a_2$ takes place is given by $\P(A=a_1)+\P(A=a_2)$, or $\P(a_1)+\P(a_2)$ for short. Or more generally, the probability that any of the events in a subset $B\subseteq A$ take place is given by $\sum_{b \in B} \P(A=b)$ or $\sum_{b \in B} \P(b)$ for short. Of course, $\sum_{a\in A}\P(A=a)=1$, which we may also write as $\sum_A\P(A)=1$.
 
 \item We denote by $\P(A=a|B=b)$ the conditional probability that $A$ takes on event $a$ given that $B$ took on event $b$. When making general statements, we may simply write $\P(A|B)$. If $\P(A|B)=\P(A)$ then $A$ is independent of $B$, denoted by $A \perp B$. \label{P_conditional}
 
 \item Note that $\sum_A \P(A|B) = 1$, but that $\sum_B \P(A|B)$ is not meaningful in most cases: it gives the sum of the probability of $A$ given all possible states of $B$, which is not a probability anymore. To illustrate that, consider a case in which $A$ always takes on event $a$ and hence $\P(A=a|B)=1$ for any $B$, in which case the aforementioned sum is larger than 1.
  
 \item We denote by $\P(A=a,B=b)$ the joint probability that $A$ takes on event $a$ and $B$ takes on event $b$. Note that $\P(A,B)=\P(B,A)$ and hence the order is of no importance.
 
 \item Joint probabilities can be factorized as $\P(A,B)=\P(A)\P(B|A)$ such that the joint probability of $A$ and $B$ is given by the probability of $A$ times the probability of $B$ given $A$. Since $\P(A,B)=\P(B,A)$, an alternative factorization is $\P(A,B)=\P(B)\P(A|B)$. This concept is easily extended to the joint probability of arbitrarily many random variables, with even more possible factorizations. The joint probability $\P(A,B,C)$ may for instance be factorized as $\P(A,B,C)=\P(A)\P(B|A)\P(C|A,B)$, but also as $\P(A,B,C)=\P(C)\P(A|C)\P(B|A,C)$.
 
 \item We can use the above to infer Bayes formula: Since $\P(A,B)=\P(A)\P(B|A)=\P(B)\P(A|B)$, we get by simple re-arrangement
 \begin{equation*}
  \P(A|B)=\frac{\P(B|A)\P(A)}{\P(B)}.
 \end{equation*}
 
 \item All the above concepts are equally valid for continuous random variables: We denote by $\P(X=x)$ the \emph{probability density} that a continuous random variable $X$ takes on value $x$ (which is sometimes denoted by $\pi(X=x)$ in the literature to indicate the continuous nature of $X$). Note that for a continuous random variable with infinitely many outcomes, the probability of any specific value $x$ is 0. However, the probability density $\P(x)$ may be non-zero, and so does the probability $\P(l\leq x\leq u)=\int_l^u \P(X=x)dx$ give the probability that $X$ takes on a value within the interval $[l,u]$. Of course, $\int_{-\infty}^{+\infty} \P(X=x)dx=1$, which we may also write as $\int \P(x)dx=1$.
  
 \item We will denote by $X\sim D(\theta)$ that the random variable $X$ follows a particular distribution $D$ with parameters $\theta$. As an example, $y\sim \N(\mu, \sigma^2)$ indicates that $y$ is normally distributed with mean $\mu$ and variance $\sigma^2$. The probability density of a particular value $x$ would then be denoted by $\P(X=x|\mu,\sigma^2)$.
 
\end{enumerate}

\subsubsection{Exercises}

\begin{enumerate}[1.]
 \item Let $A$ and $B$ denote two events that occur with probabilities $\P(A)=0.3$ and $P(B)=0.5$, respectively, where $\overline{A}$ and $\overline{B}$ denote the alternative outcomes of not $A$ and not $B$, respectively.
 \begin{enumerate}[a)]
 \item What is the probability $\P(A,B)$ under the assumption of independence, i.e. if $A \perp B$?
 
 \sol{Under the assumption of independence, $\P(A,B)=\P(A)\P(B)=0.15$.}

 \item What is $\P(A|B) + \P(\overline{A}|B)$? Make sure to use P\ref{P_conditional}.
 
 \sol{$\P(A|B) + \P(\overline{A}|B) = 1$ as it must be either $A$ or $\overline{A}$.}
 
  \item What is $\P(\overline{B})$?
 
 \sol{$\P(\overline{B}) = 1 - \P(B) = 0.5$.}
 
 \item If $\P(A|B)=0.3$, are $A \perp B$?
 
 \sol{Yes, $A \perp B$ since $\P(A|B)=\P(A)$.}
 
 \item If $\P(B|A)=0.4$, are $A \perp B$?
 
 \sol{No, $A \not\perp B$ since $\P(B|A)\neq\P(B)$.}
 
 \item If $\P(A,B) = 0.4$, what is $\P(A|B)$?
 
 \sol{Since $\P(A,B)=\P(B)\P(A|B)$, we have $\P(A|B) = \frac{\P(A,B)}{\P(B)} = \frac{0.4}{0.5} = 0.8$.}

 \item What is $\P(B|A) + \P(B|\overline{A})$?
 
 \sol{While algebraically correct, $\P(B|A) + \P(B|\overline{A})$ is not a meaningful quantity to calculate.}
 
 \end{enumerate}


\item Consider the joint probability $\P(A,B,C,D,E)$.
\begin{enumerate}[a)]
    \item How many different full factorizations are possible?
    
    \sol{There are 120 possible factorizations}
    
    \item Provide three different factorizations that don't share any terms.

    \sol{Examples: \\$\P(A)\P(B|A)\P(C|A,B)\P(D|A,B,C)\P(E|A,B,C,D)$,\\ $\P(B)\P(C|B)\P(D|B,C)\P(E|B,C,D)\P(A|B,C,D,E)$ and \\$\P(C)\P(D|C)\P(E|C,D)\P(A|C,D,E)\P(B|A,C,D,E)$}
    
    \item Provide a solution to calculate $\P(A|B,C,D,E)$ from $\P(B|A,C,D,E)$.
    
    \sol{$\P(A|B,C,D,E) = \frac{\P(B|A,C,D,E)\P(A|C,D,E)}{\P(B|C,D,E)}$.}
    
 \end{enumerate}

 
 \item Consider a normally distributed random variable $X\sim\N(\mu, \sigma^2)$.
\begin{enumerate}[a)]
\item What is the probability $\P(X<\mu|\mu, \sigma^2)$?

\sol{The normal distribution is symmetric around then mean $\mu$, and hence $\P(X<\mu|\mu, \sigma^2)=0.5$.}

\item Is the probability density $\P(X=\mu|\mu, \sigma^2)$ larger or smaller than $\P(X=\mu|\mu, \frac{\sigma^2}{2})$?

\sol{When shrinking the variance, the probability density gets higher at the mean. Hence, $\P(X=\mu|\mu, \sigma^2) < \P(X=\mu|\mu, \frac{\sigma^2}{2})$.}

\item If $Y\sim\N(\mu,\sigma^2)$ was also distributed normally with the same mean and variance $\mu$ and $\sigma^2$, respectively, what is the probability that $\P(X=Y)$ and that $\P(X<Y)$?

\sol{As these are continuous random variables, the probability that they take on the same value is zero, i.e. $\P(X=Y)=0$. As both are distributed equally, $\P(X<Y)=0.5$.}

\end{enumerate}

\end{enumerate}

\clearpage
\subsection{Working with Logarithms}

Remember the following key rules when manipulating probabilities:

\begin{enumerate}[L1)]
 \item We denote by $\log x$ the natural logarithm of $x$ and by $\log_a x$ the logarithm of $x$ to base $a$. We prefer the shorter form $\log x$ over $\log(x)$ except if the former is unclear. For instance, $\log x + y \neq \log(x+y)$.
 \item We denote by $e^{x}$ the Euler number $e$ to the power of $x$. If needed, this may also be written as $\exp(x)$. Obviously, $\log\left(e^{x}\right) = e^{\log x}=x$.
 \label{L_euler}
 \item $\log ab=\log a +\log b $ and analogously $\log\left(\frac{a}{b}\right)=\log(a)-\log(b)$. Hence, $\log \prod_A \P(A) = \sum_A \log \P(A)$.
 \label{L_product}
 \item $\log\left(a^b\right)=b\log(a)$, from which we see that $\log\left(\frac{a}{b}\right)=\log\left(ab^{-1}\right)=\log(a)+\log\left(b^{-1}\right)=\log(a)-\log(b)$.
 \label{L_power}
 \item $\log \sqrt[p]{x} = \frac{\log x}{p}$, because a root can be written as $\sqrt[p]{x} = x^{\frac{1}{p}}$. The logarithm of the square root thus is $\log \sqrt{x} = \frac{\log x}{2}$.
 \label{L_sqrt}
 \item $\prod_i e^{x_i}=\exp\left(\sum_i x_i\right)$.
\end{enumerate}

\subsubsection{Exercises} 


\begin{enumerate}[1.]
 \item 
 \begin{enumerate}[a)]
  \item Condense $6\log a + 9 \log b$ into a single logarithm.
 \sol{
 \begin{center}
  $6\log a + 9 \log b = \log a^6 + \log b^9 = \log a^6 b^9$ 
 \end{center}
 First we applied L\ref{L_power} and then combined the two logarithms using L\ref{L_product}.  
 }
  
  \item Simplify $\log(x^2+2x+1)-2\log(x+1)$
  \sol{
  \begin{center}
   $\log(x^2+2x+1)-2\log(x+1) = \log(x+1)^2 - 2\log(x+1) = 2\log(x+1) - 2\log(x+1) = 0$
  \end{center}
  We simplify $(x^2+2x+1) = (x+1)^2$, where we can then apply L\ref{L_power}.
  }
  
  \item Simplify $1-\frac{\log(x^2)}{3\log(x)}$ into a single logarithm.
  \sol{
  \begin{center}
  	$1-\frac{\log(x^2)}{3\log(x)} = 1 - \frac{2\log(x)}{3\log(x)} = 1 -\frac{2}{3} = \frac{1}{3} $
  \end{center}
  We first transformed 1 into a fraction of the denominator of the second term. With this we could merge the two fractions. Subsequently, we applied L\ref{L_power}. 
  
  }
  
  \item Expand and if possible simplify $\log(5\sqrt{xy})$ to have only logarithms of a single variable.
  \sol{
  \begin{center}
   $\log(5\sqrt{xy}) = \log 5 + \log \sqrt{xy} = \log 5 + \log (xy)^\frac{1}{2} = \log 5 + \frac{1}{2}\log x + \frac{1}{2}\log y$
  \end{center}
  
  First we expand using L\ref{L_product} after which we can transfrorm the second part of the equation using L\ref{L_sqrt}. Subsequently we use L\ref{L_power} and expand again as in the first step with L\ref{L_product}. 
  }
  
  
  \item Simplify $\log \prod_{i=1}^I \left(\frac{x_i^b}{ab}\right)$ to have only logarithms of single variables.
  \sol{
  \begin{center}
    $\log \prod_{i=1}^I \left(\frac{x_i^b}{ab}\right) = \sum_{i=1}^I \log \left(\frac{x_i^b}{ab}\right) 
    = \sum_{i=1}^I [ \log x_i^b - \log \left(ab\right)]
    = \sum_{i=1}^I \log x_i^b - \sum_{i=1}^I \log \left(ab\right)$
  \end{center}
    First we applied L\ref{L_product}. In the next step we used L\ref{L_power}, followed by $\sum_A [u-z] = \sum_A u - \sum_A z$. This transformation allows us to solve each part of the equation seperately. 
    \\ We start by solving the first part $\sum_{i=1}^I \log x_i^b$ :
  \begin{center}
    $\sum_{i=1}^I \log x_i^b = \sum_{i=1}^I b\log x_i = b\sum_{i=1}^I \log x_i$
  \end{center}
    We applied L\ref{L_power}, which enabled us to move b in front of the sum due to b being a constant. 
    \\We now solve the second part of the equation $-\sum_{i=1}^I \log \left(ab\right)$:
  \begin{center}
    $-\sum_{i=1}^I \log \left(ab\right) = -\sum_{i=1}^I [\log a + \log b] = -I (\log a + \log b )$
  \end{center}
    We applied L\ref{L_product}. As both $\log a$ and $\log b$ are constants indepentant of $i$, we applied $\sum_{n=1}^N u = Nu$. With both parts of the equation sufficiently simplified we can now state the full solution as:
    
   \begin{center}
    $ \log \prod_{i=1}^I \left(\frac{x_i^b}{ab}\right) = b\sum_{i=1}^I \log x_i - I (\log a + \log b )$
  \end{center}
  }
  
  \item Simplify $\exp(a\log(x+b))$.
  
  \sol{
  \begin{center}
   $\exp(a\log(x+b))=\exp(\log(x+b)^a)=(x+b)^a$
  \end{center}
  First we applied L\ref{L_power}. Subsequently $\exp$ and $\log$ cancel out in regards to L\ref{L_euler}.
  }
  
  \item Simplify $\sum_{i=1}^{I} \log \frac{i}{i+1}$.
  
  \sol{
  \begin{equation*}
   \sum_{i=1}^{I} \log \frac{i}{i+1} = \log \prod_{i=1}^I \frac{i}{i+1} = \log \left(\frac{1}{2} \cdot \frac{2}{3} \cdot \frac{3}{4} \cot \ldots \cdot \frac{I-1}{I} \cdot \frac{I}{I+1}\right) = \log \frac{1}{I+1}.
  \end{equation*}
   Note that in the last step the nominator of one term always cancels out with the denominaztor of the previous term, except for the firts and last. The result could further be simplified to
  \begin{equation*}
   \log\frac{1}{I+1} = \log 1 - \log (I+1) = - \log (I+1)
  \end{equation*}
    We applied L\ref{L_power} and the fact that $\log 1 = 0$.
  }

  
 \end{enumerate}

\end{enumerate}

\clearpage
\subsection{Analysis}

Remember the following key rules when working with derivatives:

\begin{enumerate}[D1)]

 \item We denote by $\frac{\dd}{\dd x}f(x)$ the derivative of the function $f(x)$ is respect to $x$. The second derivative in respect to $x$ is denoted by $\frac{\dd^2}{\dd^2 x}f(x)$.
 
 \item The derivative of a constant is $\frac{\dd}{\dd x} a = 0$. \label{D_const}
 
 \item The power rule: $\frac{\dd}{\dd x} ax^b = abx^{b-1}$, for instance, $\frac{\dd}{\dd x} 3x^2 = 6x$.\label{D_power}
 
 \item The summation rule: $\frac{\dd}{\dd x}\left [f(x) + g(x)\right] = \frac{\dd}{\dd x}f(x) + \frac{\dd}{\dd x}g(x)$.
 
 \item The product rule: $\frac{\dd}{\dd x}f(x)g(x) = g(x)\frac{\dd}{\dd x}f(x) + f(x) \frac{\dd}{\dd x}g(x)$.
 
 \item By applying the product and chain rule to $\frac{\dd}{\dd x}\frac{f(x)}{g(x)} = \frac{\dd}{\dd x}f(x)g(x)^{-1}$,\\ we get the quotient rule: $\frac{\dd}{\dd x}\frac{f(x)}{g(x)} = \frac{g(x)\frac{\dd}{\dd x}f(x) - f(x) \frac{\dd}{\dd x}g(x)}{g(x)^2}$.
 
 \item The chain rule: $\frac{\dd}{\dd x}f(g(x)) = \frac{\dd}{\dd g(x)}f(g(x)) \frac{\dd}{\dd x}g(x)$.\label{D_chain}
 
 \item The derivative of the logarithm is $\frac{\dd}{\dd x} \log x = \frac{1}{x}$. By chain rule we get $\frac{\dd}{\dd x} \log x^2 = \frac{2x}{x^2} = \frac{2}{x}$.
 \label{D_log}
 
 \item The derivative of the exponential $\frac{\dd}{\dd x} e^x = e^x$. By chain rule we get $\frac{\dd}{\dd x} e^{x^2} = 2xe^{x^2}$.
 
 
\end{enumerate}



\begin{enumerate}[1.]
 \item Consider the binomial probability density $\P(n|N,p)={N\choose n}p^n (1-p)^{N-n}$.
 
 \begin{enumerate}[a)]
  \item Determine $\log \P(n|N,p)$.
  
  \sol{
  \begin{center}
   $\log \P(n|N,p) = \log \left({N\choose n}p^n (1-p)^{N-n}\right) = \log {N\choose n} + \log p^n + \log (1-p)^{N-n}$
  \end{center}
  We fist apply L\ref{L_product} and subsequently L\ref{L_power}, resulting in the following solution:
  \begin{center}
   $\log \P(n|N,p) = \log {N\choose n} + n\log p + (N-n)\log (1-p)$
  \end{center}

  }
  
  
  \item Determine the derivative $\frac{\dd}{\dd p} \log \P(n|N,p)$.
  
  \sol{
  \begin{center}
  $\frac{\dd}{\dd p} \log \P(n|N,p) = 0 + \frac{n}{p} - \frac{N-n}{1-p} = \frac{n}{p} - \frac{N-n}{1-p}$ 
  \end{center}
  First, $\frac{\dd}{\dd p} \log {N\choose n} = 0$. We apply D\ref{D_log} to the rest of the function.
}
  
  
 \end{enumerate}


 \item Consider the normal probability density function $\P(x|\mu,\sigma^2)=\frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(\mu-x)^2}{2\sigma^2}\right)$
 \begin{enumerate}[a)]
 \item Determine the $\log \P(x|\mu,\sigma^2)$.
 
 \sol{
 \begin{center}
  $\log \P(x|\mu,\sigma^2) = \log \left( \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(\mu-x)^2}{2\sigma^2}\right)\right) = \log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right) + \log \left( \exp\left(-\frac{(\mu-x)^2}{2\sigma^2}\right)\right)$
 \end{center}
 First we apply L\ref{L_product}. This allows us to solve the two parts of the equation seperately. We begin by solving the first part $\log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right)$ :
 \begin{center}
  $\log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right) = \log(1) - \log \left(\sqrt{2\pi\sigma^2}\right) = \log 1 - \frac{1}{2}\log(2\pi\sigma^2) = - \frac{1}{2}\log(2\pi\sigma^2)$  
 \end{center}
 We expand the function by applying L\ref{L_power}. We can further transfom it with L\ref{L_sqrt} and L\ref{L_power}. With the first part solved, we can now continue by solving the second part $\log \left( \exp\left(-\frac{(\mu-x)^2}{2\sigma^2}\right)\right)$:  
 \begin{center}
  $\log \left( \exp\left(-\frac{(\mu-x)^2}{2\sigma^2}\right)\right) = -\frac{(\mu-x)^2}{2\sigma^2}$
 \end{center}
 We simplify by applying L\ref{L_euler}. With both parts of the function solved, we now have the solution:

 \begin{center}
  $\log \P(x|\mu,\sigma^2) = -\frac{1}{2}\log 2\pi\sigma^2 - \frac{(\mu-x)^2}{2\sigma^2}$
 \end{center}
 }
  
 \item Determine the derivative $\frac{\dd}{\dd \mu}\log\P(x|\mu,\sigma^2)$.
 
 \sol{
 \begin{center}
  $\frac{\dd}{\dd \mu}\log\P(x|\mu,\sigma^2) = \frac{\dd}{\dd \mu} \left( -\frac{1}{2}\log 2\pi\sigma^2 - \frac{(\mu-x)^2}{2\sigma^2}\right)= \frac{\dd}{\dd \mu} \left( -\frac{1}{2}\log 2\pi\sigma^2\right) - \frac{\dd}{\dd \mu} \left( \frac{(\mu-x)^2}{2\sigma^2}\right)$
 \end{center}
 We simplify the problem by seperating the formula into two parts. Now we start by solving the first part $\frac{\dd}{\dd \mu} \left( -\frac{1}{2}\log 2\pi\sigma^2\right)$:
 \begin{center}
  $\frac{\dd}{\dd \mu} \left( -\frac{1}{2}\log 2\pi\sigma^2\right) = 0$
 \end{center}
    As there is no $\mu$ to derive from we treat this part as a constant, Hence, we refer to D\ref{D_const}. Now we can solve the second part of the formula $- \frac{\dd}{\dd \mu} \left( \frac{(\mu-x)^2}{2\sigma^2}\right)$:
 \begin{center}
  $- \frac{\dd}{\dd \mu} \left( \frac{(\mu-x)^2}{2\sigma^2}\right) = - \frac{1}{2\sigma^2}\frac{\dd}{\dd \mu} \left((\mu-x)^2\right)$
 \end{center}
 First we factor out the constant $\frac{1}{2\sigma^2}$. We can now take the derivative  $\frac{\dd}{\dd \mu} \left((\mu-x)^2\right)$ using D\ref{D_chain}.  
 \begin{center}
  $ \frac{\dd}{\dd \mu} \left((\mu-x)^2\right) = 2(\mu-x)\left(\frac{\dd}{\dd \mu}\mu-\frac{\dd}{\dd \mu}x\right) = 2(\mu-x) (1 - 0) = 2(\mu-x)$ 
 \end{center}
 The solution for the second part is:
 \begin{center}
  $-\frac{1}{\cancel{2}\sigma^2}\cancel{2}(\mu-x)= -\frac{1}{\sigma^2}(\mu-x)$
 \end{center}

 We can now combine the solved parts of the formula and get the solution:
 \begin{center}
  $\frac{\dd}{\dd \mu}\log\P(x|\mu,\sigma^2) =0-\frac{1}{\sigma^2}(\mu-x)= \frac{1}{\sigma^2}(x-\mu)$
 \end{center}
 }
 
 \item Determine the derivative $\frac{\dd}{\dd \sigma^2}\log\P(x|\mu,\sigma^2)$.
 
 \sol{
 \begin{center}
  $\frac{\dd}{\dd \sigma^2}\log\P(x|\mu,\sigma^2) = \frac{\dd}{\dd \sigma^2}\left(-\frac{1}{2}\log 2\pi\sigma^2 - \frac{(\mu-x)^2}{2\sigma^2}\right)=  \frac{\dd}{\dd \sigma^2}\left(-\frac{1}{2}\log 2\pi\sigma^2\right) -  \frac{\dd}{\dd \sigma^2}\left(\frac{(\mu-x)^2}{2\sigma^2}\right)$
 \end{center}
 We simplify the problem by seperating the formula into two parts. Now we start by solving the first part $\frac{\dd}{\dd \sigma^2}\left(-\frac{1}{2}\log 2\pi\sigma^2\right)$:
 \begin{center}
  $\frac{\dd}{\dd \sigma^2}\left(-\frac{1}{2}\log 2\pi\sigma^2\right) = -\frac{1}{2}\frac{\dd}{\dd \sigma^2}\left(\log 2\pi\sigma^2\right)$
 \end{center}
 After factoring out the constants, we can now derive for $\frac{\dd}{\dd \sigma^2}\left(\log 2\pi\sigma^2\right)$ using D\ref{D_chain}:
 \begin{center}
   $\frac{\dd}{\dd \sigma^2}\left(\log 2\pi\sigma^2\right) = \frac{1}{2\pi\sigma^2}\frac{\dd}{\dd \sigma^2}(2\pi\sigma^2) = \frac{1}{\cancel{2\pi}\sigma^2} \cancel{2\pi} = \frac{1}{\sigma^2} $
 \end{center}
 the solution for the first part of the formula is:
 \begin{center}
  $-\frac{1}{2}\frac{1}{\sigma^2} = - \frac{1}{2 \sigma^2}$
 \end{center}
 We now solve the second part of the formula $- \frac{\dd}{\dd \sigma^2}\left(\frac{(\mu-x)^2}{2\sigma^2}\right)$ by deriving $\frac{\dd}{\dd \sigma^2}\frac{1}{2\sigma^2}$ using D\ref{D_power} , with $(\mu-x)^2$ being a constant.
 \begin{center}
  $-  \frac{\dd}{\dd \sigma^2}\left(\frac{(\mu-x)^2}{2\sigma^2}\right) = -\frac{(\mu-x)^2}{-1*2(\sigma^2)^2}= \frac{(\mu-x)^2}{2(\sigma^2)^2}$
 \end{center}
 By adding the two solved parts back togehter we can factor out $\frac{1}{2\sigma^2}$ and get to the follwoing solution:
 \begin{center}
    $\frac{\dd}{\dd \sigma^2}\log\P(x|\mu,\sigma^2) = -\frac{1}{2\sigma^2} + \frac{(\mu-x)^2}{2\left(\sigma^2\right)^2} = \frac{1}{2\sigma^2}\left(\frac{(\mu-x)^2}{\sigma^2} - 1\right)$
 \end{center}
 
}

 
 \item Determine $\log \prod_{i=1}^I \P(x_i|\mu,\sigma^2)$.
 
 \sol{
 \begin{center}
  $\log \prod_{i=1}^I \P(x_i|\mu,\sigma^2) = \log \prod_{i=1}^I \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(\mu-x_i)^2}{2\sigma^2}\right) = \sum_{i=1}^I \log \frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(\mu-x_i)^2}{2\sigma^2}\right)$
 \end{center}
 \begin{center}
  $= \sum_{i=1}^I \log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right)+\sum_{i=1}^I \log \left(\exp\left(-\frac{(\mu-x_i)^2}{2\sigma^2}\right)\right)$
 \end{center}
 We can now solve the two parts of the function individually, starting witht the first $\sum_{i=1}^I \log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right)$:
 \begin{center}
  $\sum_{i=1}^I \log \left(\frac{1}{\sqrt{2\pi\sigma^2}}\right) = \sum_{i=1}^I \left[ \log 1 - \log \sqrt{2\pi\sigma^2}\right] = \sum_{i=1}^I \left[ 0 - \log(2\pi\sigma^2)^\frac{1}{2}\right] = \sum_{i=1}^I - \frac{1}{2} \log(2\pi\sigma^2) $
 \end{center}
 \begin{center}
  $= - \frac{1}{2} \sum_{i=1}^I \log(2\pi\sigma^2) = - \frac{1}{2} I \log(2\pi\sigma^2) = - \frac{I}{2} \log(2\pi\sigma^2) $
 \end{center}
 First we apply L\ref{L_power}. $\log 1 = 0$ We now continue by solving the second part $\sum_{i=1}^I \log \left(\exp\left(-\frac{(\mu-x_i)^2}{2\sigma^2}\right)\right)$:
 \begin{center}
  $\sum_{i=1}^I \log \left(\exp\left(-\frac{(\mu-x_i)^2}{2\sigma^2}\right)\right) = - \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}$
 \end{center}
 We have applied L\ref{L_euler}. Subsequently, we add the two parts together which gives us the following solution:
 \begin{center}
  $\log \prod_{i=1}^I \P(x_i|\mu,\sigma^2) = -\frac{I}{2}\log 2\pi\sigma^2 - \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}$
 \end{center}
}
 
 \item Determine the derivative $\frac{\dd}{\dd \mu} \log \prod_{i=1}^I \P(x_i|\mu,\sigma^2)$?
 
 \sol{
 \begin{center}
  $\frac{\dd}{\dd \mu} \log \prod_{i=1}^I \P(x_i|\mu,\sigma^2) = \frac{\dd}{\dd \mu} \left(-\frac{I}{2}\log 2\pi\sigma^2 - \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}\right) =  \frac{\dd}{\dd \mu} \left(-\frac{I}{2}\log 2\pi\sigma^2 \right) - \frac{\dd}{\dd \mu} \left( \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}\right) $
 \end{center}
 First we separated the formula into two parts, in order to derive them seperately. We begin with the first part $\frac{\dd}{\dd \mu} \left(-\frac{I}{2}\log 2\pi\sigma^2 \right)$:
 \begin{center}
  $\frac{\dd}{\dd \mu} \left(-\frac{I}{2}\log 2\pi\sigma^2 \right) = 0$ 
 \end{center}
 As the whole term is a constant, we make use of D\ref{D_const}. We continue by solving the second part $- \frac{\dd}{\dd \mu} \left( \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}\right) $: 
 \begin{center}
  $- \frac{\dd}{\dd \mu} \left( \sum_{i=1}^I \frac{(\mu-x_i)^2}{2\sigma^2}\right) = -\frac{1}{2\sigma^2} \sum_{i=1}^I 2(\mu-x_i) \left(\frac{\dd}{\dd \mu} \mu -\frac{\dd}{\dd \mu} x_i \right) = - \frac{1}{2\sigma^2} \sum_{i=1}^I 2(\mu-x_i) *(1-0) $
 \end{center}
 We factor out the constants from the sum. We can then separate the sums and solve $\sum_{i=1}^I \mu$ as it is a constant independant of $i$.  
 \begin{center}
  $- \frac{\cancel{2}}{\cancel{2}\sigma^2 } \left(\sum_{i=1}^I \mu - \sum_{i=1}^I x_i \right)= -\frac{1}{\sigma^2} \left(I\mu - \sum_{i=1}^I x_i\right) $
 \end{center}
With both parts of the equation solved, we can add them back together and get the following solution:
 \begin{center}
   $\frac{\dd}{\dd \mu} \log \prod_{i=1}^I \P(x_i|\mu,\sigma^2) = 0 - \frac{1}{\sigma^2} \left(I\mu - \sum_{i=1}^I x_i\right) = \frac{1}{\sigma^2} \left(\sum_{i=1}^I x_i - I\mu \right)$
 \end{center}
}
 \end{enumerate}
\end{enumerate}


\end{document}
