\documentclass{../exercises}

\exerciseTitle{9}{Metropolis Hastings Algorithm (MCMC) 2}


\def\mmu{\boldsymbol{\mu}}
\def\f{\boldsymbol{f}}
\def\g{\boldsymbol{g}}

%\def\L{{\cal L}}

\begin{document}

\maketitle


\begin{enumerate}[1.]

\item Consider a sample of genotypes $g_{il}=0,1,2$ estimated at $l=1, \ldots, L$ loci for $i=1, \ldots, n$ individuals sampled from a randomly mating population in Hardy-Weinberg equilibrium. The observed genotypes $g_{il}$ may differ from the true underlying genotypes $\gamma_{il}=0,1,2$ due to some genotyping errors that occur at rate $\epsilon$ such that 

\begin{equation*}
 \P(g |\gamma, \epsilon) = \begin{cases}
                            1-\epsilon & \quad \mbox{if  $g=\gamma$}\\
                            \frac{\epsilon}{2} & \quad \mbox{if  $g\neq\gamma$}
                           \end{cases}.
\end{equation*}

Since the population is in Hardy-Weinberg equilibrium, we have 
\begin{equation*}
 \P(\gamma_{il}|f_l) = \binom{2}{\gamma_{il}} f_l^{\gamma_{il}} (1-f_l)^{2-\gamma_{il}} = \begin{cases}
                                                   (1-f_l)^2 & \quad \mbox{if $\gamma_{il}=0$}\\
                                                   2f_l(1-f_l) & \quad \mbox{if $\gamma_{il}=1$}\\
                                                   f_l^2 & \quad \mbox{if $\gamma_{il}=2$}\\
                                                  \end{cases},
\end{equation*}

where $f_l$ is the unknown allele frequency at locus $l$. Let us finally assume that the allele frequencies are Beta distributed with shape parameter $a$ as

\begin{equation*}
 f_l \sim \mathrm{Beta}(a,a).
\end{equation*}

The goal is to infer the error rate $\epsilon$, the shape parameter $a$ as well as all $f_l$ in a Bayesian setting by means of a Metropolis-Hastings algorithm. Let us assume a Beta prior distribution on $\epsilon$ with a peak at zero and an exponential prior distribution on $a$:
\begin{equation*}
 \epsilon \sim \mathrm{Beta}(1, \beta), \quad a \sim \mathrm{Exp}(\lambda).
\end{equation*}


(Model inspired by Bresadola \textit{et al.} 2020, Molecular Ecology Resources).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{enumerate}[a)]
\item Draw a DAG of this model.

\sol{\begin{figure}[!h]\centering\begin{tikzpicture}
            \node[latent] (a) {$a$};
            \node[latent, right = of a] (f) {$f_l$};
            \node[latent, right=of f] (gamma) {$\gamma_{il}$};
            \node[obs, right=of gamma] (g) {$g_{il}$};
            \node[latent] at (5.45,2) (e) {$\epsilon$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate1} {(gamma) (g)} {$i=1,\ldots,n$};
            \plate[inner sep=0.3cm,yshift=0.2cm] {plate2} {(f) (plate1)} {$l=1,\ldots,L$};
            \edge {a} {f};
            \edge {f} {gamma};
            \edge {gamma} {g};
            \edge {e} {g};
            \end{tikzpicture}\end{figure}}


\item We will be interested in the posterior distribution $\P(\epsilon, a, \f|\g) \propto \P(\g|\epsilon, a, \f)\P(\epsilon, a, \f)$, where $\g=(g_{11}, \ldots, g_{1L}, \ldots, g_{nL})$ is the vector of all genotypes observed. Write out the likelihood $\P(\g|\epsilon, a, \f)$, the prior $\P(\epsilon, a, \f)$ and the posterior density $\P(\epsilon, a, \f|\g)$ (up to the proportionality constant) of this model by integrating out the true genotypes $\gamma_{il}$ and using the probability densities $\P(g|\gamma,\epsilon)$, $\P(\gamma|f)$, $\P(f|a)$, $\P(\epsilon|\beta)$ and $\P(a|\lambda)$.

\sol{\begin{eqnarray*}
\P(\g | \epsilon, \f, a )&=&\P(\g | \epsilon, \f) = \prod_{l=1}^L \prod_{i=1}^n \sum_{\gamma = 0}^2 \P(g_{il}|\gamma,\epsilon) \P(\gamma|f_l)\\
\P(\epsilon, a, \f) &=& \P(\epsilon)\P(a)\prod_{l=1}^L \P(\f_l|a)\\
\P(\epsilon, a, \f| \g) &\propto& \P(\epsilon)\P(a) \prod_{l=1}^L \P(\f_l|a) \prod_{i=1}^n \sum_{\gamma = 0}^2 \P(g_{il}|\gamma,\epsilon) \P(\gamma|f_l)
\end{eqnarray*}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Propose symmetric proposal kernels for all single-parameter updates, i.e. for updating $\epsilon$, $a$ and each $f_l$ individually.

\sol{\begin{equation*}
    q(f_l,f_l') \sim {\cal N}(f, \sigma_f^2), \quad q(\epsilon,\epsilon') \sim {\cal N}(\epsilon, \sigma_\epsilon^2), \quad q(a,a') \sim {\cal N}(a, \sigma_a^2),
\end{equation*}
mirrored at 0 and 1 to respect the interval $[0,1]$ for $f_l$ and $\epsilon$ and mirrored at 0 to respect the interval $(0,\infty)$ for $a$.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Write out the simplified Hastings-ratio for single-parameter updates, assuming the symmetric proposal kernels proposed above and the probability densities $\P(g|\gamma,\epsilon)$, $\P(\gamma|f)$, $\P(f|a)$, $\P(\epsilon|\beta)$ and $\P(a|\lambda)$.

\sol{Parameter $\epsilon$:
\begin{eqnarray*}
h_\epsilon &=& \frac{\P(\epsilon')}{\P(\epsilon)}\prod_{l=1}^L \prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma, \epsilon')\P(\gamma|f_l)}{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma, \epsilon) \P(\gamma|f_l)}.\\
\end{eqnarray*}
Parameter $a$:
\begin{equation*}
h_a = \frac{\P(a')}{\P(a)}\prod_{l=1}^L \frac{\P(f_{l}|a')}{\P(f_{l}|a)}.
\end{equation*}
Parameter $f_l$:
\begin{equation*}
h_{f_l} = \frac{\P(f_l'|a)}{\P(f_l|a)}\prod_{i=1}^n \frac{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma, \epsilon)\P(\gamma|f_l')}{\sum_{\gamma = 0}^2 \P(g_{il}|\gamma, \epsilon) \P(\gamma|f_l)}.
\end{equation*}
Because of the sum over $\gamma$, we cannot simplify the likelihood any further.}

  
\item\label{item:MLE_f} We can obtain initial guesses of the parameters $f_l$ and $a$ as follows: First, estimate the MLEs $\hat{f}_l$ for each locus under the assumption of now error. Second, estimate $\hat{a}$ given all $\hat{f}_l$.

The MLE of a beta distribution does not have a general, closed form solution. We will thus make use of method of moments estimators, based on the first two moments (sample mean $\Bar{f}$ and variance $\Bar{v}$) of the MLEs $\hat{\f}$:
\begin{equation*}
    \hat{a} = \Bar{f} \left(\frac{\Bar{f}(1-\Bar{f})}{\Bar{v}} - 1 \right).
\end{equation*}

Derive the MLE $\hat{f}_l$ under the assumption that $\epsilon = 0$ and implement functions to obtain these guesses.

\sol{
If $\epsilon=0$, no genotyping errors occur and the observed genotypes $g_{il}$ will hence be the same as the true genotypes $\gamma_{il}$. We can therefore simplify the likelihood: 
\begin{equation*}
    \L(f_l) = \prod_{i=1}^n \P(g_{il}|f_l) = \prod_{i=1}^n \binom{2}{g_{il}} f_l^{g_{il}} (1-f_l)^{2-g_{il}}.
\end{equation*}
The simplified log-likelihood then is:
\begin{equation*}
    \l(f_l) = \sum_{i=1}^n \log\binom{2}{g_{il}} + \log f_l \sum_{i=1}^n g_{il} + \log(1-f_l) \left(2n - \sum_{i=1}^n g_{il}\right).
\end{equation*}
Take the derivative with respect to $f_l$:
\begin{equation*}
    \frac{d}{d f_l} \l(\f) = \frac{1}{f_l} \sum_{i=1}^n g_{il} - \frac{1}{1-f_l} \left(2n - \sum_{i=1}^n g_{il}\right).
\end{equation*}
Now set the derivative to zero and solve for $f_l$. This results in:
\begin{equation*}
    \hat{f_l} = \frac{1}{2n} \sum_{i=1}^n g_{il}.
\end{equation*}

See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation of these guessing functions.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Implement a function \texttt{update\_a} to update $s$. It should take as arguments the current value of $a$, the vector of current $\boldsymbol{f}=(f_1, \ldots, f_L)$, $\lambda$ and the parameters for the proposel kernel of $a$. Make use of hte functions \texttt{dexp} and \texttt{dbeta} to calculate the hasting ratio.

\sol{See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation.}

\item Implement a function \texttt{update\_e} to update $\epsilon$, analogously to the function to update $a$. What are the required arguments?

\sol{The required arguments are the current $\epsilon$, the vector of current $\boldsymbol{f}=(f_1, \ldots, f_L)$, the matrix of observed genotypes $\g$, the prior parameter $\beta$ and the proposal parameters. See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation.}

\item Implement a function \texttt{update\_f} to update one $f_l$.

\sol{See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation.}


\item Use these functions to implement the full MCMC.

\sol{See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\item Implement a function to simulate data under this model. The simulate data for 100 loci and 10 individuals with $\epsilon = 0.1$ and $a = 0.5$. \label{item:simulate_multiL}

\item Estimate parameters for the data simulated under \ref{item:simulate_multiL} using $\beta = 10$ and $\lambda = 1$ for the prior distributions. Plot the trace to verify proper convergence and adjust your proposal kernels if necessary. Then, plot both the two dimensional posterior for $\epsilon$ and $a$, as well as the two marginal posterior distributions.

%\sol{See link \solutions{ML_Exercises_9_MetropolisHastings2/ML_Exercises_9_MetropolisHastings2.R} for an implementation.}

\end{enumerate}

\end{enumerate}

\end{document}
