\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{exercises}[Exercises LaTeX class]

%Package options
\newif\if@solutions\@solutionsfalse
\DeclareOption{solution}{\@solutionstrue}
\DeclareOption{solutions}{\@solutionstrue}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ProcessOptions\relax

\LoadClass[a4paper,10pt]{article}% load article class with options

%other packages
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color, xcolor}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}
\usepackage{microtype}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=7mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{25.5cm}
\setlength\topmargin{-1.5cm}
\linespread{1.2}
\setcounter{secnumdepth}{0}

%% Solutions and comments
\if@solutions
  \RequirePackage[showSolutions]{optional}
\else
  \RequirePackage[hideSolutions]{optional}%% the default
\fi

\usepackage{tikz}
\usetikzlibrary{bayesnet}
\tikzset{%
  main/.style = {node distance=16mm},
  latent/.style={draw, circle, minimum size=8mm},
  obs/.style={draw, rectangle, fill=lightgray!50, minimum size=8mm}
}

%\usepackage[\pgfkeysvalueof{/exercises/solutions}]{optional}
%\usepackage[hide]{optional}
\definecolor{scriptcolor}{HTML}{7d1b8f}
\definecolor{solutioncolor}{HTML}{0b30c2}
%\definecolor{solutioncolor}{HTML}{0000ff}
\definecolor{commentcolor}{HTML}{9a173b}

\newcommand{\script}[1]{{\color{scriptcolor} \texttt{#1}}}
\newcommand{\sol}[1]{\opt{showSolutions}{\vspace{0.05cm} {\color{solutioncolor} #1}}\smallskip}
\newcommand{\com}[1]{\opt{showSolutions}{{\color{commentcolor} #1}}}

%% Links to bitbucket (for long code solutions)
\usepackage{fontawesome}
\usepackage[hidelinks]{hyperref}
\definecolor{linkcolor}{HTML}{9a173b}
\newcommand{\codesymbol}{{\textcolor{linkcolor}{\LARGE \faFileCodeO}}}
\newcommand{\baseUrlBitbucket}{https://bitbucket.org/wegmannlabteaching/statistical_modelling_and_inference/raw/master/}
\newcommand{\solutions}[1]{\marginpar{\href{\baseUrlBitbucket Exercises/#1}{\codesymbol}}}
\newcommand{\linksolutions}[1]{\sol{See link for an example implementation. \solutions{#1}}}

%opening
\newcommand{\exerciseTitle}[2]{\title{Machine Learning Exercises #1\\#2}}
\author{Prof. Daniel Wegmann}
\date{}

%% Mathematical Symbols
\def\E{\mathbb{E}}
\def\P{\mathbb{P}}
\def\L{{\cal L}}
\def\l{{\ell}}
\def\N{{\cal N}}
\DeclareMathOperator*{\argmax}{arg\,max}
\AtBeginDocument{\renewcommand{\d}{\boldsymbol{d}}} % package hyperref hides \d
\def\dd{\mathrm{d}}
\def\x{\boldsymbol{x}}
\def\z{\boldsymbol{z}}
\def\s{\boldsymbol{s}}
\def\H{\boldsymbol{\mathrm{H}}}
\def\btau{\boldsymbol{\tau}}
\def\btheta{\boldsymbol{\theta}}
\def\iif{\mathrm{if\ }}

\everymath{\displaystyle}
