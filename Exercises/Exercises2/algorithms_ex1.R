#-------------------------
#some functions
#-------------------------
logistic <- function(x, beta){
  return(1/(1+exp(-beta*x)));
}

simData<-function(n,beta){
  x <- rnorm(n,mean=0, sd=1)
  l <- logistic(x, beta);
  d <- as.numeric(runif(n) < l);
  return(data.frame(x=x,d=d));
}

logLikelihood <- function(x, d, beta){
  return(sum(beta*x*(d-1) - log(1 + exp(-beta*x)))); 
}

derivativeLogLikelihood <- function(x, d, beta){
  return( sum(x*d - x) + sum(x*exp(-beta*x) / (1 + exp(-beta*x))) ); 
}

secondDerivativeLogLikelihood <- function(x, d, beta){
  return( sum( (-x^2 * exp(-beta*x)) / (1+exp(-beta*x))^2 ) ); 
}

#-------------------------
#plot logistic
#-------------------------
x <- seq(-10,10, length.out=1000);
plot(x, logistic(x,0.5), col='blue', type='l')
lines(x, logistic(x,1), col='black')
lines(x, logistic(x,2), col='red')
legend('topleft', bty='n', lwd=1, col=c('blue', 'black', 'red'), legend=c(0.5, 1, 2))

#-------------------------
#simulate data
#-------------------------
trueBeta <- 2.0;
data <- simData(50,trueBeta);
d <- data$d; x <- data$x;

#-------------------------
#LL surface
#-------------------------
par(mfrow=c(2,2), mar=c(4,4,1,1));
beta <- seq(-5, 5, length.out=10000);
LL <- sapply(beta, FUN=logLikelihood, x=x, d=d);
plot(beta, LL, type='l', ylim=c(1.2,0.98)*max(LL));
MLE <- beta[which(LL==max(LL))];
lines(rep(MLE,2), par("usr")[3:4], lty=2, col='blue');
lines(rep(trueBeta,2), par("usr")[3:4], lty=2, col='red');

#-------------------------
#steepest ascent
#-------------------------
nIter <- 100;
curBeta <- numeric(nIter);
curBeta[1] <- 10;
step <- 0.2;
for(i in 2:nIter){
  derivative <- derivativeLogLikelihood(x, d, curBeta[i-1]); 
  curBeta[i] <- curBeta[i-1] + derivative * step;
  step <- step * 0.95;
}

plot(curBeta, type='b', ylim=c(-2,10))
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red')
lines(par("usr")[1:2], rep(MLE,2), lty=2, col='blue')

#-------------------------
#simple algorithm
#-------------------------
nIter <- 100;
curBeta <- numeric(nIter);
curBeta[1] <- 10;
step <- 10;
previousLL <- logLikelihood(x, d, curBeta[1]);  
for(i in 2:nIter) {  
  curBeta[i] <- curBeta[i-1] + step;
  LL <- logLikelihood(x, d, curBeta[i]);  
  if(LL < previousLL){ step <- -step/exp(1); }
  previousLL <- LL;
}

plot(curBeta, type='b', ylim=c(-2,10));
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red');
lines(par("usr")[1:2], rep(MLE,2), lty=2, col='blue')

#-------------------------
# Newton-Raphson method
#-------------------------
nIter <- 100;
curBeta <- numeric(nIter);
curBeta[1] <- 10;

for(i in 2:nIter) {
  first <- derivativeLogLikelihood(x, d, curBeta[i-1]); 
  second <- secondDerivativeLogLikelihood(x, d, curBeta[i-1]);
  step <- first / second;

  curBeta[i] <- curBeta[i-1] - step;
  
  #backtracking
  fac <- 0.5;
  while(abs(derivativeLogLikelihood(x, d, curBeta[i])) > abs(first)){
    
    print(paste("back tracking in iteration", i));
    
    curBeta[i] <- curBeta[i-1] - step * fac;
    fac <- fac / 2;
  }
}

plot(curBeta, type='b', ylim=c(-2,10));
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red')
lines(par("usr")[1:2], rep(MLE,2), lty=2, col='blue')



