\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[alwaysadjust]{paralist}
\usepackage[usenames,dvipsnames]{color}
\usepackage{alltt,graphicx}
\usepackage{amsmath,amsfonts,verbatim}

\usepackage[paper=a4paper,marginparwidth=0mm,marginparsep=0mm,margin=17.5mm,includemp]{geometry}
\setlength\textheight{27cm}
\setlength\topmargin{-3cm}
\linespread{1.2}

%\usepackage[withSolutions]{optional}
\usepackage[withoutSolutions]{optional}

\newcommand{\script}[1]{{\color{Plum} \texttt{#1}}}

\newcommand{\E}{\operatorname{E}}
\def\P{\mathbb{P}}
\def\L{{\cal L}}
\def\d{\boldsymbol{d}}
\def\x{\boldsymbol{x}}
\def\H{\boldsymbol{\mathrm{H}}}

\newcommand{\sol}[1]{\opt{withSolutions}{\vspace{0.05cm}\\ {\color{red} \texttt{#1}}}\smallskip}
\newcommand{\com}[1]{\opt{withSolutions}{\\ {\color{blue} \texttt{#1}}}}

\everymath{\displaystyle}


%opening
\title{Machine Learning - Exercises 2}
\author{Prof. Daniel Wegmann}
\date{}

\begin{document}

\maketitle


\section*{Numerical Approaches to find MLE}
\begin{enumerate}[1.]
\item It is suspected that exposure to a certain chemical is increasing the risk of suffering from lung cancer. To better characterize this relation ship, $n$ mice were exposed to various levels $x_i, i=1, \ldots, n$ of the chemical and it was recorded whether ($d_i=1$) or not ($d_i=0$) they developed lung cancer.\\ \\
The goal is now to model this relationship and it shall be assumed (as is commonly done for binary outcomes) that the probability of suffering from lung cancer is a logistic function of the level of exposure such that
\begin{equation*}
 \P(d_i=1|\beta, x_i) = \frac{1}{1+e^{-\beta x_i}}; \quad \P(d_i=0|\beta, x_i) = 1 - \P(d_i=1|\beta, x_i).
\end{equation*}
Note that for simplicity, the levels of exposure $x_i$ where normalized to have mean zero (hence negative values are possible).


\begin{enumerate}[a)]
\item Draw the DAG of this model.
\sol{\includegraphics[width=6cm]{DAG_Ex1.pdf}}

\item Plot $\P(d_i=1|\beta, x_i)$ as a relationship of $\beta$ using R to understand the general idea of modeling such data using a logistic function.
\sol{\script{logit <- function(x, beta)\{\\
  \hspace*{1cm} return(1/(1+exp(-beta*x)));\\
\}\\
x <- seq(-10,10, by=0.1);\\
plot(x,logit(x, 1), ylim=c(0,1));\\}\\
Try for different $\beta$ values to understand the effect of $\beta$.}

\item Write a function in R to simulate data under such a model. Assume that the $x_i$ follow a standard normal distribution: $x_i \sim {\cal N}(0, 1)$.
\sol{\script{simData <- function(n,beta) \{ \\
  \hspace*{1cm} x <- rnorm(n, mean=0, sd=1); \\
 \hspace*{1cm} tmp <- 1/(1+exp(-beta*x)); \\
 \hspace*{1cm} \# you could also use plogis(x,location=0,scale=1/beta) \\
 \hspace*{1cm} d <- as.numeric(runif(n) < tmp); \\
 \hspace*{1cm} return(data.frame(x=x,d=d)); \\
 \}\\
 trueBeta <- 2.0; \\
data <- simData(1000,trueBeta); \\
d <- data\$d; x <- data\$x;}}

\item Write out the full likelihood function $\L(\beta)=\P(\d|\beta, \x)$ with $\d=\{d_1, \ldots, d_n\}$ and $\x=\{x_1, \ldots, x_n\}$.
\sol{
Defining $\boldsymbol{d}=\{d_1, \ldots, d_n\}$, we can write the full likelihood like
\begin{eqnarray*}\P(\boldsymbol{d}|\beta, x_i) &=& \prod_{i=1}^n \left(\frac{1}{1+e^{-\beta x_i}}\right)^{d_i} \left(1-\frac{1}{1+e^{-\beta x_i}}\right)^{1-d_i}\\
&=& \prod_{i=1}^n \left(\frac{1}{1+e^{-\beta x_i}}\right)^{d_i} \left(\frac{e^{-\beta x_i}}{1+e^{-\beta x_i}}\right)^{1-d_i}
     \end{eqnarray*}
}

\item Calculate the gradient $\nabla \ell(\beta) = \frac{\mathrm{d}}{\mathrm{d}\beta} \ell(\beta)$ by taking the first derivative of the log-likelihood $\ell(\beta) = \log \L(\beta)$. Try to find the MLE of $\beta$ analytically and convince yourself that you are not able to solve for $\beta$ analytically when setting $\nabla \ell(\beta)=0$.
\sol{Let us first calculate the logarithm of the full likelihood
\begin{eqnarray*} 
\log\P(\boldsymbol{d}|\beta, x_i) &=& -\sum_{i}d_i \log(1+e^{-\beta x_i}) + \sum_{i}(1-d_i)\log e^{-\beta x_i} - \sum_{i}(1-d_i)\log(1+e^{-\beta x_i}) \\
 &=& \beta\sum_{i}x_i(d_i-1) - \sum_{i}\log(1+e^{-\beta x_i}) 
\end{eqnarray*}
We can now obtain the derivative of the logarithm of the full likelihood
\begin{eqnarray*}
\frac{\mathrm{d}\log\P(\boldsymbol{d}|\beta, x_i)}{\mathrm{d} \beta} &=& \sum_{i}x_i(d_i-1) - \sum_{i}\frac{1}{1+e^{-\beta x_i}}e^{-\beta x_i}(-x_i) \\
&=& \sum_{i}x_i(d_i-1) + \sum_{i}\frac{x_i e^{-\beta x_i}}{1+e^{-\beta x_i}}
\end{eqnarray*}\\
This equation contains a series of terms with $\beta$ both in the numerator as well as in the\\ denominator. The classic way to solve such an equation is to multiply by all denominators, but that can not lead to a closed form solution, as the number of terms is variable.
}

\item Implement a simple MLE finding algorithm that attempts to find the value of $\beta$ at which $\nabla \ell(\beta) = \frac{\mathrm{d}}{\mathrm{d}\beta} \ell(\beta) = 0$ by moving in one direction with a step size $s_t$ until you pass the zero location. Then return and start walking in the reverse direction with a new step size $s_{t+1}=s_t / e$, and so forth.
\sol{\script{logLikelihood <- function(x, d, beta)\{\\
  \hspace*{1cm} return(sum(beta*x*(d-1) - log(1 + exp(-beta*x)))); \\
\}\\
\\
nIter <- 100;\\
curBeta <- numeric(nIter);\\
curBeta[1] <- 10;\\
step <- 10;\\
previousLL <- logLikelihood(x, d, curBeta[1]);  \\
for(i in 2:nIter)\{\\  
\hspace*{1cm}  curBeta[i] <- curBeta[i-1] + step;\\
\hspace*{1cm}  LL <- logLikelihood(x, d, curBeta[i]);  \\
\hspace*{1cm}  if(LL < previousLL)\{ step <- -step/exp(1); \}\\
\hspace*{1cm}  previousLL <- LL;\\
\}\\
\\
plot(curBeta, type='b');\\
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red');\\}}


\item Implement a ``steepest ascent'' method to find the MLE of $\beta$ by starting at a random position and then moving always along the slope towards the optimum. Make sure to start with a certain step size, but to always reduce the size of the step in each interval.
\sol{\script{derivativeLogLikelihood <- function(x, d, beta)\{\\
   \hspace*{1cm} return( sum(x*d - x) + sum(x*d*exp(-beta*x) / (1 + exp(-beta*x)))  ); \\
\}\\
\\
nIter <- 100;\\
curBeta <- numeric(nIter);\\
curBeta[1] <- 10;\\
step <- 10;\\
for(i in 2:nIter)\{\\
 \hspace*{1cm} derivative <- derivativeLogLikelihood(x, d, curBeta[i-1]); \\
 %\hspace*{1cm} if(derivative > 0)\{ curBeta[i] <- curBeta[i-1] + step; \}\\
 %\hspace*{1cm} else\{ curBeta[i] <- curBeta[i-1] - step; \} \\
 \hspace*{1cm} curBeta[i] <- curBeta[i-1] + derivative * step;\\
 \hspace*{1cm} step <- step * 0.9;\\
\}\\
\\
plot(curBeta, type='b');\\
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red');}}

\item Implement the Newton-Raphson method to find the MLE for this problem.
\sol{We need the second derivative of the logarithm of the full likelihood:
\begin{eqnarray*}
 \frac{\mathrm{d}^2 \log\P(\boldsymbol{d}|\beta, x_i)}{\mathrm{d}\beta^2} & = & \frac{\mathrm{d}}{\mathrm{d}\beta} \left( \sum_{i}x_i(d_i-1) + \sum_{i}\frac{x_i e^{-\beta x_i}}{1+e^{-\beta x_i}}\right)\\
 & = & \frac{\mathrm{d}}{\mathrm{d}\beta}  \sum_{i}\frac{x_i e^{-\beta x_i}}{1+e^{-\beta x_i}}\\ 
 & = & \frac{-x_i^2 e^{-\beta x_i} \left(1+e^{-\beta x_i}\right) - x_i e^{-\beta x_i} \left(-x_i e^{-\beta x_i}\right)}{\left( 1+e^{-\beta x_i} \right)^2}\\
 & = & \sum_{i}\frac{-x_i^2 e^{-\beta x_i} }{\left( 1+e^{-\beta x_i} \right)^2}
 \end{eqnarray*}\\
 Now we can implement the Newton-Raphson algorithm as follows:\\
 \script{secondDerivativeLogLikelihood <- function(x, d, beta)\{\\
\hspace*{1cm}  return( sum( (-x\^{}2 * exp(-beta*x)) / (1+exp(-beta*x))\^{}2 ) ); \\
  \}\\
\\
 nIter <- 100;\\
curBeta <- numeric(nIter);\\
curBeta[1] <- 10;\\
for(i in 2:nIter)\{\\
\hspace*{1cm}  first <- derivativeLogLikelihood(x, d, curBeta[i-1]); \\
\hspace*{1cm}  second <- secondDerivativeLogLikelihood(x, d, curBeta[i-1]);\\
\hspace*{1cm}  curBeta[i] <- curBeta[i-1] - first / second;\\
\hspace*{1cm}  if(curBeta[i] < 0.0)\{ curBeta[i] <- 0.0001;\}\\
\}\\
plot(curBeta, type='b');\\
lines(par("usr")[1:2], rep(trueBeta,2), lty=2, col='red')}\\
Note that since $\beta$ can become negative during a Newton-Raphson iteration, we need to prevent that.}


\item Which of the methods converges fastest?
\sol{If done right, the Newton-raphson algorithm should converge fastest.}
\end{enumerate}

%---------------------
\item Consider the same model as above but also allow for an intercept such that
\begin{equation*}
 \P(d_i=1|\beta_0, \beta1, x_i) = \frac{1}{1+e^{\beta_0 -\beta_1 x_i}}.
\end{equation*}
The goal is now to find the MLE for the two parameters $\beta_0$ and $\beta_1$.

\begin{enumerate}[a)]
\item Draw the DAG of this model.
\sol{\includegraphics[width=6cm]{DAG_Ex2.pdf}}

\item Plot $\P(d_i=1|\beta_0, \beta_1, x_i)$ as a function of $\beta_1$ for different $\beta_0$ using R to understand the general idea of modeling such data using a logistic function.
\sol{\script{logit <- function(x, beta0, beta1)\{\\
  \hspace*{1cm} return(1/(1+exp(beta0-beta1*x)));\\
\}\\
x <- seq(-10,10, by=0.1);\\
plot(x,logit(x, 2, 1), ylim=c(0,1));\\}\\
Try for different $\beta_1$ values to understand the effect of $\beta_1$.}


\item Write a function in R to simulate data under such a model.
\sol{\script{simData <- function(n, beta0, beta1, sd=1) \{ \\
  \hspace*{1cm} x <- rnorm(n, mean=0, sd=sd); \\
 \hspace*{1cm} tmp <- 1/(1+exp(beta0-beta1*x)); \\
 \hspace*{1cm} d <- as.numeric(runif(n) < tmp); \\
 \hspace*{1cm} return(data.frame(x=x,d=d)); \\
 \}\\
 trueBeta0 <- 1.0; \\
trueBeta1 <- 2.0; \\
data <- simData(1000,trueBeta0,trueBeta1); \\
d <- data\$d; x <- data\$x;}}


\item Write out the full likelihood function for $\L(\beta_0, \beta_1) = \P(\d |\beta_0, \beta_1, \x)$.
\sol{
Defining $\boldsymbol{d}=\{d_1, \ldots, d_n\}$, we can write the full likelihood like
\begin{eqnarray*}\P(\boldsymbol{d}|\beta_0,\beta_1, x_i) &=& \prod_{i=1}^n \left(\frac{1}{1+e^{\beta_0-\beta_1 x_i}}\right)^{d_i} \left(1-\frac{1}{1+e^{\beta_0-\beta_1 x_i}}\right)^{1-d_i}\\
&=& \prod_{i=1}^n \left(\frac{1}{1+e^{\beta_0-\beta_1 x_i}}\right)^{d_i} \left(\frac{e^{\beta_0-\beta_1 x_i}}{1+e^{\beta_0-\beta_1 x_i}}\right)^{1-d_i}
     \end{eqnarray*}
}

\item Calculate the gradient vector $\nabla \ell(\beta_0,\beta_1)$ on the log-likelihood $\ell(\beta_0,\beta_1) = \log L(\beta_0,\beta_1)$. Note that 
\begin{equation*}
\nabla \ell(\beta_0,\beta_1) = \begin{pmatrix}\frac{\partial}{\partial \beta_0} \ell(\beta_0, \beta1) \\\frac{\partial}{\partial \beta_1} \ell(\beta_0, \beta1)\end{pmatrix}
\end{equation*}
\sol{
Let's first calculate the logarithm of the full likelihood
\begin{eqnarray*} 
\log\P(\boldsymbol{d}|\beta_0,\beta_1, x_i) &=& -\sum_{i}d_i \log(1+e^{\beta_0-\beta_1 x_i}) + \sum_{i}(1-d_i)\log e^{\beta_0-\beta_1 x_i}-\sum_{i}(1-d_i)\log(1+e^{\beta_0-\beta_1 x_i}) \\
 &=& \sum_{i}(\beta_0-\beta_1 x_i)(1-d_i) - \sum_{i}\log(1+e^{\beta_0-\beta_1 x_i}).
\end{eqnarray*}
We can now obtain the derivative of the logarithm of the full likelihood
\begin{eqnarray*}
\frac{\partial}{\partial \beta_0} \ell(\beta_0, \beta_1) &=& \sum_{i}(1-d_i)-\sum_{i}\frac{e^{\beta_0-\beta_1 x_i}}{1+e^{\beta_0-\beta_1 x_i}} \\
\end{eqnarray*}
\begin{eqnarray*}
\frac{\partial}{\partial \beta_1}\ell(\beta_0, \beta_1) &=& \sum_{i}x_i(d_i-1) + \sum_{i}\frac{x_i e^{\beta_0-\beta_1 x_i}}{1+e^{\beta_0-\beta_1 x_i}}
\end{eqnarray*}
that we need to calculate our 
\begin{eqnarray*}
\nabla \ell(\beta_0,\beta_1)=\left(\begin{array}{cc} \frac{\partial\log\P(\boldsymbol{d}|\beta_0,\beta_1, x_i)}{\partial \beta_0} \\
\frac{\partial\log\P(\boldsymbol{d}|\beta_0,\beta_1, x_i)}{\partial \beta_1}
\end{array}
\right)=
\left(\begin{array}{cc} \sum_{i}(1-d_i)-\sum_{i}\frac{e^{\beta_0-\beta_1 x_i}}{1+e^{\beta_0-\beta_1 x_i}}  \\
\sum_{i}x_i(d_i-1) + \sum_{i}\frac{x_i e^{\beta_0-\beta_1 x_i}}{1+e^{\beta_0-\beta_1 x_i}}
\end{array}
\right)
\end{eqnarray*}
In R: \\
\script{calcGradient <- function(x, d, beta0, beta1)\{\\
\hspace*{1cm}  grad <- c(sum( 1-d-(exp(beta0-beta1*x)) / (1 + exp(beta0-beta1*x))), \\ 
\hspace*{1.5cm}  sum( x*(d-1)+(x*exp(beta0-beta1*x)) / (1 + exp(beta0-beta1*x)))); \\
\hspace*{1cm}  return <- grad; \\
  \}\\
\\
 nIter <- 100;\\
curBeta0 <- numeric(nIter);\\
curBeta0[1] <- 9;\\
curBeta1 <- numeric(nIter);\\
curBeta1[1] <- 10;\\
step <- 10;\\
for(i in 2:nIter)\{\\
\hspace*{1cm}  F <- calcGradient(x, d, curBeta0[i-1], curBeta1[i-1]); \\
\hspace*{1cm}  if(F[2] > 0) \{ \\
\hspace*{2cm}  curBeta1[i] <- curBeta1[i-1] + step; \\
\hspace*{1cm}  \} else \{ curBeta1[i] <- curBeta1[i-1] - step; \} \\
\hspace*{1cm}  if(F[1] > 0) \{ \\
\hspace*{2cm}  curBeta0[i] <- curBeta0[i-1] + step; \\
\hspace*{1cm}  \} else \{ \\ 
\hspace*{1cm}  curBeta0[i] <- curBeta0[i-1] - step; \} 
\hspace*{1cm}  step <- step * 0.9; \\
\}\\
par(mfrow=c(1,2))\\
plot(curBeta0, type='b');\\
lines(par("usr")[1:2], rep(trueBeta0,2), lty=2, col='red')\\
plot(curBeta1, type='b');\\
lines(par("usr")[1:2], rep(trueBeta1,2), lty=2, col='red')}}

\item Calculate the Hessian matrix of second derivatives 
\begin{equation*}
\H=\begin{pmatrix}
    \frac{\partial^2}{\partial^2 \beta_0} \ell(\beta_0, \beta_1) & \frac{\partial^2}{\partial \beta_0 \partial \beta_1} \ell(\beta_0, \beta_1)\\
    \frac{\partial^2}{\partial \beta_1 \partial \beta_0} \ell(\beta_0, \beta_1) & \frac{\partial^2}{\partial^2 \beta_1} \ell(\beta_0, \beta_1)
   \end{pmatrix}.
\end{equation*}
Note that $\frac{\partial^2}{\partial a \partial b}f(x) =\frac{\partial^2}{\partial b \partial a}f(x)$ (Schwarz's theorem).
\sol{\begin{eqnarray*}
\frac{\partial^2}{\partial \beta_0^2} \ell(\beta_0, \beta_1) &=& -\sum_{i}\frac{e^{\beta_0-\beta_1 x_i}(1+e^{\beta_0-\beta_1 x_i})-(e^{\beta_0-\beta_1 x_i})^2}{(1+e^{\beta_0-\beta_1 x_i})^2} = -\sum_{i}\frac{e^{\beta_0-\beta_1 x_i}}{(1+e^{\beta_0-\beta_1 x_i})^2} \\
\frac{\partial^2}{\partial \beta_1^2} \ell(\beta_0, \beta_1) &=& \sum_{i}\frac{-x_i^2e^{\beta_0-\beta_1 x_i}(1+e^{\beta_0-\beta_1 x_i})-x_ie^{\beta_0-\beta_1 x_i}(-x_i)e^{\beta_0-\beta_1 x_i}}{(1+e^{\beta_0-\beta_1 x_i})^2} = -\sum_{i}\frac{x_i^2e^{\beta_0-\beta_1 x_i}}{(1+e^{\beta_0-\beta_1 x_i})^2} \\
\frac{\partial^2}{\partial \beta_0 \partial \beta_1} \ell(\beta_0, \beta_1) &=& -\sum_{i}\frac{e^{\beta_0-\beta_1 x_i}(-x_i)(1+e^{\beta_0-\beta_1 x_i})-e^{(\beta_0-\beta_1 x_i)^2}(-x_i)}{(1+e^{\beta_0-\beta_1 x_i})^2} = \sum\frac{x_ie^{\beta_0-\beta_1 x_i}}{(1+e^{\beta_0-\beta_1 x_i})^2}\\
\end{eqnarray*}
}

\item Implement the Newton-Raphson method to find the MLE for this problem.
\sol{
We can write the new point $x_1$ as a function of the starting point $x_0$
$$x_1=x_0-\nabla\log\P(x_0) H(\log\P(x_0))^{-1} $$
In R: \\
\script{calcHessian <- function(x, beta0, beta1)\{\\
\hspace*{1cm} H <- matrix(data=c(sum(-exp(beta0-beta1*x)/(1+exp(beta0-beta1*x))**2), \\
\hspace*{1.5cm} sum(x*exp(beta0-beta1*x)/(1+exp(beta0-beta1*x))**2), \\
\hspace*{1.5cm} sum(x*exp(beta0-beta1*x)/(1+exp(beta0-beta1*x))**2), \\
\hspace*{1.5cm} sum(-x**2*exp(beta0-beta1*x)/(1+exp(beta0-beta1*x))**2)), ncol=2, nrow=2); \\
\hspace*{1cm} return(H);\\
\}\\
nIter <- 100; \\
pos <- matrix(nrow=100, ncol=2); \\
pos[1,] <- c(9, 10); \\
for(i in 2:nIter)\{\\
\hspace*{1cm} F <- matrix(data=c(calcGradient(x, d, pos[i-1,1], pos[i-1,2])), ncol=2, nrow=1);\\
\hspace*{1cm} H <- calcHessian(x, pos[i-1,1], pos[i-1,2]);\\
\hspace*{1cm} pos[i,] <- c(pos[i-1,1], pos[i-1,2]) - F\%*\%solve(H);\\
\hspace*{1cm} if(pos[i,2] < 0.0)\{ pos[i,2] <- 0.0001;\}\\
\hspace*{1cm} if(pos[i,1] < 0.0)\{ pos[i,1] <- 0.0001;\}\\
\}\\
par(mfrow=c(1,2))\\
plot(pos[,1], type='b');\\
lines(par("usr")[1:2], rep(trueBeta0,2), lty=2, col='red')\\
plot(pos[,2], type='b');\\
lines(par("usr")[1:2], rep(trueBeta1,2), lty=2, col='red')\\
}
}




\end{enumerate}

\end{enumerate}

\end{document}
